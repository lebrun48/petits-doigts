//Products
LEAF_TYPE_ROOT_FOLDER = '1';
LEAF_TYPE_FOLDER = '2';
LEAF_TYPE_PRODUCT_CHOICE = '3';
LEAF_TYPE_PRODUCT = '4';
LEAF_TYPE_MATERIAL = '5';
LEAF_TYPE_FINISHED_PRODUCT = '6';

LEAF_MENU_EDIT = 1;
LEAF_MENU_DUPLICATE = 2;
LEAF_MENU_REMOVE = 3;
LEAF_MENU_NEW_FOLDER = 4;
LEAF_MENU_NEW_PRODUCT_CHOICE = 5;
LEAF_MENU_ADD_PRODUCT = 6;
LEAF_MENU_ADD_MATERIAL = 7;
LEAF_MENU_ADD_EXISTING_MATERIAL = 8;
LEAF_MENU_DROP = 9;

DROP_COPY_BEFORE = 1;
DROP_COPY_AFTER = 2;
DROP_COPY_INSIDE = 3;
DROP_MOVE_BEFORE = 4;
DROP_MOVE_AFTER = 5;
DROP_MOVE_INSIDE = 6;


dragELement = '';
all = [];

function treeListener()
{
  $('.treeview').mdbTreeview();

  $('.tree-leaf').off("click").on('click', function () {
    if ($("#context-menu:visible").length) {
      $("#context-menu").removeClass("show").hide();
    }
    if ((leafId = $(this).parent().attr("id")) == '1-0-') {
      userRequest({xAction: "edit", action: LEAF_MENU_NEW_FOLDER, leafId})
      return;
    }

    parentProduct = $(this).parent().parents().filter("[id^=" + LEAF_TYPE_PRODUCT + "]").first().attr("id");
    $("#context-menu a").show().each(function () {
      menu = $(this).data("action");
      switch (leafId.charAt(0)) {
        case LEAF_TYPE_FOLDER:
        case LEAF_TYPE_ROOT_FOLDER:
          menu > LEAF_MENU_NEW_FOLDER && menu != LEAF_MENU_ADD_PRODUCT && ($(this).hide());
          break;

        case LEAF_TYPE_PRODUCT_CHOICE:
          // No product choice of product option
          (menu == LEAF_MENU_NEW_FOLDER || menu == LEAF_MENU_NEW_PRODUCT_CHOICE) && ($(this).hide());
          break;

        case LEAF_TYPE_MATERIAL:
          (menu == LEAF_MENU_DUPLICATE || menu >= LEAF_MENU_NEW_FOLDER) && ($(this).hide());
          break;
      }
    });
    pos = $(this).position();
    $("#context-menu").css({
      display: "block",
      top: pos.top + 15,
      left: pos.left + 20
    }).addClass("show").data("leafid", leafId).data("parent", parentProduct);
    $(document).keyup(function (event) {
      if (event.keyCode === 27) {
        $("#context-menu").removeClass("show").hide();
        $(document).off("keyup");
      }
    }).click(function () {
      $("#context-menu").removeClass("show").hide();
      $(document).off("click");
    });
    return false; //blocks default Webbrowser right click menu
  });

  $(".drag").off("dragstart").on("dragstart", function (ev) {
    dragELement = ev.originalEvent.target.id;
    all = [];
    tree = dragELement.slice(2, -1).split('-');
    //all contains all product following drag element
    all.push(tree[tree.length - 1]);
    $("#" + dragELement).find("[id]").each(function () {
      if (this.id.charAt(0) != LEAF_TYPE_MATERIAL) {
        ar = this.id.slice(2, -1).split('-');
        all.push(ar[ar.length - 1]);
      }
    });
  });

  $(".drop").off("dragover").on("dragover", function (ev) {
    // no drop on itself
    (leafId = this.parentNode.id) || (leafId = this.previousSibling.id);
    ev.originalEvent.dataTransfer.dropEffect = (isCopy = ev.originalEvent.ctrlKey) ? "copy" : "move";
    if (leafId == dragELement) {
      return;
    }

    if (!isCopy) {
      parentDrag = $("#" + dragELement).parents("li").first().attr("id");
      parentDrop = $("#" + leafId).parents("li").first().attr("id");
      if (parentDrag == parentDrop) {
        ev.preventDefault();
        return;
      }
    }

    if (dropInsideAllowed(leafId)) {
      ev.preventDefault();
    }

    //no drop for root folder or material on folder or folder on folder except root folder or product choice only on product

  }).off("drop").on("drop", function (ev) {
    ev.preventDefault();
    leafId = this.parentNode.id;
    isCopy = ev.originalEvent.ctrlKey ? 1 : 0;
    !(parentProduct = $(this).parent().parents().filter("[id^=" + LEAF_TYPE_PRODUCT + "]").first().attr("id")) && (parentProduct = '');
    parentDrag = $("#" + dragELement).parents("li").first().attr("id");
    parentDrop = $("#" + leafId).parents("li").first().attr("id");
    //if drag is MATERIAL and drop parent is not product only inside is authorized
    if (dragELement.charAt(0) == LEAF_TYPE_MATERIAL && parentDrop.charAt(0) != LEAF_TYPE_PRODUCT && parentDrop.charAt(0) != LEAF_TYPE_PRODUCT_CHOICE) {
      $("#drop-menu").data("parent", leafId).data("parent_product", parentProduct).data("is_copy", isCopy);
      $("#drop-menu .dropdown-item[data-action=" + (isCopy ? DROP_COPY_INSIDE : DROP_MOVE_INSIDE) + "]").click();
      return;
    }
    diff = 0;
    if (sameParent = parentDrag == parentDrop) {
      if (isCopy) {
        //only copy inside allowed
        $("#drop-menu").data("parent", leafId).data("parent_product", parentProduct).data("is_copy", 1);
        $("#drop-menu .dropdown-item[data-action=" + DROP_COPY_INSIDE + "]").click();
        return;
      }
      diff = $(this.parentNode).index() - $("#" + dragELement).index();
    }

    if (isCopy) {
      $("#drop-menu .move").hide();
      $("#drop-menu .copy").show();
      //only copy or move inside allowed on root folder leaf
      leafId.charAt(0) == LEAF_TYPE_ROOT_FOLDER && $("#drop-menu .copy").hide() && $("#drop-menu .dropdown-item[data-action=" + DROP_COPY_INSIDE + "]").show();
    } else {
      $("#drop-menu .copy").hide();
      $("#drop-menu .move").show();
      //only copy or move inside allowed on root folder leaf
      leafId.charAt(0) == LEAF_TYPE_ROOT_FOLDER && $("#drop-menu .move").hide() && $("#drop-menu .dropdown-item[data-action=" + DROP_MOVE_INSIDE + "]").show();
    }
    switch (diff) {
      case - 1:
        $("#drop-menu .dropdown-item[data-action=" + DROP_MOVE_AFTER + "]").hide();
        break;

      case 1:
        $("#drop-menu .dropdown-item[data-action=" + DROP_MOVE_BEFORE + "]").hide();
        break;
    }
    //check if move inside allowed for same parent
    sameParent && !isCopy && !dropInsideAllowed(leafId) && $("#drop-menu .dropdown-item[data-action=" + DROP_MOVE_INSIDE + "]").hide();

    pos = $(this).position();
    $("#drop-menu").css({
      display: "block",
      top: pos.top + 15,
      left: pos.left + 20
    }).addClass("show").data("parent", leafId).data("parent_product", parentProduct).data("is_copy", isCopy);

    $(document).keyup(function (event) {
      if (event.keyCode === 27) {
        $("#drop-menu").removeClass("show").hide();
        $(document).off("keyup");
      }
    }).click(function () {
      $("#drop-menu").removeClass("show").hide();
      $(document).off("click");
    });
  });
}

function productsListener()
{
  treeListener();
  $("#context-menu a").on("click", function () {
    $(this).parent().removeClass("show").hide();
  });

  $("#context-menu .dropdown-item").click(function () {
    userRequest({xAction: "edit", action: $(this).data("action"), leafId: $(this).parent().data("leafid"), parentProduct: $(this).parent().data("parent")})
  });

  $("#drop-menu .dropdown-item").click(function () {
    parent = (data = $(this).parent()).data("parent");
    before = after = '';
    parentOrig = $("#" + dragELement).parents('li').first().attr("id");
    parentProduct = data.data("parent_product");
    if ((action = $(this).data("action")) != DROP_COPY_INSIDE && action != DROP_MOVE_INSIDE) {
      tree = parent.slice(2, -1).split('-');
      action == DROP_COPY_AFTER || action == DROP_MOVE_AFTER ? after = tree[tree.length - 1] : before = tree[tree.length - 1];
      tree = tree.slice(0, -1);
      parent = tree[tree.length - 1].toString().charAt(0) + '-' + tree.join('-') + '-';
      //parentProduct = $('#'+parentProduct).parents().filter("[id^=" + LEAF_TYPE_PRODUCT + "]").first().attr("id");
    }
    userRequest({xAction: "edit", action: LEAF_MENU_DROP, before, after, parentOrig, leafId: dragELement, parent, parentProduct, isCopy: data.data("is_copy")})
    $(this).parent().removeClass("show").hide();
  });
}

function dropInsideAllowed(leafId)
{
  dropType = leafId.charAt(0);
  dragType = dragELement.charAt(0);
  if (dragType == LEAF_TYPE_ROOT_FOLDER
          || dragType == LEAF_TYPE_MATERIAL && dropType <= LEAF_TYPE_FOLDER
          || dragType == LEAF_TYPE_FOLDER && dropType != LEAF_TYPE_ROOT_FOLDER && dropType <= LEAF_TYPE_PRODUCT_CHOICE
          || dragType == LEAF_TYPE_PRODUCT_CHOICE && dropType != LEAF_TYPE_PRODUCT) {
    return false;
  }

  //Avoid loop
  tree = leafId.slice(2, -1).split("-");
  //include direct children as well
  $("#" + leafId).children("ul").children("li").each(function () {
    ar = $(this).attr("id").slice(2, -1).split('-');
    tree.push(ar[ar.length - 1]);
  });
  for (i in tree) {
    if (all.find(function (elt) {
      return elt == tree[i];
    })) {
      //loop detected: a parent element of drop leaf is a children element of drag leaf
      return false;
    }
  }
  return true;
}

var TREE_REMOVE_LEAF = 1;
var TREE_ADD_AFTER = 2;
var TREE_ADD_BEGIN = 3;
var TREE_APPEND = 4;
var TREE_ADD_BEFORE = 5;
var TREE_LEAF_RENAME = 6;
var TREE_LEAF_SET_STYLE = 7;

function manageTree(operation, id, data)
{
  (global = operation > 100) && (operation -= 100);
  type = id.charAt(0);
  tree = id.slice(2, -1).split('-');
  switch (operation) {
    case TREE_LEAF_RENAME:
      $(".treeview li[id$='-" + tree[tree.length - 1] + "-']>a>span").text(data);
      return;

    case TREE_ADD_AFTER:
      search = (tree.length > 1 ? "-" + tree[tree.length - 2] : '') + "-" + tree[tree.length - 1] + "-";
      $(".treeview li[id$='" + search + "']").after(data);
      return;

    case TREE_APPEND:
      $(".treeview li[id$='-" + tree[tree.length - 1] + "-']>ul").append(data).prev().prev(":not(.down)").click();
      return;

    case TREE_REMOVE_LEAF:
      search = (tree.length > 1 ? "-" + tree[tree.length - 2] : '') + "-" + tree[tree.length - 1] + "-";
      $(".treeview li[id$='" + search + "']").remove();
      return;

    case TREE_ADD_BEFORE:
      search = (tree.length > 1 ? "-" + tree[tree.length - 2] : '') + "-" + tree[tree.length - 1] + "-";
      $(".treeview li[id$='" + search + "']").before(data);
      return;

    case TREE_LEAF_SET_STYLE:
      $(".treeview li[id$='-" + tree[tree.length - 1] + "-']>a>i").css(data[0], data[1]);

  }

}

function leafUpdated(attr)
{
  $("input[name='changed']").val('1');
  if (attr) {
    switch (attr.id.slice(3)) {
      case "unit":
        axExecute({xAction: "edit", action: "getUsageUnit", unit: $(attr).val()});
        break;

      case "isFinishedProduct":
        attr.checked && $("#ed_hasSerialNb").prop("disabled", false);
        !attr.checked && $("#ed_hasSerialNb").prop("disabled", true).prop("checked", false);
    }
  }
}

function setSelectData(id, data)
{
  $('#' + id).replaceWith(data);
  $("#ed_" + id).materialSelect();
}

function setUnitMaterial(obj, leafId)
{
  leafId += (val = $(obj).val()) + '-';
  $("[name='leafId']").val(leafId);
  axExecute({xAction: "edit", action: "buildUnitUsage", material: val});
}

//materials
function materialsListener()
{
  $(".lots").click(function () {
    axExecute({page: 'lots', xAction: 'show', ref: $(this).data('ref')});
    return false;
  });
  tableListener("materials");
}

//Production
function productionTableListener()
{
  $(".lots").click(function () {
    axExecute({page: 'lots', xAction: 'show', ref: $(this).data('ref')});
    return false;
  });
  tableListener("production");
}

//toSale
function toSaleListener()
{
  $(".lots").click(function () {
    axExecute({page: 'lots', xAction: 'show', ref: $(this).data('ref')});
    return false;
  });
  tableListener("toSale");
}

function productionListener()
{
  $('.treeview').mdbTreeview();
  //$('.toast').toast();

  $('.tree-leaf').on('click', function () {
    parentType = parentSelected = null;
    ($parent = $(this).parents('li').eq(1)).length && (parentType = $parent.attr("id").charAt(0)) && (parentSelected = $parent.children('a').children(".to-produce").length);
    //can't select or unselect product descandant of a selected product or product choice
    if (parentSelected && (parentType == LEAF_TYPE_PRODUCT || parentType == LEAF_TYPE_PRODUCT_CHOICE)) {
      return;
    }
    type = this.parentNode.id.charAt(0);
    //remove selection not allowed for folder
    if ($(this).children(".to-produce").length && type != LEAF_TYPE_FOLDER) {
      $(this).parent().find(".to-produce").removeClass("to-produce");
      buildProductName();
      return;
    }

    if (!parentSelected) {
      //unselect all is parent not selected (select outside branch
      $('.treeview .to-produce').removeClass("to-produce");
    } else {
      //unselect others if parent is folder
      $parent.filter("[id^='" + LEAF_TYPE_FOLDER + "']").find("ul").find(".to-produce").removeClass("to-produce");
    }

    otherChoice = $(this).parent().find("li").filter("[id^=" + LEAF_TYPE_FOLDER + "]").length;
    //select all except folder && product choice
    type != LEAF_TYPE_FOLDER && type != LEAF_TYPE_ROOT_FOLDER && type != LEAF_TYPE_PRODUCT_CHOICE && $(this).children().addClass("to-produce");
    //open if folder or at least one folder as children
    (type == LEAF_TYPE_FOLDER || type == LEAF_TYPE_PRODUCT_CHOICE || otherChoice) && $(this).prev(":not(.down)").click();
    //open all childs products with folders
    $(this).parent().find("[id]").each(function () {
      $parent = $(this).parents("li").eq(0);
      parentSelected = $parent.children('a').children(".to-produce").length;
      parenType = $parent.attr("id").charAt(0);
      if (parentSelected && (parenType == LEAF_TYPE_PRODUCT || parenType == LEAF_TYPE_PRODUCT_CHOICE)) {
        //open if other folder in product
        type = this.id.charAt(0);
        (type == LEAF_TYPE_FOLDER || $(this).find("li").filter("[id^=" + LEAF_TYPE_FOLDER + "]").length) && $(this).children("i:not(.down)").click();
        $(this).children('a').children().addClass("to-produce");
      }

    });
    buildProductName();

  });
  addFormListener("productionForm");
}

function buildProductName()
{
  complete = true;
  //Build product name
  leafs = [];
  $(".treeview .to-produce").parent().parent().each(function () {
    //parent is terminal => don't set as name
    if ($(this).parents().hasClass("is-terminal")) {
      return;
    }
    //remove useles leaf to determine name
    switch (type = this.id.charAt(0)) {
      case LEAF_TYPE_FOLDER:
        // check if terminated:
        complete && (complete = $(this).children("ul").find(".to-produce").length);
        //folder nor product choice never in name
        return;

      case LEAF_TYPE_PRODUCT:
      case LEAF_TYPE_PRODUCT_CHOICE:
        //no folder as child => set terminal
        !(hasFolder = $(this).find(".to-produce").parent().parent().filter("[id^=" + LEAF_TYPE_FOLDER + "]").length) && $(this).addClass("is-terminal");
        //if parent folder => get it as else ignore
        if (hasFolder || $(this).parents("li").first().filter(jQuery("[id^=" + LEAF_TYPE_FOLDER + "]").add("[id^=" + LEAF_TYPE_ROOT_FOLDER + "]")).length) {
          break;
        } else {
          return;
        }
    }
    $(this).find(["id$='-'" + LEAF_TYPE_FOLDER]).length
    leafs.push({
      tree: this.id.slice(2, -1).split('-'),
      isTerminal: $(this).hasClass("is-terminal"),
      position: $(this).parent().children().index(this),
      name: $(this).children("a").children("span").text(),
    });
  });
  //sort
  leafs.sort(function (a, b) {
    shortest = (aIsShortest = a.tree.length <= b.tree.length) ? a : b;
    for (i in shortest.tree) {
      if (a.tree[i] != b.tree[i]) {
        //get leafs at level
        toFind = a.tree.slice(0, i / 1 + 1).toString();
        leafA = leafs.find(function (obj) {
          return obj.tree.toString() === toFind;
        });
        toFind = b.tree.slice(0, i / 1 + 1).toString();
        leafB = leafs.find(function (obj) {
          return obj.tree.toString() === toFind;
        });
        return leafA && leafB ? leafA.position - leafB.position : 0;
      }
    }
    return aIsShortest ? -1 : 1;
  });
  //build name 
  productName = ''
  tree = [];
  productTree = [];
  //check if all choices has been made
  leafs.forEach(function (leaf) {
    productName += "-" + leaf.name;
    productTree.push(leaf.tree[leaf.tree.length - 1])
    leaf.isTerminal && tree.push(leaf.tree);
  });

  complete && (complete = productName);
  !(productName = productName.slice(1)) && (productName = '\u00A0');
  $("#toProduce").text(productName).css("color", complete ? 'black' : "red");
  $("#productionSubmit").prop("disabled", !complete);
  complete && $("[name='tree']").val(JSON.stringify(tree)) && $("[name='toProduce'").val(productName) && $("[name='productTree'").val(JSON.stringify(productTree));
}

function materialFormListener()
{
  $("#ed_provider").on("change", function () {
    leafUpdated();
    $(this).val() == 0 && axExecute({xAction: 'edit', page: "providers", action: 'insert'});
  });
}

function materialFormValidate()
{
  if ($("#ed_provider").val() === "0") {
    $("#ed_provider").val(null);
    $("#ed_provider").parent().find('.select-dropdown').val('').attr("placeholder", "Choisir fournisseur");
    return false;
  }
  return true;
}

function orderFormListener()
{
  addFormListener("orderForm");
  $("#ed_customer").on("change", function () {
    axExecute({xAction: 'edit', page: "customers", action: "choice", customer: $(this).val()});
  });
}

function orderFormValidate()
{
  if ($("#ed_customer").val() === "0") {
    $("#ed_customer").val(null);
    $("#ed_customer").parent().find('.select-dropdown').val('').attr("placeholder", "Choisir client");
    return false;
  }
  return true;
}

function currentSaleListener()
{
  tableListener("currentSale");
  $("#currentSale .form-check-input").change(function () {
    axExecute({page: "currentSale", xAction: "state", checked: this.checked ? 1 : 0, id: this.id});
  })
  $("#currentSale .order").click(function () {
    axExecute({page: "currentSale", xAction: "order", ref: $(this).data("ref")});
  });
}


var intervalTimer;
function timesheetListener()
{
  tableListener("timesheet");

  //select task following project
  $("#timesheetFormTable .mdb-select").materialSelect();
  $("#ed_activeDay").change(function () {
    pageAction("timesheet");
  })
  $("#ed_activeDay").parent().css({width: "10em"}).addClass("m-0 mt-1").children().first().css({color: "white"}).next().css({"font-size": "1.5em", color: "white"});

  //blink time
  intervalTimer && clearInterval(intervalTimer);
  started = ($time = $("#timesheet [data-started]")).data("started");
  typeof started !== "undefined" && (intervalTimer = setInterval(function () {
    $time.css("visibility") == "hidden" ? $time.css("visibility", "visible") : $time.css("visibility", "hidden");
    if (started[0] && (ellapsed = (dt = new Date()).getHours() * 60 + dt.getMinutes() - started[0] + started[1]) && Math.floor(dt.getSeconds()) % 60 == 0) {
      //Update value
      $time.text(("0" + Math.floor(ellapsed / 60)).slice(-2) + ':' + ('0' + (ellapsed % 60)).slice(-2));
      //totalDay
      totData = $("#tshTotalTime").data("work");
      ellapsed += totData;
      $("#tshTotalTime").children().eq(1).text(("0" + Math.floor(ellapsed / 60)).slice(-2) + ':' + ('0' + (ellapsed % 60)).slice(-2));
    }


  }, 500))
}

function timesheetFormListener()
{
  $("#ed_project").change(function () {
    axExecute({page: 'timesheet', xAction: 'edit', action: 'getTaskSelect', project: this.value}, null, function (data) {
      $("#ed_task").materialSelect({destroy: true});
      $("#ed_task").parent().replaceWith(data);
      $("#ed_task").materialSelect();
    });
  })
}

function timeValidate(field, min)
{
  var noError;
  !min && (noError = true) && (min = []);
  time = $(field).get(0);
  if (!time)
    return true;
  idx = time.value.replace('h', ':').indexOf(':');
  if (idx != -1 && time.value.slice(idx + 1) > 59) {
    setCustomValidity(time, "Minutes doit être inférieur à 60");
    return true;
  }
  idx != -1 && (min.push(time.value.slice(0, idx) * 60 + time.value.slice(idx + 1) / 1) || true) ||
          (idx = time.value.indexOf('.')) != -1 && (min.push(time.value.slice(0, idx) * 60 + time.value.slice(idx) * 60) || true) || min.push(time.value * 60)
  if (!(limit = $(time).data("hour"))) {
    !noError && time.setCustomValidity('');
    return;
  }

  if (min < limit[0] || min > limit[1]) {
    setCustomValidity(time, "Valeur en dehors des limites");
    time.setCustomValidity("error");
    return true;
  }
  !noError && time.setCustomValidity('');
  return;

}

var customValidity = [];
function setCustomValidity(element, message)
{
  $(element).nextAll(".invalid-feedback").hide().first().after("<div class=invalid-feedback>" + message + "</div>");
  element.setCustomValidity("Error");
  customValidity.push(element);
}

function usersFormValidate()
{
  timeValidate("#ed_workTime");
}

function compensationFormValidate()
{
  var min = [];
  var time = $('#ed_time').get(0);
  if (!timeValidate(time, min)) {
    if (min < 60) {
      setCustomValidity(time, "Minimum 1 heure de compensation");
      return;
    }
    time.setCustomValidity('');
  }


}

function timesheetFormValidate()
{
  var from = [];
  var to = [];
  var fromObj = $("#ed_timeFrom").get(0);
  var toObj = $("#ed_timeTo").get(0);
  if (!timeValidate(fromObj, from) && !timeValidate(toObj, to)) {
    var error = "L'heure d'arrêt doit être supérieur à l'heure de début";
    if (to[0] <= from[0]) {
      setCustomValidity(fromObj, error);
      setCustomValidity(toObj, error);
      return;
    }
    $("#timesheetForm [name='val_started']").val(from[0]);
    $("#timesheetForm [name='val_time']").val(to[0] - from[0]);
    fromObj.setCustomValidity('');
    toObj.setCustomValidity('');
    return;
  }
}

function workTimeFormValidate()
{
  timeValidate("#ed_dayTime");
  timeValidate("#ed_pauseTime");
}

function configTabCalendarListener()
{
  $('#calendarBox .mdb-select').materialSelect();

  $("#calendar .icon-action").click(function () {
    $(this).parent().append($("#calendarLeave"));
  });

  $("#calendarLeave a").click(function () {
    pageAction('calendar', {page: 'configTab', xAction: 'submit', action: 'leave', leave: $(this).data("leave"), day: $(this).parents("[data-day]").data("day")});
  });
}

