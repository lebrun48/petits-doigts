$('#tree-leaf').on('contextmenu', function(e) {
  pos = $(this).position();
  $("#context-menu").css({
    display: "block",
    top: pos.top+5,
    left: pos.left+10
  }).addClass("show");
  return false; //blocks default Webbrowser right click menu
}).on("click", function() {
  $("#context-menu").removeClass("show").hide();
});

$("#context-menu a").on("click", function() {
  $(this).parent().removeClass("show").hide();
});
