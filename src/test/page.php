<?php

include "site/htmlPage.php";

echo "<body class='grey lighten-5'>";
utils()->htmlHeaderLoaded = true;
echo "<div id=debug_info></div><div id=modals></div>";

if (function_exists("buildHeader")) {
  buildHeader();
}
include "test.php";
//include "testMain.php";

if (function_exists("buildFooter")) {
  buildFooter();
}

include "site/scriptPage.php";
utils()->scriptLoaded = true;
echo "</body>";

