<?php

class ConfigProject extends ConfigBase
{

  const SICK = 0;
  const PARTIAL = 1;
  const HOLYDAY = 2;
  const LEAVE = 3;
  const WE = 4;
  const WORK = 5;
  const EDUC = 6;

  public $leave = [
      self::SICK    => "Maladie",
      self::EDUC    => "Congé éducation",
      self::HOLYDAY => "Congé",
      self::LEAVE   => "Férié",
      self::WORK    => "Normal",
      self::PARTIAL => "Partiel",
      self::WE      => "Weekend",
  ];
  private $rolesName;
  public $workTime;
  
  function getRoles()
  {
    return self::get(CONFIG_ROLES)->getNameListById();
  }

  function __construct()
  {
    parent::__construct();
  }

  function getWorkTime($type = null)
  {
    !$this->workTime && $this->workTime = json_decode(dbUtil()->result(dbUtil()->selectRow("config", "val", "ri=" . CONFIG_WORKTIME), 0), JSON_OBJECT_AS_ARRAY);
    return $type ? $this->workTime[$type] : $this->workTime;
  }

  function saveWorkTime()
  {
    dbUtil()->insertRow("config", ["ri" => CONFIG_WORKTIME, "val" => json_encode($this->workTime)], "val=values(val)");
  }

  private function setCurrent($keyIdx)
  {
    $this->currentIdx = $keyIdx;
    switch ($keyIdx) {
      case CONFIG_ROLES:
        $this->currentAttr = &$this->rolesName;
        break;

      default:
        throw new Exception("Invalid keyIdx");
    }
    $this->buildAttr();
  }

  static function get($keyIdx = null): ConfigProject
  {
    static $config;
    !$config && $config = new ConfigProject();
    isset($keyIdx) && $config->setCurrent($keyIdx);
    return $config;
  }

}
