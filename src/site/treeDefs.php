<?php
//Unit production
define("LEAF_TYPE_ROOT_FOLDER", 1);
define("LEAF_TYPE_FOLDER", 2);
define("LEAF_TYPE_PRODUCT_CHOICE", 3);
define("LEAF_TYPE_PRODUCT", 4);
define("LEAF_TYPE_MATERIAL", 5);
define("LEAF_TYPE_FINISHED_PRODUCT", 6);

define("LEAF_FOLDER_PRODUCT", 0);
define("LEAF_FOLDER_BUILD_CHOICE", 1);
define("LEAF_FOLDER_ROOT", 2);
define("LEAF_FOLDER_PRODUCT_CHOICE", 3);

define("LEAF_MENU_EDIT", 1);
define("LEAF_MENU_DUPLICATE", 2);
define("LEAF_MENU_REMOVE", 3);
define("LEAF_MENU_NEW_FOLDER", 4);
define("LEAF_MENU_NEW_PRODUCT_CHOICE", 5);
define("LEAF_MENU_ADD_PRODUCT", 6);
define("LEAF_MENU_ADD_MATERIAL", 7);
define("LEAF_MENU_ADD_EXISTING_MATERIAL", 8);
define("LEAF_MENU_DROP", 9);

define("LEAF_FINISHED_PRODUCT", "#006400");

define("TREE_REMOVE_LEAF", 1);
define("TREE_ADD_AFTER", 2);
define("TREE_ADD_BEGIN", 3);
define("TREE_APPEND", 4);
define("TREE_ADD_BEFORE", 5);
define("TREE_LEAF_RENAME", 6);
define("TREE_LEAF_SET_STYLE", 7);
define("TREE_MOVE_LEAF", 8);

define ("DROP_COPY_BEFORE", 1);
define ("DROP_COPY_AFTER", 2);
define ("DROP_COPY_INSIDE", 3);
define ("DROP_MOVE_BEFORE", 4);
define ("DROP_MOVE_AFTER", 5);
define ("DROP_MOVE_INSIDE", 6);
