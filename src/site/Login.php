<?php

//Login
//Login
define('SITE_SECURE_ONLY', true);
define('SITE_ALWAYS_LOGIN', true);
define('SITE_LOGIN_TABLE_ID', "users");
define('SITE_LOGIN_KEY', "ri");
define('SITE_LOGIN_MAIL', "mail");

class Login extends LoginBase
{
  
  function __construct()
  {
    parent::__construct();
    $this->selectLoginAttr = "select ri,mail, roles, name, locked, firstName, workTime, partialTime, state, token, lastLogin, updpwd, nUpd, pwd from $this->loginTable where ";
  }

  function loginMailValidation($user)
  {
    $buildMail = new GenLoginMail($user, "validation_" . $user[SITE_LOGIN_KEY] . ".html", "loginValidation.html");
    $buildMail->run();

    if (!serviceConfig()->config["mail"]["test"] && utils()->isDevMode) {
      msgBox([
          MSGBOX_CONTENT    => utf8_encode(file_get_contents($buildMail->outFile)),
          MSGBOX_TITLE      => "Mail Envoyé à " . $user["name"],
          MSGBOX_SHOW_AFTER => true,
          MSGBOX_MODAL_ATTR => [MODAL_POS => MODAL_POS_LEFT, MODAL_DIR => MODAL_DIR_LEFT]
      ]);
      return;
    }

    new SendMail("Validation accès " . SITE_PROJECT . " pour " . $user["firstName"][0] . ". " . $user["name"], $user[SITE_LOGIN_MAIL], $buildMail->outFile);
  }

  function loginMailResetPwd($user)
  {
    $buildMail = new GenLoginMail($user, "resetPwd_" . $user[SITE_LOGIN_KEY] . ".html", "loginResetPwd.html");
    $buildMail->run();

    if (!serviceConfig()->config["mail"]["test"] && utils()->isDevMode) {
      msgBox([
          MSGBOX_CONTENT    => utf8_encode(file_get_contents($buildMail->outFile)),
          MSGBOX_TITLE      => "Mail Envoyé à " . $user["name"],
          MSGBOX_SHOW_AFTER => true,
          MSGBOX_MODAL_ATTR => [MODAL_POS => MODAL_POS_LEFT, MODAL_DIR => MODAL_DIR_LEFT]
      ]);
      return;
    }

    new SendMail("Nouveau mot de passe Caisse PR " . $user["prName"], $user[SITE_LOGIN_MAIL], $buildMail->outFile);
  }
}
