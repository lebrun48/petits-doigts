<?php

include "common/src/buildCompos.php";
include "common/src/tableDef.php";
include "treeDefs.php";

define("PRODUCTS_ID_LEN", 15);
define("PRODUCTS_NAME_LEN", 45);
define("MATERIAL_NAME_LEN", 45);
define("MATERIAL_BILL_NUM_LEN", 20);

define("SALE_TABLE_PRODUCTS", 0);
define("SALE_TABLE_STOCK", 1);

define("SALE_STATE_ORDER", 0);
define("SALE_STATE_REGISTERED", 1);
define("SALE_STATE_CHARGED", 2);
define("SALE_STATE_PAID", 4);
define("SALE_STATE_PRODUCT_SENT", 8);

define("OTHER_PARTY_FACTORY_LEN", 50);
define("OTHER_PARTY_CONTACT_LEN", 50);
define("OTHER_PARTY_EMAIL_LEN", 150);
define("OTHER_PARTY_ADDRESS_LEN", 150);
define("OTHER_PARTY_PHONE_LEN", 20);

function buildTableDefs()
{
  dbUtil()->tables["monthlyHours"] = [
      DB_TABLENAME    => "monthly_hours moh",
      DB_RIGHTS_ACCES => TABLE_RIGHT_ALL,
  ];

  dbUtil()->tables["calendar"] = [
      DB_TABLENAME => "calendar",
  ];

  dbUtil()->tables["config"] = [
      DB_TABLENAME    => "config",
      DB_RIGHTS_ACCES => TABLE_RIGHT_ALL
  ];

  dbUtil()->tables["users"] = [
      DB_TABLENAME     => "users_petitsdoigts usr",
      DB_RIGHTS_ACCES  => TABLE_RIGHT_ALL,
      DB_DEFAULT_WHERE => utils()->hasRootRole() ? null : "usr.ri>0",
  ];

  dbUtil()->tables["projects"] = [
      DB_TABLENAME    => "projects prj",
      DB_RIGHTS_ACCES => TABLE_RIGHT_ALL,
  ];

  dbUtil()->tables["tasks"] = [
      DB_TABLENAME    => "tasks tsk",
      DB_COL_PRIMARY  => "tsk.ri",
      DB_RIGHTS_ACCES => TABLE_RIGHT_ALL + TABLE_RIGHT_USER_ACTION,
  ];

  dbUtil()->tables["timesheet"] = [
      DB_TABLENAME    => "timesheet tsh",
      DB_RIGHTS_ACCES => TABLE_RIGHT_ALL,
  ];

  dbUtil()->tables["supTime"] = [
      DB_TABLENAME    => "sup_time",
      DB_RIGHTS_ACCES => TABLE_RIGHT_ALL,
  ];

  dbUtil()->tables["products"] = [
      DB_TABLENAME    => "products",
      DB_RIGHTS_ACCES => TABLE_RIGHT_ALL,
  ];
  dbUtil()->tables["stock"] = [
      DB_TABLENAME    => "stock",
      DB_RIGHTS_ACCES => TABLE_RIGHT_ALL,
  ];
  dbUtil()->tables["materials"] = [
      DB_TABLENAME         => "materials",
      DB_RIGHTS_ACCES      => TABLE_RIGHT_ALL,
      DB_NO_DEFAULT_GLOBAL => true
  ];
  dbUtil()->tables["providers"] = [
      DB_COLS_DEFINITION => [
          "ri" => [COL_PRIMARY => true, COL_HIDDEN => true],
      ],
      DB_TABLENAME       => "other_party",
      DB_DEFAULT_WHERE   => "type='P'",
      DB_DEFAULT_INSERT  => "type='P'",
  ];

  dbUtil()->tables["customers"] = [
      DB_COLS_DEFINITION => [
          "ri" => [COL_PRIMARY => true, COL_HIDDEN => true],
      ],
      DB_TABLENAME       => "other_party",
      DB_DEFAULT_WHERE   => "type='C'",
      DB_DEFAULT_INSERT  => "type='C'",
  ];

  dbUtil()->tables["production"] = [
      DB_RIGHTS_ACCES  => TABLE_RIGHT_DELETE,
      DB_DEFAULT_WHERE => "(stockType=" . LEAF_TYPE_PRODUCT . " or stockType=" . LEAF_TYPE_FINISHED_PRODUCT . ")",
  ];

  dbUtil()->tables["serialNb"] = [
      DB_TABLENAME => "serialnb",
  ];

  dbUtil()->tables["stockUsage"] = [
      DB_TABLENAME => "stock_usage",
  ];

  dbUtil()->tables["sales"] = [
      DB_COLS_DEFINITION => ["ri" => COL_PRIMARY, COL_HIDDEN => true],
      DB_TABLENAME       => "sales",
  ];

  dbUtil()->tables["orders"] = [
      DB_COLS_DEFINITION => [
          "ri"         => [COL_PRIMARY => true, COL_HIDDEN => true],
          "tva"        => [COL_HIDDEN => true],
          "quantity"   => [COL_TITLE => "Quantité", COL_TD_ATTR => "class=td-1"],
          "stockRef"   => [COL_TITLE => "Produit"],
          "serialNb"   => [COL_TITLE => "N° de série", COL_NO_DB => true],
          "price"      => [COL_TITLE => "Prix unitaire HTVA", COL_TYPE => "money", COL_TH_ATTR => "class=text-right"],
          "totalPrice" => [COL_TITLE => "Prix total HTVA", COL_NO_DB => true, COL_TYPE => "money", COL_TH_ATTR => "class=text-right"],
      ],
      DB_TABLENAME       => "orders",
      DB_RIGHTS_ACCES    => TABLE_RIGHT_DELETE,
  ];

  dbUtil()->tables["toSale"] = [
      DB_COLS_DEFINITION => [
          "stockRef" => [COL_NO_DB => true, COL_PRIMARY => true, COL_HIDDEN => true],
          "size"     => [COL_NO_DB => true, COL_PRIMARY => true, COL_HIDDEN => true],
          "name"     => [COL_TITLE => "Nom", COL_TD_ATTR => "class=td-1"],
          "stock"    => [COL_TITLE => "Stock", COL_NO_DB => true],
          "serialNb" => [COL_TITLE => "N° série", COL_NO_DB => true],
          "sales"    => [COL_TITLE => "Ventes", COL_NO_DB => true],
          "folder"   => [COL_GROUP => 0, COL_TD_ATTR => "colspan=6 class=group-col", COL_GROUP_ORDER_DIRECTION => "desc"],
      ],
      DB_RIGHTS_ACCES    => TABLE_RIGHT_USER_ACTION
  ];

  dbUtil()->tables["currentSale"] = [
      DB_COLS_DEFINITION => [
          "customer"              => [
              COL_TITLE        => "Client",
              COL_EXT_REF      => "customers",
              COL_DB           => "contact",
              COL_GROUP        => 0,
              COL_TD_ATTR      => "colspan=7 class=group-col",
              COL_GROUP_ACTION => "updCustomer"
          ],
          "factory"               => [COL_HIDDEN => true, COL_EXT_REF => "customers", COL_DB => "factory"],
          "date"                  => [COL_TITLE => "Date", COL_TYPE => "date[d/m/Y]", COL_TD_ATTR => "class=td-1"],
          "id"                    => [COL_TITLE => "ID", COL_TH_ATTR => "class=text-right", COL_TD_ATTR => "class=text-right", COL_NO_DB => true],
          "ri"                    => [COL_PRIMARY => true, COL_HIDDEN => true],
          "amount"                => [COL_TITLE => "Montant HTVA", COL_NO_DB => true, COL_TYPE => "money", COL_TH_ATTR => "class=text-right"],
          "ttc"                   => [COL_TITLE => "TTC", COL_NO_DB => true, COL_TYPE => "money", COL_TH_ATTR => "class='text-right pr-3'", COL_TD_ATTR => "class=pr-3"],
          "state"                 => [COL_HIDDEN => true],
          SALE_STATE_CHARGED      => [COL_TITLE => "Facturé", COL_NO_DB => true],
          SALE_STATE_PAID         => [COL_TITLE => "Payé", COL_NO_DB => true],
          SALE_STATE_PRODUCT_SENT => [COL_TITLE => "Délivré", COL_NO_DB => true],
      ],
      DB_TABLENAME       => "sales",
      DB_RIGHTS_ACCES    => TABLE_RIGHT_DELETE
  ];
}
