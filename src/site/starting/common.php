<?php

define('NL', "<br>");
define('nl', "\n");

$include_path = ""
        . PATH_SEPARATOR . serviceConfig()->config["path"]["project_full_path"] . '/' . serviceConfig()->config["path"]["sources_path"] . "/site"
        . PATH_SEPARATOR . serviceConfig()->config["path"]["project_full_path"] . "/plugin"
        . PATH_SEPARATOR . (($tmp = serviceConfig()->config["path"]["plugin_path"])[0] == '/' ? serviceConfig()->config["path"]["include_path"] . "$tmp" : "$srv/$tmp");
set_include_path(get_include_path() . $include_path);

include "defines.php";
include "main.php";
include "common/src/Utils.php";
Login::get()->loginOptions = LOGIN_OPTION_NO_PWD | LOGIN_OPTION_ALWAYS_REMEMBER_ME;
utils()->projectName = SITE_PROJECT;
utils()->checkRequest();
