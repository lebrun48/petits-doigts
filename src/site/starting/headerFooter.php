<?php

define("MENU_USERS", 'users');
define("MENU_PRODUCTS", 'products');
define("MENU_MATERIALS", 'materials');
define("MENU_PRODUCTION", 'production');
define("MENU_SALE", 'sales');
define("MENU_LOGOUT", "logout");
define("MENU_TIMESHEET", "timesheet");

function buildHeader()
{
  echo ""
  . "<header id=header>"
  . "  <h4 style=overflow-x:auto class='h4 text-center text-nowrap my-lg-3'>&laquo;Les petits doigts&raquo;</h4>"
  . "  <p class='my-0 text-right' style=font-size:15px class=ml-2>(" . utils()->userSession()["firstName"] . " " . strtoupper(utils()->userSession()["name"]) . ")</p>";
  buildMenu();
  echo ""
  . "</header>";
}

function buildMenu()
{
  include "common/src/NavBar.php";
  $menu = [
      NAV_LEFT_COLLAPSE_MENU  => [
          [
              NAV_LINK_ATTR  => "href=?setPage=users",
              NAV_LINK_TITLE => "<i class='fas fa-user-lock mr-1'></i>Utilisateurs",
          ], [
              NAV_LINK_ATTR  => "href=?setPage=projects",
              NAV_LINK_TITLE => "<i class='fas fa-user-lock mr-1'></i>Projets",
          ]
      ],
      NAV_RIGHT_COLLAPSE_MENU => [
          [
              NAV_LINK_ATTR  => "onclick=axExecute('logout')",
              NAV_LINK_TITLE => "<i class='fas fa-sign-out-alt mr-1'></i>'</i>Déconnexion",
          ]
      ]
  ];
  if (!utils()->hasUserRole(ROLE_ADMIN)) {
    unset($menu[NAV_LEFT_COLLAPSE_MENU][0]);
  }
  utils()->isRoot && $menu[NAV_RIGHT_COLLAPSE_MENU][] = NavBar::getMenuRoot();
    
  (new NavBar($menu))->buildNavBar("lg", "class=navbar-light");
}
