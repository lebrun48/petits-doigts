<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Production Les petits doigts</title>
  <!-- petits-doigts icon -->
  <link rel="icon" href="img/logo.png" type="image/x-icon">
  
  <?php
  
  Plugin::buildIncludeCSSs();
  Plugin::staticBuildIncludeCSS(SITE_PROJECT, utils()->projectRootPath."/include/page.css", utils()->isDevMode);
  
  utils()->htmlHeaderLoaded = true;
  ?>

</head>

