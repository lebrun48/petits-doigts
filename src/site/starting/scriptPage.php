<i id=iconWait class='fas fa-spinner fa-pulse <?php echo SITE_DEFAULT_TEXT_COLOR?> fa-2x' style='display:none; z-index:2000; position:fixed; top: 50%; left: 50%;'></i>

<!-- Your custom scripts (optional) -->
<?php

Plugin::buildIncludeScripts();
Plugin::staticBuildIncludeScript(SITE_PROJECT, utils()->projectRootPath."/include/page.js", utils()->isDevMode);
  
utils()->scriptLoaded = true;

utils()->buildReadyFunctions();
?>
