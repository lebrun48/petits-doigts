<?php

include "htmlPage.php";

echo "<body class='grey lighten-5'>";
echo "<div id=debug_info></div><div id=modals></div>";

if (!utils()->hasSession()) {
  Login::get()->userLogin("Connexion &laquo;Petits-doigts&raquo;"
          , utils()->requestRootPath . "/img/respectable-logo_9303056e.jpg"
          , SITE_DEFAULT_COLOR
          , LOGIN_OPTION_ALWAYS_REMEMBER_ME | LOGIN_OPTION_NO_PWD);
}
else {
  echo "<div class=container>";
  include "headerFooter.php";
  if (function_exists("buildHeader")) {
    buildHeader();
  }
  $main = utils()->getCurrentPageName() . "Main";
  new $main;

  if (function_exists("buildFooter")) {
    buildFooter();
  }
  utils()->insertIncludeScripts("page");
  echo "</div>";
}

include "scriptPage.php";
echo "</body>";

