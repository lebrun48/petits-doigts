<?php

class usersMain
{

  public function __construct()
  {
    echo ""
    . "<main id='main'>";
    buildUsersBox();
    echo ""
    . "</main>";
  }

}

class Users extends BuildTable
{

  public function __construct()
  {
    dbUtil()->tables["users"][DB_COLS_DEFINITION] = [
        "name"   => [
            COL_TITLE   => "Prénom NOM",
            COL_DB      => "concat(firstName, ' ', upper(name))",
            COL_TD_ATTR => "class=td-1"
        ],
        "mail"   => [COL_TITLE => "E-Mail", COL_MOVE_ON_HIDE => [COL_MOVE_TO_KEY => "name"], COL_HIDE_ON => "lg"],
        "roles"  => [COL_TITLE => "Roles"],
        "locked" => [COL_TITLE => "Bloquer", COL_TD_ATTR => "class='font-weight-bold text-center'"],
        "ri"     => [COL_HIDDEN => true, COL_PRIMARY => true]
    ];
    $this->theadAttr = "class='grey lighten-4'";
    $this->defaultOrderLast = "name asc";
    $this->settings[TABLE_ATTR_BEFORE_TABLE_ATTR] = "class=pb-3";
    parent::__construct("users");
    $this->buildFullTable();
  }

  protected function DBupdate()
  {
    if (!utils()->action)
      return;

    dbUtil()->begin_transaction();
    //no part time for root
    //$_REQUEST["primary"][0] && $this->buildPartialTimeCalendar();

    is_array($_REQUEST["val_roles"]) && $_REQUEST["val_roles"] = '-' . implode('-', $_REQUEST["val_roles"]) . '-';
    $t = $_REQUEST["val_workTime"];
    ($min = strpos(str_replace("h", ":", $t), ':')) && ($_REQUEST["val_workTime"] = substr($t, 0, $min) * 60 + substr($t, $min + 1)) ||
            $_REQUEST["val_workTime"] = strtok($t, ".") * 60 + ("0." . strtok('')) * 60;

    try {
      parent::DBupdate();
    } catch (Exception $exc) {
      if ($exc->getCode() == E_DB_DUPLICATE) {
        msgBox("l'adresse mail &laquo;" . $_REQUEST["val_mail"] . "&raquo; existe déjà", "Email existe", MODAL_SIZE_SMALL, [MSGBOX_MODAL_ATTR => [MODAL_CENTERED => true, MODAL_NO_BACKDROP => true]]);
        exit();
      }
      throw $exc;
    }
    dbUtil()->commit();
  }

  protected function buildPartialTimeCalendar()
  {
    for ($i = 1; $i < 5; $i++) {
      ($v = $_REQUEST["day$i"]) && $partTime[$i] = $v;
    }
    $_REQUEST["val_partialTime"] = $partTime ? json_encode($partTime) : "";
    ($rowPartTime = dbUtil()->getCurrentEditedRow()["partialTime"]) && $rowPartTime = json_decode($rowPartTime, JSON_OBJECT_AS_ARRAY);

    if (($ne = $partTime != $rowPartTime) && (!$_REQUEST["val_partialDate"] ||
            ($begin = DateTime::createFromFormat("Y-m-d", $_REQUEST["val_partialDate"])) <= utils()->now ||
            $begin->format("N") != 1)
    ) {
      msgBox("<p>Pour tout changement du temps partiel, la date de prise en compte doit être un lundi dans le futur.</p>", null, MODAL_SIZE_SMALL,
             [MSGBOX_MODAL_ATTR => [MODAL_CENTERED => true, MODAL_NO_BACKDROP => true]]);
      exit();
    }
    $_REQUEST["val_partialDate"] = str_replace("-", '', $_REQUEST["val_partialDate"]);
    if ($ne) {
      if ($rowPartTime) {
        foreach ($rowPartTime as $day => $part) {
          if (!$partTime[$day]) {
            //delete current
            $weekDays[] = $day;
          }
        }
        $weekDays && dbUtil()->deleteRow("calendar"
                        , "weekDay in (" . implode(',', $weekDays) . ") and day>=" . $_REQUEST["val_partialDate"] . " and worker=" . $_REQUEST["primary"][0] . " and reason=" . ConfigProject::PARTIAL);
      }
      if ($partTime) {
        foreach ($partTime as $day => $part) {
          if ($rowPartTime[$day] && $rowPartTime[$day] != $part) {
            //update current
            dbUtil()->updateRow("calendar", ["partial" => $part]
                    , "weekDay=$day and day>=" . $_REQUEST["val_partialDate"] . " and worker=" . $_REQUEST["primary"][0] . " and reason=" . ConfigProject::PARTIAL);
          }
        }
        foreach ($partTime as $day => $part) {
          if (!$rowPartTime[$day]) {
            //insert new
            $dt = clone $begin;
            $year = utils()->now()->format("Y");
            $dt->add(new DateInterval("P" . ($day - 1) . "D"));
            while ($dt->format("Y") == $year) {
              $insert .= ", (" . $dt->format("Ymd") . ",$day,$year,$part," . ConfigProject::PARTIAL . "," . $_REQUEST["primary"][0] . ")";
              $dt->add(new DateInterval("P7D"));
            }
          }
        }
        $insert && dbUtil()->query("insert into calendar (day,weekDay,year,partial,reason,worker) values " . substr($insert, 2) . " on duplicate key update day=day");
      }
    }
  }

  protected function getPagging($hideOn = null)
  {
    return""
            . "<div class='btn-group dropleft' id=localMenuDef>"
            . "  <i class='fa-lg fas fa-cog' data-toggle='dropdown'></i>"
            . "  <div class='dropdown-menu dropdown-dark'>"
            . "    <a class='dropdown-item' onclick=pageAction('configTab',{active:'roles'})>Rôles</a>"
//            . "    <a class='dropdown-item' onclick=pageAction('configTab',{active:'workTime',xAction:'edit'})>Temps de travail</a>"
            . "  </div>"
            . "</div>";
  }

  protected function getDisplayValue($key, $row)
  {
    switch ($key) {
      case "roles":
        $roles = explode('-', substr($row["roles"], 1, -1));
        foreach ($roles as $role) {
          $dispVal .= ", " . ConfigProject::get(CONFIG_ROLES)->getNameById($role);
        }
        return substr($dispVal, 2);

      case "locked":
        return $row[$key] ? "X" : '';
    }
    return parent::getDisplayValue($key, $row);
  }

}

function buildUsersBox()
{
  echo ""
  . "<div id=usersBox class='card mt-3'>"
  . "  <div class=card-body>"
  . "    <h5 class='text-center card-title m-0'>Mise à jour des Utilisateurs</h5>";

  new Users();

  echo ""
  . "  </div>"
  . "</div>";
  utils()->insertReadyFunction("tableListener", "users");
}
