<?php

class RolesEdit
{

  public function __construct()
  {
    utils()->action == "update" && $row = ConfigProject::get(CONFIG_ROLES)->getByIndex($_REQUEST["primary"][0]);

    $fields = [
        "!name" => [
            ED_LABEL       => "Nom",
            ED_TYPE        => ED_TYPE_ALPHA,
            ED_VALIDATE    => [
                ED_VALIDATE_REQUIRED   => true,
                ED_VALIDATE_MAX_LENGTH => 50,
                ED_VALIDATE_INVALIDE   => "Manquant"
            ],
            ED_FIELD_WIDTH => 12,
        ],
        "!id"   => [ED_TYPE => ED_TYPE_HIDDEN]
    ];

    ConfigProject::get()->addSystemEdField($fields, 12);
    msgBox(BuildForm::getForm($fields, $row), "Edition Rôle", null, [
        MSGBOX_BUTTON_ACTION => "Enregistrer",
        MSGBOX_BUTTON_CLOSE  => "Annuler",
        MSGBOX_MODAL_ATTR    => [MODAL_NO_BACKDROP => true]
    ]);
  }

}
