<?php

class configTabMain
{

  public function __construct()
  {
    if (!utils()->action) {
      obStart();
      buildConfigTabBox();
      MsgBox::buildModal([MODAL_BODY => ob_get_clean(), MODAL_TITLE => "Rôles", MODAL_ID => "modalRoles"]);
      utils()->axExecuteJS("msgBoxShow", "data");
      utils()->axExecuteJS("configTabListener");
      return;
    }
    utils()->axExecuteJS("msgBoxClose", "msgbox");
    utils()->axRefreshElement("configTab", false);
    utils()->axExecuteJS("configTabListener");
    return;
  }

}

function axConfigTabSubmit()
{
  switch (utils()->getPermanentValue("active", "configTab")) {
    case "roles":
      new configTabMain();
      return;

    case "workTime":
      ConfigProject::get()->workTime = [
          CONFIG_WORKTIME_DAYTIME    => utils()->translateFromEdTime($_REQUEST[CONFIG_WORKTIME_DAYTIME]),
          CONFIG_WORKTIME_PAUSETIME  => utils()->translateFromEdTime($_REQUEST[CONFIG_WORKTIME_PAUSETIME]),
          CONFIG_WORKTIME_MAXSUPTIME => utils()->translateFromEdTime($_REQUEST[CONFIG_WORKTIME_MAXSUPTIME])
      ];
      ConfigProject::get()->saveWorkTime();
      utils()->axExecuteJS("msgBoxClose");
  }
}

function axConfigTabEdit()
{
  switch (utils()->getPermanentValue("active", "configTab")) {
    case "roles":
      if (utils()->action == "delete") {
        $id = ConfigProject::get(CONFIG_ROLES)->getPropByIndex($_REQUEST["primary"][0], "id");
        if (mysqli_num_rows(dbUtil()->selectRow("users", "ri", "roles like '%-$id-%' limit 1"))) {
          msgBox("<p>Le rôle «<b>" . ConfigProject::get()->getNameById($id) . "</b>» est assigné à au moins 1 utilisateur!</p>"
                  . "<p>Suppression annulée.</p>", null, MODAL_SIZE_SMALL, [MSGBOX_MODAL_ATTR => [MODAL_NO_BACKDROP => true]]
          );
          return;
        }
        if (mysqli_num_rows(dbUtil()->selectRow("tasks", "ri", "role=$id limit 1"))) {
          msgBox("<p>Le rôle «<b>" . ConfigProject::get()->getNameById($id) . "</b>» est utilisé dans au moins 1 projet!</p>"
                  . "<p>Suppression annulée.</p>", null, MODAL_SIZE_SMALL, [MSGBOX_MODAL_ATTR => [MODAL_NO_BACKDROP => true]]);
          return;
        }
        msgBox("$msg<p>Confirmation suppression rôle.</p>", "Confirmation", null, [
            MSGBOX_BUTTON_ACTION => "Supprimer",
            MSGBOX_BUTTON_CLOSE  => "annuler",
            MSGBOX_MODAL_ATTR    => [MODAL_NO_BACKDROP => true, MODAL_ID => "msgbox"]
        ]);
        return;
      }

      new RolesEdit();
      return;

    case "workTime":
      new WorkTimeEdit();
      return;
  }
}

function buildConfigTabBox()
{
  echo ""
  . "<div id=configTabBox>";
  new Roles();
  echo ""
  . "</div>";
}
