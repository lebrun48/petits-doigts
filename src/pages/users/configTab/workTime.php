<?php

class Roles extends ConfigTable
{

  protected function buildTableDef()
  {
    $this->colDefinition = [
        "index" => [
            COL_HIDDEN  => true,
            COL_PRIMARY => true,
        ],
        "id"    => [
            COL_TITLE => "id"
        ],
        "name"  => [
            COL_TITLE      => "Nom",
            COL_TD_ACTIONS => true,
        ],
    ];
    if (!utils()->hasRootRole()) {
      unset($this->colDefinition["id"]);
    }
    ConfigProject::get()->addSystemField($this->colDefinition);
    dbUtil()->tables["configTab"] = [
        DB_COLS_DEFINITION => $this->colDefinition,
        DB_RIGHTS_ACCES    => TABLE_RIGHT_EDIT | TABLE_RIGHT_DELETE | TABLE_RIGHT_USER_ACTION
    ];
  }

  function __construct()
  {
    $this->buildTableDef();

    parent::__construct("configTab");
    unset($this->settings[TABLE_ATTR_TABLE_RESPONSIVE]);
    $this->buildFullTable();
  }

  protected function buildBeforeTable()
  {
    
  }

  protected function getConfigObject(): ConfigBase
  {
    return ConfigProject::get(CONFIG_ROLES);
  }

  protected function getConfigRow()
  {
    return [
        "id"     => $_REQUEST["id"],
        "name"   => $_REQUEST["name"],
        "system" => $_REQUEST["system"]
    ];
  }

}
