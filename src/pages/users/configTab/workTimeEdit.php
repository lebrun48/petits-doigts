<?php

class WorkTimeEdit
{

  public function __construct()
  {
    $row = ConfigProject::get()->getWorkTime();

    $fields = [
        "!".CONFIG_WORKTIME_DAYTIME   => [
            ED_LABEL                => "Prestation par jour",
            ED_TYPE                 => ED_TYPE_ALPHA,
            ED_TEXT_SELECT_ON_FOCUS => true,
            ED_ATTR                 => 'autocomplete=off pattern=\d{1,2}((:|h|\.)\d{1,2})?$',
            ED_VALUE                => utils()->translateToEdTime($row[CONFIG_WORKTIME_DAYTIME]),
            ED_VALIDATE             => [ED_VALIDATE_INVALIDE => "Doit correspondre au format h:m ou h.d (ex 5:15, 5h15, 5.25, 2)"],
            ED_FIELD_WIDTH          => 12
        ],
        "!".CONFIG_WORKTIME_PAUSETIME => [
            ED_LABEL                => "Temps pause par jour",
            ED_TYPE                 => ED_TYPE_ALPHA,
            ED_TEXT_SELECT_ON_FOCUS => true,
            ED_ATTR                 => 'autocomplete=off pattern=\d{1,2}((:|h|\.)\d{1,2})?$',
            ED_VALUE                => utils()->translateToEdTime($row[CONFIG_WORKTIME_PAUSETIME]),
            ED_VALIDATE             => [ED_VALIDATE_INVALIDE => "Doit correspondre au format h:m ou h.d (ex 5:15, 5h15, 5.25, 2)"],
            ED_FIELD_WIDTH          => 12
        ],
        "!".CONFIG_WORKTIME_MAXSUPTIME => [
            ED_LABEL                => "Max heure sup. par jour",
            ED_TYPE                 => ED_TYPE_ALPHA,
            ED_TEXT_SELECT_ON_FOCUS => true,
            ED_ATTR                 => 'autocomplete=off pattern=\d{1,2}((:|h|\.)\d{1,2})?$',
            ED_VALUE                => utils()->translateToEdTime($row[CONFIG_WORKTIME_MAXSUPTIME]),
            ED_VALIDATE             => [ED_VALIDATE_INVALIDE => "Doit correspondre au format h:m ou h.d (ex 5:15, 5h15, 5.25, 2)"],
            ED_FIELD_WIDTH          => 12
        ]
    ];

    msgBox(BuildForm::getForm($fields, $row, "workTime"), "Edition temps", null, [
        MSGBOX_BUTTON_ACTION => "Enregistrer",
        MSGBOX_BUTTON_CLOSE  => "Annuler",
    ]);
  }

}
