<?php

class UsersEdit
{

  private $arForm;

  function __construct()
  {
    ($row = dbUtil()->getCurrentEditedRow()) && ($row["roles[]"] = explode("-", $row["roles"])) &&
            $row["partialTime"] && $row["partialTime"] = json_decode($row["partialTime"], JSON_OBJECT_AS_ARRAY);
    $row["partialDate"] && $row["partialDate"] = substr($row["partialDate"], 0, 4) . "-" . substr($row["partialDate"], 4, 2) . "-" . substr($row["partialDate"], 6);
    if (utils()->action == 'delete') {
      if ($row["ri"] == 0) {
        msgbox("Jack, tu peux pas supprimer 'root'!");
        exit();
      }
      return;
    }


    //insert or update
    $roles = utils()->roles;

    $this->arForm = [
        ED_FORM_ATTR => "class='px-3 nofocus'",
        "firstName"  => [
            ED_TYPE        => ED_TYPE_ALPHA,
            ED_LABEL       => "Prénom",
            ED_VALIDATE    => [
                ED_VALIDATE_INVALIDE   => "Le Prénom est requis",
                ED_VALIDATE_REQUIRED   => true,
                ED_VALIDATE_MAX_LENGTH => 20,
            ],
            ED_FIELD_WIDTH => 12
        ],
        "name"       => [
            ED_TYPE        => ED_TYPE_ALPHA,
            ED_LABEL       => "Nom",
            ED_VALIDATE    => [
                ED_VALIDATE_INVALIDE   => "Le NOM est requis",
                ED_VALIDATE_REQUIRED   => true,
                ED_VALIDATE_MAX_LENGTH => 20,
            ],
            ED_FIELD_WIDTH => 12
        ],
        "mail"       => [
            ED_TYPE        => "email",
            ED_LABEL       => "E-Mail adresse",
            ED_VALIDATE    => [
                ED_VALIDATE_INVALIDE   => "Le caractère '@' est requis",
                ED_VALIDATE_MAX_LENGTH => "50",
                ED_VALIDATE_REQUIRED   => true
            ],
            ED_FIELD_WIDTH => 12
        ],
        "roles[]"    => [
            ED_TYPE            => ED_TYPE_SELECT,
            ED_LABEL           => "Rôles",
            ED_SELECT_MULTIPLE => true,
            ED_PLACEHOLDER     => '',
            ED_OPTIONS         => mdbCompos()->getOptions(ConfigProject::get(CONFIG_ROLES)->getNameListById()),
            ED_VALIDATE        => [ED_VALIDATE_REQUIRED => true],
            ED_FIELD_WIDTH     => 8,
            ED_ATTR            => "class=mb-5"
        ],
        "locked"     => [
            ED_TYPE        => ED_TYPE_CHECK,
            ED_VALUE       => 1,
            ED_LABEL       => "Bloquer",
            ED_FIELD_WIDTH => 4,
        ],
    ];
//    $this->buildPartial($row);

    if (utils()->hasRootRole()) {
      $this->arForm["allowTrace"] = [ED_TYPE => ED_TYPE_CHECK, ED_LABEL => "view Trace on Prod", ED_VALUE => '1'];
    }


    msgBox([
        MSGBOX_TITLE         => ($row ? "Modification" : "Création") . " utilisateur",
        //MSGBOX_SIZE          => MODAL_SIZE_SMALL,
        MSGBOX_CONTENT       => BuildForm::getForm($this->arForm, $row, "usersForm"),
        MSGBOX_BUTTON_ACTION => "Enregistrer",
        MSGBOX_BUTTON_CLOSE  => "Annuler",
        MSGBOX_MODAL_ATTR    => [MODAL_SCROLLABLE => true]
    ]);
  }

  private function buildPartial($row)
  {
    $this->arForm["!html"] = [
        ED_INSERT_HTML => "<div class='z-depth-1 text-center pl-3 pt-2'><h5 class=h5>Temps partiel</h2>"
    ];
    $this->arForm["partialDate"] = [
        ED_TYPE        => "date",
        ED_LABEL       => "A partir du",
        ED_FIELD_WIDTH => 6,
        ED_NEXT_LINE   => true,
    ];
    $days = [1 => "Lu.", "Ma.", "Me.", "Je.", "Ve."];
    foreach ($days as $i => $day) {
      $this->arForm += [
          "!day$i" => [
              ED_VALUE       => $row["partialTime"][$i],
              ED_TYPE        => ED_TYPE_SELECT,
              ED_LABEL       => $day,
              ED_OPTIONS     => mdbCompos()->getOptions([null => "Plein", "1" => "Absent", "0.5" => "1/2 J."]),
              ED_FIELD_WIDTH => 2
          ]
      ];
    }
    $this->arForm["!html1"] = [ED_INSERT_HTML => "</div>"];
  }

}
