<?php

class MaterialEdit
{

  function getMaterialsList(Leaf $leaf)
  {
    $leaf && $list = Leaf::getChildrenByType(LEAF_TYPE_MATERIAL, $leaf->content["children"], ',');
    $res = dbUtil()->selectRow("materials", "ri, name", ($list ? "ri not in ($list)" : '1') . " order by name");
    while ($tup = dbUtil()->fetch_row($res)) {
      $options[] = [ED_CONTENT => $tup[1], ED_VALUE => LEAF_TYPE_MATERIAL . "$tup[0]"];
    }
    return $options;
  }

  function getProviders()
  {
    $options[] = [ED_VALUE => 0, ED_CONTENT => "(nouveau fournisseur)"];
    $options = array_merge($options, mdbCompos()->getOptions(dbUtil()->selectRow("providers", "ri, factory", "1 order by factory")));
    return $options;
  }

  function __construct(Leaf $leaf)
  {
    dbUtil()->tables["stock"][DB_COLS_DEFINITION] = [
        "name"     => [COL_TITLE => "Nom", COL_TD_ATTR => "class=td-1"],
        "ri"       => [COL_PRIMARY => true, COL_HIDDEN => true],
        "stock"    => [COL_TITLE => "Stock", COL_NO_DB => true],
        "provider" => [
            COL_GROUP        => 0,
            COL_GROUP_ACTION => "updProvider",
            COL_TD_ATTR      => "colspan=4 class=group-col",
            COL_EXT_REF      => "providers",
            COL_DB           => "factory"
        ],
        "ref"      => [COL_TITLE => "Réf. matériau"],
    ];

    switch (utils()->action) {
      case LEAF_MENU_DROP:
      case LEAF_MENU_EDIT:
        $row = $leaf->content;
        $usage = json_decode($leaf->parent["childrenUsage"], true)[$leaf->ri];
      case LEAF_MENU_ADD_EXISTING_MATERIAL :
      case LEAF_MENU_ADD_MATERIAL:
        $arForm = [
            ED_FORM_ATTR => "class='px-3'",
            "!changed"   => [ED_TYPE => ED_TYPE_HIDDEN, ED_VALUE => 0],
            "!type"      => [ED_TYPE => ED_TYPE_HIDDEN, ED_VALUE => LEAF_TYPE_MATERIAL],
            "!material"  => [
                ED_TYPE        => ED_TYPE_SELECT,
                ED_OPTIONS     => utils()->action == LEAF_MENU_ADD_EXISTING_MATERIAL ? $this->getMaterialsList($leaf) : '',
                ED_PLACEHOLDER => "Choisir le matériau",
                ED_LABEL       => "Liste matériaux",
                ED_ATTR        => "searchable=Chercher... onchange=setUnitMaterial(this,'" . LEAF_TYPE_MATERIAL . substr($leaf->leafId, 1) . "')",
                ED_VALIDATE    => [ED_VALIDATE_REQUIRED]
            ],
            "!parent"    => [ED_TYPE => ED_TYPE_HIDDEN, ED_VALUE => $leaf->leafId],
            "!leafId"    => [ED_TYPE => ED_TYPE_HIDDEN],
            "name"       => [
                ED_FIELD_WIDTH => 12,
                ED_TYPE        => ED_TYPE_ALPHA,
                ED_ATTR        => "onchange=leafUpdated()",
                ED_LABEL       => "name",
                ED_VALIDATE    => [
                    ED_VALIDATE_INVALIDE   => "Le nom est requis",
                    ED_VALIDATE_REQUIRED   => true,
                    ED_VALIDATE_MAX_LENGTH => MATERIAL_NAME_LEN,
                ]
            ],
            "provider"   => [
                ED_FIELD_WIDTH => 6,
                ED_DIV_ATTR    => "id=provider",
                ED_ATTR        => "searchable=Chercher...",
                ED_TYPE        => ED_TYPE_SELECT,
                ED_LABEL       => "Fournisseur",
                ED_PLACEHOLDER => "Choisir fournisseur",
                ED_OPTIONS     => $this->getProviders(),
                ED_VALIDATE    => [ED_VALIDATE_REQUIRED => true, ED_VALIDATE_INVALIDE => "Le fournisseur est requis"]
            ],
            "ref"        => [
                ED_FIELD_WIDTH => 6,
                ED_TYPE        => ED_TYPE_ALPHA,
                ED_ATTR        => "onchange=leafUpdated()",
                ED_LABEL       => "Réf fournisseur",
                ED_VALIDATE    => [
                    ED_VALIDATE_MAX_LENGTH => 30,
                ]
            ],
            "unit"       => [
                ED_FIELD_WIDTH => 3,
                ED_TYPE        => ED_TYPE_SELECT,
                ED_VALUE       => $row["unit"],
                ED_OPTIONS     => units()->getOptions(),
                ED_LABEL       => "Unité du matériau",
                ED_PLACEHOLDER => "Choisir",
                ED_ATTR        => "onchange=leafUpdated(this)",
                ED_VALIDATE    => [ED_VALIDATE_REQUIRED => true]
            ],
            "line1"      => [
                ED_INSERT_HTML => ''
                . '<div style="border:1px solid lightgrey;border-radius:4px" class="p-2">'
                . '  <h1 class=h6>Utilisation par le produit parent</h1>'
            ],
            "!usage"     => [
                ED_FIELD_WIDTH => 3,
                ED_TYPE        => ED_TYPE_FLOAT,
                ED_VALUE       => $usage[0],
                ED_LABEL       => "Quantité",
                ED_VALIDATE    => [ED_VALIDATE_REQUIRED => true]
            ],
            "!unitUsage" => [
                ED_DIV_ATTR    => "id=unitUsage",
                ED_FIELD_WIDTH => 3,
                ED_TYPE        => ED_TYPE_SELECT,
                ED_VALUE       => $usage[1] ?? $row["unit"],
                ED_OPTIONS     => units()->getOptions($row["unit"][0] ?? -1),
                ED_LABEL       => "Unité",
                ED_VALIDATE    => [ED_VALIDATE_REQUIRED => true]
            ],
            "line2"      => [ED_INSERT_HTML => '</div>']
        ];
    }

    if (utils()->action == LEAF_MENU_DROP || utils()->action == LEAF_MENU_ADD_EXISTING_MATERIAL) {
      $arForm["!action"] = [ED_TYPE => ED_TYPE_HIDDEN, ED_VALUE => LEAF_MENU_EDIT];
      unset($arForm["provider"]);
      unset($arForm["ref"]);
      unset($arForm["unit"]);
      if (utils()->action == LEAF_MENU_ADD_EXISTING_MATERIAL) {
        unset($arForm["name"]);
      }
      else {
        $arForm["name"][ED_ATTR] = "disabled";
        unset($arForm["name"][ED_LABEL]);
        unset($arForm["!material"]);
        unset($arForm["!parent"]);
        unset($arForm["!leafId"]);
      }
    }
    else {
      unset($arForm["!material"]);
      unset($arForm["!parent"]);
      unset($arForm["!leafId"]);
    }

    msgBox([
        MSGBOX_TITLE         => (utils()->action == LEAF_MENU_DROP ? ($_REQUEST["isCopy"] ? "Copie" : "Déplacement") : ($row ? "Modification" : "Création")) . " matériau",
        MSGBOX_CONTENT       => BuildForm::getForm($arForm, $row, "materialForm"),
        MSGBOX_BUTTON_ACTION => "Enregistrer",
        MSGBOX_BUTTON_CLOSE  => "Annuler",
    ]);
  }

}
