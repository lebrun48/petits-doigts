<?php

class FolderEdit
{

  function __construct(Leaf $leaf)
  {
    switch (utils()->action) {
      case LEAF_MENU_DROP:
        $_REQUEST["val_folderType"] = $leaf->folderType;
        $_REQUEST["changed"] = 0;
        $_REQUEST["type"] = $leaf->type;
        utils()->action = LEAF_MENU_EDIT;
        axProductsSubmit();
        return;

      case LEAF_MENU_EDIT:
        $row = &$leaf->content;
        $type = $leaf->type;
      case LEAF_MENU_NEW_FOLDER:
        !$type && $type = $leaf->ri ? LEAF_TYPE_FOLDER : LEAF_TYPE_ROOT_FOLDER;
      case LEAF_MENU_NEW_PRODUCT_CHOICE:
        !$type && $type = LEAF_TYPE_PRODUCT_CHOICE;
        $form = [
            ED_FORM_ATTR => "class='px-3'",
            "!changed"   => [ED_TYPE => ED_TYPE_HIDDEN, ED_VALUE => 0],
            "!type"      => [ED_TYPE => ED_TYPE_HIDDEN, ED_VALUE => $type],
            "name"       => [
                ED_TYPE     => ED_TYPE_ALPHA,
                ED_ATTR     => "onchange=leafUpdated()",
                ED_LABEL    => "Nom",
                ED_VALIDATE => [
                    ED_VALIDATE_INVALIDE   => "Le nom est requis",
                    ED_VALIDATE_REQUIRED   => true,
                    ED_VALIDATE_MAX_LENGTH => PRODUCTS_NAME_LEN,
                ]
            ],
//          "id"          => [
//              ED_TYPE     => ED_TYPE_ALPHA,
//              ED_ATTR     => "onchange=leafUpdated()",
//              ED_LABEL    => "Identifiant",
//              ED_VALIDATE => [
//                  ED_VALIDATE_MAX_LENGTH => PRODUCTS_ID_LEN,
//              ]
//          ],
        ];
    }
    msgBox([
        MSGBOX_TITLE         => ($row ? "Modification" : "Création")
        . ($type == LEAF_TYPE_PRODUCT_CHOICE ? " choix fabrication" : ($type == LEAF_TYPE_ROOT_FOLDER ? " liste de produits" : " options de produits")),
        MSGBOX_CONTENT       => BuildForm::getForm($form, $row),
        MSGBOX_BUTTON_ACTION => "Enregistrer",
        MSGBOX_BUTTON_CLOSE  => "Annuler",
    ]);
  }

}
