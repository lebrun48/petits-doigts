<?php

class ProductEdit
{

  function __construct(Leaf $leaf)
  {
    switch (utils()->action) {
      case LEAF_MENU_DROP:
      case LEAF_MENU_EDIT:
        $row = &$leaf->content;
        $row["isFinishedProduct"] = (int) ($row["leafType"] == LEAF_TYPE_FINISHED_PRODUCT);
        if ($row["duration"]) {
          $row["!hhh"] = intdiv($row["duration"], 3600);
          $row["!mm"] = intdiv($row["duration"] % 3600, 60);
          $row["!ss"] = $row["duration"] % 60;
        }
        $usage = json_decode($leaf->parent["childrenUsage"], true)[$leaf->ri];
      case LEAF_MENU_ADD_PRODUCT:
        $arForm = [
            ED_FORM_ATTR         => "class='px-3'",
            "!changed"           => [ED_TYPE => ED_TYPE_HIDDEN, ED_VALUE => 0],
            "!type"              => [ED_TYPE => ED_TYPE_HIDDEN, ED_VALUE => LEAF_TYPE_PRODUCT],
            "name"               => [
                ED_FIELD_WIDTH => 8,
                ED_TYPE        => ED_TYPE_ALPHA,
                ED_ATTR        => "onchange=leafUpdated()",
                ED_LABEL       => "Nom",
                ED_VALIDATE    => [
                    ED_VALIDATE_INVALIDE   => "Le nom est requis",
                    ED_VALIDATE_REQUIRED   => true,
                    ED_VALIDATE_MAX_LENGTH => PRODUCTS_NAME_LEN,
                ]
            ],
            "id"                 => [
                ED_FIELD_WIDTH => 4,
                ED_TYPE        => ED_TYPE_ALPHA,
                ED_ATTR        => "onchange=leafUpdated()",
                ED_LABEL       => "Identifiant",
                ED_VALIDATE    => [
                    ED_VALIDATE_MAX_LENGTH => PRODUCTS_ID_LEN,
                ]
            ],
            "!isFinishedProduct" => [
                ED_FIELD_WIDTH => 6,
                ED_TYPE        => ED_TYPE_CHECK,
                ED_VALUE       => 1,
                ED_CHECKED     => $row["isFinishedProduct"],
                ED_ATTR        => "onchange=leafUpdated(this)",
                ED_LABEL       => "Produit fini (peut être vendu)"
            ],
            "hasSerialNb"        => [
                ED_FIELD_WIDTH => 6,
                ED_TYPE        => ED_TYPE_CHECK,
                ED_VALUE       => 1,
                ED_CHECKED     => $row["hasSerialNb"],
                ED_ATTR        => "onchange=leafUpdated()" . ($row["isFinishedProduct"] ? "" : " disabled"),
                ED_LABEL       => "Contient le N° de série"
            ],
            "size"               => [
                ED_FIELD_WIDTH => 3,
                ED_DIV_ATTR    => "class=input-group",
                ED_TYPE        => ED_TYPE_INTEGER,
                ED_ATTR        => "onchange=leafUpdated()",
                ED_VALUE       => $row ? $row["unit"] : 1,
                ED_LABEL       => "Taille prod.",
                ED_VALIDATE    => [ED_VALIDATE_REQUIRED => true]
            ],
            "unit"               => [
                ED_FIELD_WIDTH => 3,
                ED_TYPE        => ED_TYPE_SELECT,
                ED_ATTR        => "class=input-group onchange=leafUpdated(this)",
                ED_VALUE       => $row["unit"],
                ED_OPTIONS     => units()->getOptions(),
                ED_LABEL       => "Unité production",
                ED_PLACEHOLDER => "Choisir",
                ED_NEXT_LINE   => true,
                ED_VALIDATE    => [ED_VALIDATE_REQUIRED => true]
            ],
            "lineTP"             => [ED_INSERT_HTML => '<div>Temps de production'],
            "!hhh"       => [
                ED_FIELD_WIDTH     => 3,
                ED_DIV_ATTR        => "class=input-group",
                ED_TYPE            => ED_TYPE_INTEGER,
                ED_NO_STEPPER      => true,
                ED_ATTR            => "onchange=leafUpdated()",
                ED_TEXT_AFTER      => "h.",
                ED_SELECT_ON_FOCUS => true,
                ED_VALIDATE        => [ED_VALIDATE_MAX_VALUE => 999, ED_VALIDATE_MIN_VALUE => 0, ED_VALIDATE_MAX_LENGTH => 3]
            ],
            "!mm"        => [
                ED_FIELD_WIDTH     => 3,
                ED_DIV_ATTR        => "class=input-group",
                ED_TYPE            => ED_TYPE_INTEGER,
                ED_ATTR            => "onchange=leafUpdated()",
                ED_NO_STEPPER      => true,
                ED_TEXT_AFTER      => "min.",
                ED_SELECT_ON_FOCUS => true,
                ED_VALIDATE        => [ED_VALIDATE_MAX_VALUE => 59, ED_VALIDATE_MIN_VALUE => 0, ED_VALIDATE_MAX_LENGTH => 2, ED_VALIDATE_INVALIDE => "max 59"]
            ],
            "!ss"        => [
                ED_FIELD_WIDTH     => 3,
                ED_DIV_ATTR        => "class=input-group",
                ED_TYPE            => ED_TYPE_INTEGER,
                ED_ATTR            => "onchange=leafUpdated()",
                ED_NO_STEPPER      => true,
                ED_TEXT_AFTER      => "sec.",
                ED_SELECT_ON_FOCUS => true,
                ED_VALIDATE        => [ED_VALIDATE_MAX_VALUE => 59, ED_VALIDATE_MIN_VALUE => 0, ED_VALIDATE_MAX_LENGTH => 2, ED_VALIDATE_INVALIDE => "max 59"]
            ],
            "lineTP1"    => [ED_INSERT_HTML => "</div>"],
//            "cost"               => [
//                ED_FIELD_WIDTH => 2,
//                ED_TYPE        => ED_TYPE_FLOAT,
//                ED_ATTR        => "onchange=leafUpdated()",
//                ED_LABEL       => "Coût fixe",
//                ED_ATTR        => "class=pb-0",
//                ED_TEXT_AFTER  => "€"
//            ],
            "line1"      => [
                ED_INSERT_HTML => ''
                . '<div style="border:1px solid lightgrey;border-radius:4px" class="p-2">'
                . '  <h1 class=h6>Utilisation par produit parent</h1>'
            ],
            "!usage"     => [
                ED_FIELD_WIDTH => 3,
                ED_TYPE        => ED_TYPE_FLOAT,
                ED_VALUE       => $usage[0],
                ED_LABEL       => "Quantité",
                ED_VALIDATE    => [ED_VALIDATE_REQUIRED => true]
            ],
            "!unitUsage" => [
                ED_DIV_ATTR    => "id=unitUsage",
                ED_FIELD_WIDTH => 3,
                ED_TYPE        => ED_TYPE_SELECT,
                ED_VALUE       => $usage[1],
                ED_OPTIONS     => units()->getOptions($row["unit"][0] ?? -1),
                ED_LABEL       => "Unité",
                ED_VALIDATE    => [ED_VALIDATE_REQUIRED => true]
            ],
            "line2"      => [ED_INSERT_HTML => '</div>']
        ];
    }
    if (utils()->action == LEAF_MENU_DROP) {
      $arForm["!action"] = [ED_TYPE => ED_TYPE_HIDDEN, ED_VALUE => LEAF_MENU_EDIT];
      unset($arForm["id"]);
      unset($arForm["duration"]);
      unset($arForm["!isFinishedProduct"]);
      unset($arForm["hasSerialNb"]);
      unset($arForm["cost"]);
      unset($arForm["unit"]);
      unset($arForm["size"]);
      unset($arForm["lineTP"]);
      unset($arForm["!hhh"]);
      unset($arForm["!mm"]);
      unset($arForm["!ss"]);
      unset($arForm["lineTP1"]);
      $arForm["name"][ED_ATTR] = "disabled";
      unset($arForm["name"][ED_LABEL]);
    }
    if (!$_REQUEST["parentOrig"] && !$_REQUEST["parentProduct"]) {
      unset($arForm["line1"]);
      unset($arForm["!usage"]);
      unset($arForm["!unitUsage"]);
      unset($arForm["line2"]);
    }

    msgBox([
        MSGBOX_TITLE         => (utils()->action == LEAF_MENU_DROP ? ($_REQUEST["isCopy"] ? "Copie" : "Déplacement") : ($row ? "Modification" : "Création")) . " produit",
        MSGBOX_CONTENT       => BuildForm::getForm($arForm, $row),
        MSGBOX_BUTTON_ACTION => "Enregistrer",
        MSGBOX_BUTTON_CLOSE  => "Annuler",
    ]);
  }

}
