<?php

class UpdateInsertLeaf
{

  function updateParentOnDrop(Leaf $leaf)
  {
    $children = $leaf->parent["children"];
    if (!$_REQUEST["isCopy"]) {
      //remove current
      if ($_REQUEST["parentOrig"] && $_REQUEST["parent"] != $_REQUEST["parentOrig"]) {
        $ri = explode('-', substr($_REQUEST["parentOrig"], 2, -1));
        $ri = substr($ri[count($ri) - 1], 1);
        $parent = dbUtil()->fetch_assoc(dbUtil()->selectRow("products", "children, childrenUsage", "ri=$ri"));
        $usage = json_decode($parent["childrenUsage"], true);
        unset($usage[$leaf->ri]);
        $parent["childrenUsage"] = json_encode($usage, JSON_NUMERIC_CHECK);
        $parent["children"] = str_replace("-$leaf->ri-", '-', $parent["children"]);
        dbUtil()->updateRow("products", $parent, "ri=$ri");
      }
      else {
        $children = str_replace("-$leaf->ri-", '-', $children);
      }
      utils()->axExecuteJS("manageTree", TREE_REMOVE_LEAF, $_REQUEST["leafId"]);
    }
    //add moved or copied leaf
    $op = TREE_APPEND;
    if ($_REQUEST["before"]) {
      $leafDest = $_REQUEST["before"] . '-';
      $op = TREE_ADD_BEFORE;
      $children = str_replace("-$leafDest", "-$leaf->ri-$leafDest", $children);
      $leafDest = $_REQUEST["parent"] . $leafDest;
    }
    elseif ($_REQUEST["after"]) {
      $leafDest = $_REQUEST["after"] . '-';
      $op = TREE_ADD_AFTER;
      $children = str_replace("-$leafDest", "-$leafDest$leaf->ri-", $children);
      $leafDest = $_REQUEST["parent"] . $leafDest;
    }
    $op == TREE_APPEND && $children .= "$leaf->ri-";
    dbUtil()->updateRow("products", ["children" => $children], "ri=$leaf->parentRealRI");

    $tree = new ProductsTree;
    $tree->buildBranch($tree->getLeaf($leaf->ri), substr($_REQUEST["parent"], 2, -1) . "-$leaf->ri");
    utils()->axExecuteJS("manageTree", $op, $op == TREE_APPEND ? $_REQUEST["parent"] : $leafDest, "data");
    utils()->axExecuteJS("treeListener");
  }

  function make(Leaf $leaf)
  {
    if ($_REQUEST["parent"]) {
      $this->updateParentOnDrop($leaf);
    }
    //update DB
    $_REQUEST["val_duration"] = intval($_REQUEST["hhh"]) * 3600 + intval($_REQUEST["mm"]) * 60 + intval($_REQUEST["ss"]);
    if (!$_REQUEST["val_duration"]) {
      unset($_REQUEST["val_duration"]);
    }
    if (utils()->action == LEAF_MENU_EDIT) {
      //update parent for usage
      if ($_REQUEST["usage"]) {
        $usage = json_decode($leaf->parent["childrenUsage"], true) ?? [];
        $usage[$leaf->ri] = [$_REQUEST["usage"], $_REQUEST["unitUsage"]];
        $tup["childrenUsage"] = json_encode($usage, JSON_NUMERIC_CHECK);
        dbUtil()->updateRow("products", "childrenUsage='" . json_encode($usage, JSON_NUMERIC_CHECK) . "'", "ri=$leaf->parentRealRI");
      }
      //update leaf content
      if ($_REQUEST["changed"]) {
        $_REQUEST["page"] = $leaf->table;
        if ($_REQUEST["type"] != LEAF_TYPE_MATERIAL) {
          $_REQUEST["val_leafType"] = $_REQUEST["isFinishedProduct"] ? LEAF_TYPE_FINISHED_PRODUCT : LEAF_TYPE_PRODUCT;
          !$_REQUEST["val_hasSerialNb"] && $_REQUEST["val_hasSerialNb"] = "=null";
        }
        dbUtil()->updateRow(null, null, "ri=$leaf->realRI");
        utils()->axExecuteJS("manageTree", TREE_LEAF_RENAME, $_REQUEST["leafId"], $_REQUEST["val_name"]);
        ($_REQUEST["val_leafType"] == LEAF_TYPE_PRODUCT || $_REQUEST["val_leafType"] == LEAF_TYPE_FINISHED_PRODUCT) && utils()->axExecuteJS(
                        "manageTree",
                        TREE_LEAF_SET_STYLE, $_REQUEST["leafId"],
                        ["color", $_REQUEST["val_leafType"] == LEAF_TYPE_FINISHED_PRODUCT ? LEAF_FINISHED_PRODUCT : "initial"]);
      }
      return true;
    }

    //insert
    if (($type = $_REQUEST["type"]) == LEAF_TYPE_MATERIAL) {
      $_REQUEST["page"] = "materials";
    }
    else {
      $_REQUEST["val_leafType"] = $_REQUEST["isFinishedProduct"] ? LEAF_TYPE_FINISHED_PRODUCT : $type;
      $_REQUEST["page"] = "products";
    }
    dbUtil()->insertRow();
    $newRI = $type . DBUtil()->getDbCnx()->insert_id;
    //insert is made at leafId
    if ($leaf->ri) {
      //add new leaf in parent
      if ($_REQUEST["usage"]) {
        $usage = json_decode($leaf->content["childrenUsage"], true) ?? [];
        $usage[$newRI] = [$_REQUEST["usage"], $_REQUEST["unitUsage"]];
        //update parent only (current leaf)
        $tup["childrenUsage"] = json_encode($usage, JSON_NUMERIC_CHECK);
      }
      $tup["children"] = $leaf->content["children"] . "$newRI-";
      dbUtil()->updateRow("products", $tup, "ri=$leaf->realRI");
    }

    //display
    $tree = new ProductsTree();
    $leafId = substr($leaf->leafId, 2, -1);
    $tree->buildBranch($tree->getLeaf($newRI), $leafId ? "$leafId-$newRI" : $newRI);
    if (!$leaf->ri) {
      $operation = TREE_ADD_BEFORE;
      $leafId = '0';
    }
    else {
      $operation = TREE_APPEND;
    }
    utils()->axExecuteJS("manageTree", $operation, "$leaf->type-$leafId-", "data");
    utils()->axExecuteJS("treeListener");
    return true;
  }

}
