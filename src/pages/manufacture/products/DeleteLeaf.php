<?php

class DeleteLeaf
{

  function __construct(Leaf $leaf)
  {

    foreach ($leaf->tree as $ri) {
      if (dbUtil()->result(dbUtil()->selectRow("products", "count(*)", "children like '%-$ri-%'"), 0) > 1) {
        $ri = 0;
        break;
      }
    }

    //delete everywhere after
    if ($ri && $leaf->type != LEAF_TYPE_MATERIAL) {
      $this->recursiveDelete([$leaf->ri], $toDelete);
      if ($toDelete = substr($toDelete, 1)) {
        foreach (explode(',', $toDelete) as $product) {
          if (dbUtil()->result(dbUtil()->selectRow("stock", "count(*)", "stockRef like '%-$product-%'"), 0)) {
            msgBox("<p class=font-weight-bold>Suppression impossible.</p><p>Au moins un des produits de la branche à un stock!</p>", "Suppression branche");
            exit();
          }
        }
        dbUtil()->deleteRow("products", "ri in ($toDelete)");
      }
    }
    if ($leaf->parent) {
      //update parent
      $tup["children"] = str_replace("-" . $leaf->ri . "-", '-', $leaf->parent["children"]);
      $usage = json_decode($leaf->parent["childrenUsage"], true) ?? [];
      unset($usage[$leaf->ri]);
      $tup["childrenUsage"] = json_encode($usage, JSON_NUMERIC_CHECK);
      dbUtil()->updateRow("products", $tup, "ri=" . $leaf->parentRealRI);
    }
    //rebuild display
    utils()->axExecuteJS("manageTree", TREE_REMOVE_LEAF, $_REQUEST["leafId"]);
  }

  function recursiveDelete($children, &$toDelete)
  {
    foreach ($children as $ri) {
      if ($ri && dbUtil()->result(dbUtil()->selectRow("products", "count(*)", "children like '%-$ri-%'"), 0) <= 1) {
        $subChildren = explode('-', substr(dbUtil()->result(dbUtil()->selectRow("products", "children", "ri=" . substr($ri, 1)), 0), 1, -1));
        $toDelete .= "," . substr($ri, 1);
        $this->recursiveDelete($subChildren, $toDelete);
      }
    }
  }

}
