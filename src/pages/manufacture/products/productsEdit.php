<?php

include "Units.php";

function axProductsEdit()
{
  $leaf = new Leaf;
  if (utils()->action == LEAF_MENU_DROP &&
          !$_REQUEST["isCopy"] && $_REQUEST["parent"] == $_REQUEST["parentOrig"] ||
          ($_REQUEST["before"] || $_REQUEST["before"]) && !$_REQUEST["parentProduct"]) {
    $_REQUEST["changed"] = 0;
    $_REQUEST["type"] = $leaf->type;
    utils()->action = LEAF_MENU_EDIT;
    axProductsSubmit();
    return;
  }

  switch (utils()->action) {
    case LEAF_MENU_EDIT:
    case LEAF_MENU_DROP:
      switch ($leaf->type) {
        case LEAF_TYPE_FOLDER:
        case LEAF_TYPE_ROOT_FOLDER:
        case LEAF_TYPE_PRODUCT_CHOICE:
          new FolderEdit($leaf);
          return;

        case LEAF_TYPE_PRODUCT:
        case LEAF_TYPE_FINISHED_PRODUCT:
          new ProductEdit($leaf);
          return;

        case LEAF_TYPE_MATERIAL:
          new MaterialEdit($leaf);
          return;

        default:
          throw new Exception("Invalid type");
      }

    case LEAF_MENU_NEW_FOLDER:
    case LEAF_MENU_NEW_PRODUCT_CHOICE:
      new FolderEdit($leaf);
      return;

    case LEAF_MENU_ADD_PRODUCT:
      new ProductEdit($leaf);
      return;

    case LEAF_MENU_ADD_MATERIAL:
    case LEAF_MENU_ADD_EXISTING_MATERIAL:
      new MaterialEdit($leaf);
      return;

    case LEAF_MENU_DUPLICATE:
      dbUtil()->begin_transaction();
      new DuplicateLeaf($leaf);
      dbUtil()->commit();
      return;

    case LEAF_MENU_REMOVE:
      dbUtil()->begin_transaction();
      new DeleteLeaf($leaf);
      dbUtil()->commit();
      return;

    case "buildUnitUsage":
      $_REQUEST["unit"] = dbUtil()->result(dbUtil()->selectRow("materials", "unit", "ri=" . substr($_REQUEST["material"], 1)), 0);
      getUsageUnit();
      return;

    case "getUsageUnit":
      getUsageUnit();
      return;

    default:
      msgbox("Not yet implemented");
      return;
  }
}

function getUsageUnit()
{
  $options = units()->getOptions($_REQUEST["unit"][0]);
  $edUnit = [
      ED_DIV_ATTR    => "id=unitUsage",
      ED_FIELD_WIDTH => 3,
      ED_NAME        => "!unitUsage",
      ED_TYPE        => ED_TYPE_SELECT,
      ED_VALUE       => $_REQUEST["unit"],
      ED_OPTIONS     => $options,
      ED_LABEL       => "Unité"
  ];
  mdbCompos()->buildSelect($edUnit);
  utils()->axExecuteJS("setSelectData", "unitUsage", "data");
}
