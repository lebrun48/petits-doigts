<?php

class ProductsTree extends Tree
{

  function getRootLeafsRi()
  {
    return explode(',', dbUtil()->result(dbUtil()->selectRow("products", "group_concat(concat(" . LEAF_TYPE_ROOT_FOLDER . ",ri) separator ',')", "leafType=" . LEAF_TYPE_ROOT_FOLDER), 0));
  }

  function buildBranch($tup, $leafId)
  {
    $tup["children"] && $tup["children"] = $this->translateChildren($tup["children"]);
    parent::buildBranch($tup, $leafId);
  }

  function translateChildren($children)
  {
    return strlen($children) > 1 ? explode('-', substr($children, 1, -1)) : [];
  }

  function getLeafIcon($leaf)
  {
    switch ($leaf["leafType"]) {
      case LEAF_TYPE_ROOT_FOLDER:
      case LEAF_TYPE_FOLDER: return '<i class="far fa-folder-open mx-1"></i>';
      case LEAF_TYPE_PRODUCT_CHOICE : return '<i class="fas fa-expand-arrows-alt mx-1"></i>';
      case LEAF_TYPE_FINISHED_PRODUCT:
      case LEAF_TYPE_PRODUCT: return "<i class='fas fa-industry mx-1'" . ($leaf["realLeafType"] == LEAF_TYPE_FINISHED_PRODUCT ? " style=color:" . LEAF_FINISHED_PRODUCT : '') . "></i>";
      case LEAF_TYPE_MATERIAL: return '<i class="fas fa-pallet mx-1"></i>';
    }
  }

  function getLeaf($ri)
  {
      if ($ri[0] == LEAF_TYPE_MATERIAL) {
        return dbUtil()->fetch_assoc(dbUtil()->selectRow("materials", "name, 1 as isTerminal, " . LEAF_TYPE_MATERIAL . " as leafType", "ri=" . substr($ri, 1)));
      }
      $tup = dbUtil()->fetch_assoc(dbUtil()->selectRow("products", "name, children, leafType", "ri=" . substr($ri, 1)));
      ($tup["realLeafType"] = $tup["leafType"]) && $tup["leafType"] == LEAF_TYPE_FINISHED_PRODUCT && $tup["leafType"] = LEAF_TYPE_PRODUCT;
      return $tup;
  }

  function isDragAble($leaf)
  {
    return $leaf["leafType"] != LEAF_TYPE_ROOT_FOLDER;
  }

  function isDropAble($leaf)
  {
    return $leaf["leafType"] != LEAF_TYPE_MATERIAL;
  }

}
