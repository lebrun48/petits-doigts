<?php

//DBUtil::$seeRequest = 1;

function buildMain()
{
  echo ""
  . "<main id='main'>";
  buildProductsBox();
  echo ""
  . "</main>";
}

function buildProductsBox()
{
  echo ""
  . "<div id=productsBox class='card mt-3'>"
  . "  <div class=card-body>"
  . "    <h5 class='text-center card-title m-0'>Définition produit</h5>"
  . "    <div class='treeview w-100 dropdown'>";
  $edit = ""
          . "<div class='dropdown-menu' id='context-menu'>"
          . "  <a class=dropdown-item data-action=" . LEAF_MENU_EDIT . "><i class='far fa-edit mr-1'></i>Editer</a>"
          . "  <a class=dropdown-item data-action=" . LEAF_MENU_DUPLICATE . "><i class='far fa-clone mr-1'></i>Dupliquer</a>"
          . "  <a class=dropdown-item data-action=" . LEAF_MENU_REMOVE . "><i class='far fa-trash-alt mr-1'></i>Supprimer</a>"
          . "  <a class=dropdown-item data-action=" . LEAF_MENU_NEW_FOLDER . "><i class='far fa-plus-square mr-1'></i>Ajouter options produit</a>"
//          . "  <a class=dropdown-item data-action=" . LEAF_MENU_NEW_PRODUCT_CHOICE . "><i class='far fa-plus-square mr-1'></i>Ajouter choix fabrication</a>"
          . "  <a class=dropdown-item data-action=" . LEAF_MENU_ADD_PRODUCT . "><i class='far fa-plus-square mr-1'></i>Ajouter produit</a>"
          . "  <a class=dropdown-item data-action=" . LEAF_MENU_ADD_MATERIAL . "><i class='far fa-plus-square mr-1'></i>Ajouter nouveau matériau</a>"
          . "  <a class=dropdown-item data-action=" . LEAF_MENU_ADD_EXISTING_MATERIAL . "><i class='far fa-plus-square mr-1'></i>Ajouter matériau existant</a>"
          . "</div>";

  $drop = ""
          . "<div class='dropdown-menu' id='drop-menu'>"
          . "  <a class='copy dropdown-item' data-action=" . DROP_COPY_BEFORE . ">Copier avant</a>"
          . "  <a class='copy dropdown-item' data-action=" . DROP_COPY_AFTER . ">Copier après</a>"
          . "  <a class='copy dropdown-item' data-action=" . DROP_COPY_INSIDE . ">Copier dedans</a>"
          . "  <a class='move dropdown-item' data-action=" . DROP_MOVE_BEFORE . ">Déplacer avant</a>"
          . "  <a class='move dropdown-item' data-action=" . DROP_MOVE_AFTER . ">Déplacer après</a>"
          . "  <a class='move dropdown-item' data-action=" . DROP_MOVE_INSIDE . ">Déplacer dedans</a>"
          . "</div>";

  dbUtil()->tables["products"][DB_COLS_DEFINITION] = [
      "id"   => [COL_TITLE => "Identifiant"],
      "name" => [COL_TITLE => "Nom"],
      "unit" => [COL_TITLE => "Unité production"],
      "cost" => [COL_TITLE => "Coût", COL_NO_DB],
      "ri"   => [
          COL_HIDDEN  => true,
          COL_PRIMARY => true],
  ];
  $tree = new ProductsTree("$edit $drop", "Nouvelle liste de produits");
  $tree->buildTree();
  echo ""
  . "    </div>"
  . "  </div>"
  . "</div>";
  utils()->insertReadyFunction("productsListener");
}

function axProductsSubmit()
{
  $leaf = new Leaf;

  try {
    dbUtil()->getDbCnx()->begin_transaction();
    if (!(new UpdateInsertLeaf())->make($leaf)) {
      dbUtil()->getDbCnx()->rollback();
      return;
    }
  } catch (Exception $e) {
    if ($e->getCode() == E_DB_DUPLICATE) {
      msgBox("<p>Ce nom existe déjà!</p>"
              , "Nom existant"
              , MODAL_SIZE_SMALL
              , [MSGBOX_MODAL_ATTR => [MODAL_NO_BACKDROP => true, MODAL_NO_FADE => true, MODAL_CENTERED => true]]
      );
      exit();
    }
    throw $e;
  }
  utils()->axExecuteJS("msgBoxClose");
  dbUtil()->getDbCnx()->commit();
}
