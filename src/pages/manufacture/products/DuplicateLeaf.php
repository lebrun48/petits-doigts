<?php

class DuplicateLeaf
{

  function __construct(Leaf &$leaf)
  {
    //change name if duplicate
    $baseName = preg_match("/^(.*)\(\d*\)$/", $leaf->content["name"], $out) ? $out[1] : $leaf->content["name"];
    $list = !$leaf->parent ?
            dbutil()->result(dbUtil()->selectRow("products", "group_concat(ri separator ',')", "leafType=" . LEAF_TYPE_ROOT_FOLDER), 0) :
            Leaf::getChildrenExceptTypes(LEAF_TYPE_MATERIAL, $leaf->parent["children"], ',');
    $list && $names = dbUtil()->fetch_all(dbUtil()->selectRow("products", "name", "ri in ($list)"));
    $list = Leaf::getChildrenByType(LEAF_TYPE_MATERIAL, $leaf->parent["children"], ',');
    $list && ($tmp = dbUtil()->fetch_all(dbUtil()->selectRow("products", "name", "ri in ($list)"))) && ($names && ($names = array_merge($names, $tmp)) || $names = $tmp);
    if ($names) {
      foreach ($names as $name) {
        preg_match("/^" . preg_quote($baseName, '/') . "\((\d*)\)$/", $name[0], $out) && ($nums[] = $out[1]);
      }
    }
    if (!$nums) {
      $num = '1';
    }
    else {
      for ($num = 1; $i <= sizeof($nums); $num++) {
        if (array_search($num, $nums) === false) {
          break;
        }
      }
    }
    $name = "$baseName($num)";

    //duplicate leaf in db
    $values = $leaf->content;
    $values["name"] = $name;
    unset($values["ri"]);
    dbUtil()->insertRow($leaf->table, $values);
    $values["ri"] = $leaf->type . dbUtil()->getDbCnx()->insert_id;
    if ($leaf->parent) {
      $usage = json_decode($leaf->parent["childrenUsage"], true) ?? [];
      $usage[$values["ri"]] = $usage[$leaf->ri];
      $leaf->parent["children"] = str_replace("-" . $leaf->ri . "-", "-" . $leaf->ri . "-" . $values["ri"] . "-", $leaf->parent["children"]);
      $leaf->parent["childrenUsage"] = json_encode($usage, JSON_NUMERIC_CHECK);
      //update only
      $tup["childrenUsage"] = $leaf->parent["childrenUsage"];
      $tup["children"] = $leaf->parent["children"];
      dbUtil()->updateRow("products", $tup, "ri=" . $leaf->parentRealRI);
    }

    //build branch
    $leafId = $leaf->tree;
    $leafId[sizeof($leafId) - 1] = $values["ri"];
    $tree = new ProductsTree;
    $tree->buildBranch($tree->getLeaf($values["ri"]), implode('-', $leafId));
    utils()->axExecuteJS("manageTree", TREE_ADD_AFTER, $_REQUEST["leafId"], "data");
    utils()->axExecuteJS("treeListener");
  }

}
