<?php

class LotsEdit extends BuildTable
{

  public $type;

  function getDisplayValue($key, $lot)
  {
    switch ($key) {
      case "ri":
        return $lot->refLot;
        
      case "date":
        return $lot->buildDate;

      case "content":
        foreach ($lot->current as $ar) {
          $content .= "<div>" . units()->getUnitQuantityText($ar[0], $ar[1], $lot->unit) . "</div>";
        }
        return $content;


      case "orig":
        return units()->getUnitQuantityText($lot->buildContent[0], $lot->buildContent[1], $lot->unit);
    }

    return parent::getDisplayValue($key, $lot);
  }
  
  function getUserAction($row)
  {
      return parent::getActionIcon($action) . '<span style="display:none" class=icon-action  data-action=distribute><i class="fas fa-eye-dropper fa-lg"></i></span>';
  }

}
