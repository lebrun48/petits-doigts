<?php

function axProvidersEdit()
{
  $row = dbUtil()->getCurrentEditedRow();
  $arForm = [
      "!xAction" => [ED_TYPE => ED_TYPE_HIDDEN, ED_VALUE => "submit"],
      "!action"  => [ED_TYPE => ED_TYPE_HIDDEN, ED_VALUE => utils()->action],
      "!page"    => [ED_TYPE => ED_TYPE_HIDDEN, ED_VALUE => "providers"],
      "!primary" => [ED_TYPE => ED_TYPE_HIDDEN, ED_VALUE => $_REQUEST["primary"]],
      "factory"  => [
          ED_LABEL    => "Entreprise",
          ED_TYPE     => ED_TYPE_ALPHA,
          ED_VALIDATE => [ED_VALIDATE_MAX_LENGTH => OTHER_PARTY_FACTORY_LEN, ED_VALIDATE_REQUIRED => true]
      ],
      "contact"  => [
          ED_LABEL    => "Nom contact",
          ED_TYPE     => ED_TYPE_ALPHA,
          ED_VALIDATE => [ED_VALIDATE_MAX_LENGTH => OTHER_PARTY_CONTACT_LEN]
      ],
      "email"    => [
          ED_LABEL    => "E-Mail",
          ED_TYPE     => ED_TYPE_ALPHA,
          ED_VALIDATE => [ED_VALIDATE_MAX_LENGTH => OTHER_PARTY_EMAIL_LEN]
      ],
      "phoneNbr" => [
          ED_LABEL    => "Tél.",
          ED_TYPE     => "tel",
          ED_ATTR     => "pattern=[\d/\.+\s]*",
          ED_VALIDATE => [ED_VALIDATE_MAX_LENGTH => OTHER_PARTY_PHONE_LEN, ED_VALIDATE_INVALIDE => "Chiffre, '+',  '.' ou '/' autorisé"]
      ],
      "address"  => [
          ED_LABEL    => "Adresse",
          ED_TYPE     => ED_TYPE_ALPHA,
          ED_VALIDATE => [ED_VALIDATE_MAX_LENGTH => OTHER_PARTY_ADDRESS_LEN]
      ],
  ];
  $options = [
      MSGBOX_MODAL_ID      => "providerModal",
      MSGBOX_BUTTON_ACTION => "Enregistrer",
      MSGBOX_BUTTON_CLOSE  => "Annuler",
  ];
  utils()->action == "insert" && $options[MSGBOX_MODAL_ATTR] = [MODAL_NO_BACKDROP => true, MODAL_CENTERED => true, MODAL_NO_FADE => true];

  msgBox(BuildForm::getForm($arForm, $row, "providerForm"), "Edition fournisseur", MODAL_SIZE_SMALL, $options);
}

function axProvidersSubmit()
{
  try {
    switch (utils()->action) {
      case "insert":
        dbUtil()->insertRow();
        $insertId = dbUtil()->getDbCnx()->insert_id;
        utils()->axExecuteJS("msgBoxClose", "providerModal");
        $options[] = [ED_VALUE => 0, ED_CONTENT => "(nouveau fournisseur)"];
        $options = array_merge($options, mdbCompos()->getOptions(dbUtil()->selectRow("providers", "ri, factory", "1 order by factory")));
        $edProvider = [
            ED_DIV_ATTR => "id=provider",
            ED_NAME     => "provider",
            ED_TYPE     => ED_TYPE_SELECT,
            ED_VALUE    => $insertId,
            ED_OPTIONS  => $options,
            ED_LABEL    => "Fournisseur"
        ];
        mdbCompos()->buildSelect($edProvider);
        utils()->axExecuteJS("setSelectData", "provider", "data");
        utils()->axExecuteJS("materialFormListener");
        break;

      case "update":
        dbUtil()->updateRow();
        include "materialsMain.php";
        utils()->axRefreshElement("materials");
        break;
    }
  } catch (Exception $exc) {
    if ($exc->getCode() == E_DB_DUPLICATE) {
      msgBox("<p>Ce nom d'entreprise existe Déjà !</p>", "Nom existe Déjà", MODAL_SIZE_SMALL, [MSGBOX_MODAL_ATTR => [MODAL_NO_BACKDROP => true, MODAL_CENTERED => true, MODAL_NO_FADE => true]]);
      exit();
    }
    throw $exc;
  }
}
