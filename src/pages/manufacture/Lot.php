<?php

class Lot
{

  public $reserved;  // [productionRef=>[quantity reserved, size reserved in unit], ...] // ordered by inclusion time. 
  public $current = []; // [current quantity, current size in unit], ...] sort ascending. Max 2 for units with non fixed size (piece, weight, liquid) 
  public $buildContent; // [current quantity, current size in unit]
  public $buildDate;
  public $unit; //unit of lot
  public $refLot; //Lot ref in stock
  static $usageRef; //ref to production
  static $saveReserved;
  static $reservedRef = [];

  public function __construct($stockTup)
  {
    $this->refLot = $stockTup["ri"];
    $res = dbUtil()->selectRow("stockUsage", "quantity, size, concat(usageType, usageRef) as usageRef", "lotRef=$this->refLot");
    while ($tup = dbUtil()->fetch_assoc($res)) {
      $this->reserved[$tup["usageRef"]][] = [intval($tup["quantity"]), floatval($tup["size"])];
    }
    $this->buildContent = [intval($stockTup["quantity"]), floatval($stockTup["size"])];
    $this->current = [$this->buildContent];
    $this->buildDate = $stockTup["date"];
    $this->unit = $stockTup["unit"];
  }

  public function reserve($quantity, $usage)
  {
    if ($this->current) {
      $size = units()->translateValue($usage[0], $usage[1], $this->unit);
      $reserved = $this->getFromCurrent([$quantity, $size]);
      //imcomplete number for size (only for not fixed size) in $reserved[2]
      $this->saveReserve([$quantity - $reserved[0], $size]);
    }
    return $reserved ?? [$quantity];
  }

  public function saveReserve($reserved)
  {
    $this->reserved[Lot::$usageRef][] = $reserved;
    dbUtil()->insertRow("stockUsage", [
        "lotRef"    => $this->refLot,
        "usageRef"  => substr(Lot::$usageRef, 1),
        "usageType" => Lot::$usageRef[0],
        "quantity"  => $reserved[0],
        "size"      => $reserved[1]]
    );
    if (Lot::$saveReserved) {
      Lot::$reservedRef[] = dbUtil()->getDbCnx()->insert_id;
    }
  }

  public function getFromCurrent($reserved)
  {
    //debugLog($this->current, "before");
    foreach ($this->current as $idx => $current) {
      //debugLog("$reserved[1], $current[1], " . (round($current[1], 6) >= round($reserved[1], 6)));
      if (!$reserved[0]) {
        break;
      }
      //debugLog("get $reserved[0] unity of $reserved[1] " . units()->getName($this->unit) . " on lot size $current[0] unity of $current[1] " . units()->getName($this->unit));
      if (("$this->unit")[0] < PRODUCT_UNIT_LENGTH) {
        $available = $current[0] * $current[1];
        $toReserve = $reserved[0] * $reserved[1];
        if (($rest = round($available - $toReserve, 6)) <= 0) {
          $remIdx[] = $idx;
          $reserved = [-$rest, 1];
        }
        else {
          $reserved[0] = 0;
          $this->current[$idx][0] = intdiv($rest, $current[1]);
          ($rest = fmod($rest, $current[1])) && $toAdd[] = [1, $rest];
        }
      }
      elseif (round($current[1], 6) >= round($reserved[1], 6)) {
        $nInUnit = (int) round($current[1] / $reserved[1], 6);
        $restInUnit = round($current[1] - $nInUnit * $reserved[1], 6);
        $available = $current[0] * $nInUnit;
        //debugLog("nInUnity = $nInUnity, resteInUnity = $resteInUnity, available = $available");
        if (($rest = $available - $reserved[0]) <= 0) {
          $reserved[0] = -$rest;
          ($this->current[$idx][1] = $restInUnit) || $remIdx[] = $idx;
        }
        else {
          ($this->current[$idx][0] = intdiv($rest, $nInUnit)) || $remIdx[] = $idx;
          ($rest = fmod($rest, $nInUnit)) && $toAdd[] = [1, $rest * $reserved[1] + $restInUnit];
          $restInUnit && ($used = intdiv($reserved[0], $nInUnit)) && $toAdd[] = [$used, $restInUnit];
          $reserved[0] = 0;
        }
      }
    }
    if ($remIdx) {
      foreach ($remIdx as $idx) {
        unset($this->current[$idx]);
      }
    }
    if ($toAdd) {
      foreach ($toAdd as $ar) {
        $this->current[] = $ar;
      }
    }
    $this->current && usort($this->current, function ($a, $b) {
              return $a[1] - $b[1];
            });
    //debugLog($this->current, "after");
    return $reserved;
  }

  function computeFree()
  {
    if ($this->reserved) {
      foreach ($this->reserved as $reservedProduct) {
        foreach ($reservedProduct as $reserved) {
          //remove current from reserve
          $this->current && $this->getFromCurrent($reserved);
        }
      }
    }
  }

}
