<?php

include "Units.php";

dbUtil()->tables["lots"] = [
    DB_COLS_DEFINITION => [
        "ri"      => [COL_HIDDEN => true, COL_PRIMARY => true],
        "date"    => [COL_TITLE => "Date", COL_TYPE => "date[d/m/Y]", COL_TD_ATTR => "class=td-1"],
        "orig"    => [COL_TITLE => "Contenu originel"],
        "content" => [COL_TITLE => "Contenu actuel"],
    ],
    DB_TABLENAME       => "stock",
    DB_RIGHTS_ACCES    => TABLE_RIGHT_DELETE | TABLE_RIGHT_EDIT
];

function axLotsShow()
{
  $stock = new Stock($_REQUEST["ref"]);
  $table = new LotsEdit("lots", $stock->lots);
  $table->type = $stock->stockRef[0];

  obStart();
  $table->buildFullTable();

  msgBox(ob_get_clean(), "Lots");
  utils()->axExecuteJS("tableListener", "lots");
}

function axLotsEdit()
{
  $row = dbUtil()->getCurrentEditedRow();
  if ($isMaterial = $row["stockRef"][1] == LEAF_TYPE_MATERIAL) {
    $material = json_decode($row["products"], true);
    unset($material["ri"]);
    $row = array_merge($row, $material, dbUtil()->fetch_assoc(dbUtil()->selectRow("providers", "factory", "ri=" . $material["provider"])));
    debugLog($row);
  }
  else {
    $row["name"] = Stock::getStockName($row["stockRef"], $row["products"]);
  }
  $lot = new Lot($row);
  $lot->computeFree();
  switch (utils()->action) {
    case "update":
      $form = [
          [ED_INSERT_HTML => "<h4 class=h4>Lot " . ($isMaterial ? "acheté" : "produit") . "</h4><div class='px-2 border border-light rounded-sm'>"],
          "name"     => [ED_TYPE => ED_TYPE_ALPHA, ED_ATTR => "disabled", ED_LABEL => "Nom " . ($isMaterial ? "matériau" : "produit")],
          "factory"  => [ED_TYPE => ED_TYPE_ALPHA, ED_ATTR => "disabled", ED_LABEL => "Nom fournisseur"],
          "ref"      => [ED_TYPE => ED_TYPE_ALPHA, ED_ATTR => "disabled", ED_LABEL => "Réf. fournisseur"],
          "quantity" => [
              ED_TYPE       => ED_TYPE_INTEGER,
              ED_NO_STEPPER => true,
              ED_VALIDATE   => [ED_VALIDATE_REQUIRED => true, ED_VALIDATE_MIN_VALUE => 0],
              ED_LABEL      => "Quantité"
          ],
          "size"     => [
              ED_TYPE       => ED_TYPE_FLOAT,
              ED_NO_STEPPER => true,
              ED_VALIDATE   => [ED_VALIDATE_REQUIRED => true, ED_VALIDATE_MIN_VALUE => 0],
              ED_LABEL      => "Taille"
          ],
          "unit"     => [
              ED_TYPE     => ED_TYPE_SELECT,
              ED_OPTIONS  => units()->getOptions($row["unit"][0]),
              ED_LABEL    => "Unité",
              ED_VALIDATE => [ED_VALIDATE_REQUIRED => true]
          ],
          "price"    => [ED_TYPE => ED_TYPE_FLOAT, ED_LABEL => "Prix du lot", ED_NO_STEPPER => true, ED_VALIDATE => [ED_VALIDATE_MIN_VALUE => 0]],
          [ED_INSERT_HTML => "</div><h4 class='mt-3 h4'>Disponibilité du lot</h4><div class='px-2 border border-light rounded-sm'>"],
      ];
      if (!$isMaterial) {
        unset($form["factory"]);
        unset($form["ref"]);
        $form["quantity"][ED_ATTR] = $form["size"][ED_ATTR] = $form["unit"][ED_ATTR] = "disabled";
      }
      foreach ($lot->current as $idx => $current) {
        $form["!quantity_$idx"] = [
            ED_TYPE       => ED_TYPE_INTEGER,
            ED_VALUE      => $current[0],
            ED_NO_STEPPER => true,
            ED_VALIDATE   => [ED_VALIDATE_REQUIRED => true, ED_VALIDATE_MIN_VALUE => 0],
            ED_LABEL      => "Quantité"
        ];
        $form["!size_$idx"] = [
            ED_TYPE       => ED_TYPE_FLOAT,
            ED_VALUE      => $current[1],
            ED_NO_STEPPER => true,
            ED_VALIDATE   => [ED_VALIDATE_REQUIRED => true, ED_VALIDATE_MIN_VALUE => 0],
            ED_LABEL      => "Taille"
        ];
        $form["!unit_$idx"] = [
            ED_TYPE     => ED_TYPE_SELECT,
            ED_OPTIONS  => units()->getOptions($row["unit"][0]),
            ED_LABEL    => "Unité",
            ED_VALIDATE => [ED_VALIDATE_REQUIRED => true]
        ];
      }
      $form[] = [ED_INSERT_HTML => "</div>"];

      msgBox(BuildForm::getForm($form, $row, "lotForm"), "Modification lot", null, [
          MSGBOX_BUTTON_ACTION => "enregistrer",
          MSGBOX_BUTTON_CLOSE  => "annuler",
          MSGBOX_MODAL_ATTR    => [MODAL_NO_BACKDROP => true, MODAL_NO_FADE => true, MODAL_CENTERED => true]
      ]);
  }
}
