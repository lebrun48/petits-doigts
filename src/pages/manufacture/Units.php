<?php

define("PRODUCT_UNIT_PIECE", 0);
define("PRODUCT_UNIT_WEIGHT", 1);
define("PRODUCT_UNIT_LIQUID", 2);
define("PRODUCT_UNIT_LENGTH", 3);
define("PRODUCT_UNIT_AREA", 4);
define("PRODUCT_UNIT_VOLUME", 5);

class Units
{

  public $unitProduction = [
      "Pièce",
      "Poids"   => [["Tonne", 1, 1], ["Kg", 1000], ["gr", 1000000]],
      "Liquide" => [["Litre", 1, 1], ["dl", 10], ["cl", 100]],
      "Longeur" => [["Km", 1], ["mètre", 1000, 1], ["cm", 100000], ["mm", 1000000]],
      "Surface" => [["m²", 1], ["cm²", 10000], ["mm²", 1000000]],
      "Volume"  => [["m³", 1], ["cm³", 1000000]]
  ];

  function translateValue($value, string $from, string $to)
  {
    if ($from == $to) {
      return $value;
    }
    $units = array_keys($this->unitProduction);
    if (($unit = $from[0]) != $to[0]) {
      throw new Exception("Conversion impossible entre «" . $this->getName($from) . "» et «" . $this->getName($to) . "»", E_USER_ERROR);
    }
    if ($unit == 0) {
      return $value;
    }
    $unitsValues = array_values($this->unitProduction);
    return $value * $unitsValues[$to[0]][$to[1]][1] / $unitsValues[$from[0]][$from[1]][1];
  }

  function getName($unit, $size = null, $full = false)
  {
    $unit = "$unit";
    $unitKeys = array_keys($this->unitProduction);
    $unitValues = array_values($this->unitProduction);
    if (!$full) {
      return strlen($unit) > 1 ? ($def = $unitValues[$unit[0]][$unit[1]])[0] . ($size >= 2 && $def[2] ? 's' : '') : $unitValues[$unit[0]] . ($size >= 2 ? 's' : '');
    }
    return strlen($unit) > 1 ? $unitKeys[$unit[0]] . ' (' . $unitKeys[$unit[0]] . ')' : $unitValues[$unit[0]];
  }

  function getUnitQuantityText($quantity, $size, $unit)
  {
    if ($unit == 0 && $size == 1) {
      return "$quantity " . units()->getName($unit, $quantity);
    }
    !$quantity && ($s = "·s") || $quantity > 1 && $s = 's';
    return "$quantity unité$s de $size " . units()->getName($unit, $size);
  }

  function getOptions($only = null)
  {
    $idxUnit = array_values($this->unitProduction);
    $keyUnit = array_keys($this->unitProduction);

    foreach ($idxUnit as $idx => $units) {
      $hide = isset($only) && $only != $idx;
      if (is_array($units)) {
        !isset($only) && $options[] = [ED_LABEL => $keyUnit[$idx]];
        foreach ($units as $key => $unit) {
          !$hide && $options[] = [ED_VALUE => "$idx$key", ED_CONTENT => $unit[0]];
        }
      }
      else {
        !$hide && $options[] = [ED_VALUE => "$idx", ED_CONTENT => $idxUnit[$idx]];
      }
    }
    return $options;
  }

}

function units(): Units
{
  static $units;
  !$units && $units = new Units();
  return $units;
}
