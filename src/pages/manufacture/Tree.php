<?php

abstract class Tree
{

  public $contextMenu;
  public $buildLeafName;
  private $isFirst = false;
  private $last;
  private $begin;

  abstract function getLeafIcon($leaf);

  abstract function getRootLeafsRi();

  abstract function getLeaf($ri);

  function isDragAble($leaf)
  {
    return false;
  }

  function isDropAble($leaf)
  {
    return false;
  }

  function getClasses($leaf)
  {
    
  }

  public function __construct($contextMenu = null, $buildLeafName = null)
  {
    $this->contextMenu = $contextMenu;
    $this->buildLeafName = $buildLeafName;
  }

  public function buildTree()
  {
    echo ""
    . "<ul class='mb-1 pl-3 pb-2'>";
    $leafs = $this->getRootLeafsRi();
    $this->isFirst = true;
    foreach ($leafs as $ri) {
      $ri && $this->buildBranch($this->getLeaf($ri), $ri);
    }
    echo $this->contextMenu;
    $this->buildLeafName && $this->buildAddLeaf();
    echo "</ul>";
  }

  public function buildAddLeaf()
  {
    $id = LEAF_TYPE_ROOT_FOLDER . "-0-";
    echo ""
    . "<li id=$id>"
    . "<a class=tree-leaf><i class='far fa-plus-square mr-1'></i>$this->buildLeafName</a></li>";
  }

  function buildBranch($tup, $leafId)
  {
    if ($tup["isTerminal"]) {
      $this->buildTerminalLeaf($tup, $leafId);
      return;
    }
    $this->buildLeaf($tup, $leafId);
  }

  function buildLeaf($leaf, $leafId)
  {
    echo ""
    . "<li class='" . $this->getClasses($leaf) . ' ' . ($this->isDragAble($leaf) ? "drag' draggable=true" : "'") . " id=" . $leaf["leafType"] . "-$leafId->"
    . "  <i class='fas fa-angle-right rotate px-1'></i>"
    . "  <a class='" . ($this->isDropAble($leaf) ? "drop " : '') . "tree-leaf'>" . $this->getLeafIcon($leaf) . "<span>" . $leaf["name"] . "</span></a>"
    . "  <ul class='nested'>";
    if ($leaf["children"]) {
      foreach ($leaf["children"] as $ri) {
        $this->buildBranch($this->getLeaf($ri), "$leafId-$ri");
      }
    }
    echo ""
    . "  </ul>"
    . "</li>";
  }

  function buildTerminalLeaf($leaf, $leafId)
  {
    echo ""
    . "<li class='" . $this->getClasses($leaf) . ' ' . ($this->isDragAble($leaf) ? "drag' draggable=true" : "'") . " id=" . $leaf["leafType"] . "-$leafId->"
    . "<a class='" . ($this->isDropAble($leaf) ? "drop " : '') . "tree-leaf'>" . $this->getLeafIcon($leaf) . "<span>" . $leaf["name"] . "</span></a></li>";
    $this->isFirst = false;
  }

}
