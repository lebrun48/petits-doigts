<?php

class Stock
{

  public $lots = []; // array of Lot
  public $fromStock; //
  public $toProduce;
  public $isChoiceStock;
  public $toProduceStock = [];
  public $stockUnit;
  public $isFirstStock = false;
  public $stockRef;
  public $minSize;

  function getLot($idx): Lot
  {
    return $this->lots[$idx];
  }

  static function buildTableDef()
  {
    dbUtil()->tables["stock"][DB_COLS_DEFINITION] = [
        "description" => [COL_TITLE => "Description"],
        "ri"          => [COL_PRIMARY => true, COL_TITLE => "Lot"],
    ];
  }

  function __construct($leaf = null, $stockRef = null)
  {
    if (!$leaf) {
      return;
    }
    self::buildTableDef();
    if (is_array($leaf)) {
      if ($leaf["leafType"] == LEAF_TYPE_PRODUCT_CHOICE) {
        return;
      }
      $ri = $leaf["ri"];
      $leafId = $leaf["leafType"] . $ri;
      $stockRef || $stockRef = $leafId;
      //compute min size
      $this->minSize = PHP_FLOAT_MAX;
      $res = dbUtil()->selectRow("products", "childrenUsage", "children like '%-$leafId-%'");
      while ($tup = dbUtil()->fetch_row($res)) {
        $usage = json_decode($tup[0], true)[$leafId];
        isset($usage[0]) && isset($usage[1]) && $this->minSize = min($this->minSize, units()->translateValue($usage[0], $usage[1], $leaf["unit"]));
      }
      $this->minSize == PHP_FLOAT_MAX && $this->minSize = 0;
    }
    else {
      $stockRef = $leaf;
    }
    $this->stockRef = $stockRef;

    //get all lots
    $res = dbUtil()->selectRow("stock", "ri, quantity, size, unit, date", "stockRef='-$stockRef-' and isProduction=0 order by date asc");
    while ($tup = dbUtil()->fetch_assoc($res)) {
      $lot = new Lot($tup);
      $lot->computeFree();
      $this->lots[] = $lot;
    }
  }

  function getSumary()
  {
    $sumary = [];
    foreach ($this->lots as $lot) {
      $lot->current && !isset($this->stockUnit) && $this->stockUnit = [1, $lot->unit];
      foreach ($lot->current as $current) {
        if (($size = units()->translateValue($current[1], $lot->unit, $this->stockUnit[1])) && $size >= $this->minSize) {
          $stockUnit = $this->stockUnit[1];
          if (("$stockUnit")[0] == PRODUCT_UNIT_PIECE) {
            $sumary["1"] += $size * $current[0];
          }
          else {
            $sumary["$size"] += $current[0];
          }
        }
      }
    }
    return $sumary;
  }

  function getSumaryText($buttonLots = true, $onlySize = null)
  {
    if ($sumary = $this->getSumary()) {
      $line = ""
              . "<div class='d-flex align-items-center'>"
              . "  " . ($buttonLots ? getButton([BUTTON_ATTR => "data-ref=$this->stockRef class='lots px-1 py-0 mr-2'", BUTTON_SIZE_SM => true, BUTTON_OUTLINE => true, BUTTON_ROUNDED => true, BUTTON_TEXT => "Lots"]) : '')
              . "  <div>";
      foreach ($sumary as $size => $quantity) {
        if (!$onlySize || $onlySize == $size) {
          if (!$onlySize && $quantity == 1) {
            $line .= "<div class='d-flex'>$size " . units()->getName($this->stockUnit[1], $size == 1 ? $quantity : $size) . "</div>";
          }
          else {
            $line .= "<div class='d-flex'>$quantity " . ($size != 1 ? "unité" . ($quantity > 1 ? 's' : '') . " de $size " : '') . units()->getName($this->stockUnit[1], $size == 1 ? $quantity : $size) . "</div>";
          }
        }
      }

      $line .= ""
              . "  </div>"
              . "</div>";
      return $line;
    }
    return "Pas de stock";
  }

  function reserve($usage, $quantity, $unitProd, &$serialNb = null)
  {
    $reserve = [$quantity];

    !$this->stockUnit && $this->stockUnit = $unitProd;
    if ($this->lots) {
      $prev = $quantity;
      foreach ($this->lots as $lot) {
        $reserve = $lot->reserve($reserve[0], $usage);
        if (isset($serialNb) && ($reserved = ceil(($prev - $reserve[0]) * $usage[0]))) {
          $serialNb[$lot->refLot] = $reserved;
        }
        $prev = $reserve[0];
        if (!$reserve[0]) {
          break;
        }
      }
    }
    $this->fromStock += units()->translateValue(($quantity - $reserve[0]) * $usage[0], $usage[1], $this->stockUnit[1]);
    $toProduce = $reserve[0] * ceil(units()->translateValue($usage[0], $usage[1], $this->stockUnit[1]) / $this->stockUnit[0]);
    if ($reserve[0]) {
      $this->toProduce += units()->translateValue($reserve[0] * $usage[0], $usage[1], $this->stockUnit[1]) / $this->stockUnit[0];
    }
    //debugLog("stockRef = $this->stockRef, quantity=$quantity, fromStock=$this->fromStock " . units()->getName($this->stockUnit[1], $this->fromStock) . ", toProduce=" . units()->getUnitQuantityText($this->toProduce, $this->stockUnit[0], $this->stockUnit[1]) . ", reserve=$reserve[0], usage=$usage[0] " . units()->getName($usage[1]));
    return $toProduce;
  }

  static function getStockName($stockRef, $products = null)
  {
    foreach (explode('-', $stockRef[0] == '-' ? substr($stockRef, 1, -1) : $stockRef) as $ri) {
      $name .= '-' . ($products[$ri]["name"] ?? dbUtil()->result(dbUtil()->selectRow(("$ri")[0] == LEAF_TYPE_MATERIAL ? "materials" : "products", "name", "ri=" . substr($ri, 1)), 0));
    }
    return substr($name, 1);
  }

  function getReserved($usageRef, $size)
  {
    debugLog("$usageRef, $size");
    foreach ($this->lots as $lot) {
      foreach ($lot->reserved[$usageRef] as $reserved) {
        $reserved[1] == $size && $quantity += $reserved[0];
      }
    }
    return $quantity;
  }

  static function getSerialNb($rangeSN)
  {
    if (!$rangeSN) {
      return;
    }
    $prev = -1;
    $rangeSN = explode(',', $rangeSN);
    foreach ($rangeSN as $sn) {
      if ($sn == $prev + 1) {
        $range[strlen($range) - 1] != '-' && $range .= '-';
      }
      elseif ($range[strlen($range) - 1] == '-') {
        $range .= "$prev, $sn";
      }
      else {
        $range && $range .= ", ";
        $range .= $sn;
      }
      $prev = $sn;
    }
    $range[strlen($range) - 1] == '-' && ($range .= $prev);
    return $range;
  }

  function getLotsSerialNb($onlyRange = false)
  {
    //get serialNb from all lot for this prod.
    foreach ($this->lots as $idx => $lot) {
      $riList .= ',' . $this->getLot($idx)->refLot;
    }
    if (!$riList) {
      return;
    }
    $rangeSN = dbUtil()->result(dbUtil()->selectRow("serialNb", "group_concat(ri)", "usedBy" . (Lot::$usageRef ? "='" . Lot::$usageRef . "'" : " is null")
                    . " and stockRef in (" . substr($riList, 1) . ") and ordersRef is null"), 0);

    return $onlyRange ? $rangeSN : Stock::getSerialNb($rangeSN);
  }

}
