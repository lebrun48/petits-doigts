<?php

function axCurrentSaleEdit()
{
  $row = dbUtil()->getCurrentEditedRow();
  switch (utils()->action) {
    case "updCustomer":
      include "customersMain.php";
      $_REQUEST["page"] = "customers";
      $_REQUEST["primary"] = "ri=" . $row["customer"];
      utils()->action = "update";
      axCustomersEdit();
      break;

    case "delete":
      if ($row["state"] != SALE_STATE_REGISTERED) {
        msgBox("<p>La suppression de la vente n'est pas possible dans cet état.</p>", "Suppression vente");
        return;
      }
      msgbox("<p class=font-weight-bold>Cette opération est irréversible!</p>"
              . "Les produits de cette vente seront remis disponibles dans le stock!</p>",
             "Confirmation suppression vente",
             null,
             [MSGBOX_BUTTON_ACTION => "Supprimer", MSGBOX_BUTTON_CLOSE => "annuler"]);
      break;

    case "archive":
      msgBox("A faire");
      break;
  }
}

function axCurrentSaleSubmit()
{
  //only delete
  include "ordersMain.php";
  dbUtil()->begin_transaction();
  $res = dbUtil()->selectRow("orders", "ri", "salesRef=" . $_REQUEST["primary"][0][1]);
  $_REQUEST["page"] = "orders";
  while ($ri = dbUtil()->fetch_row($res)) {
    $_REQUEST["primary"] = "ri=$ri[0]";
    axOrdersEdit($ri[0]);
  }
  dbUtil()->commit();
  unset($_REQUEST["page"]);
  utils()->axRefreshElement("toSale");
  utils()->axRefreshElement("currentSales");
}

function axCurrentSaleState()
{
  $tmp = explode('_', $_REQUEST["id"]);
  dbUtil()->updateRow("sales", "state=" . ($_REQUEST["checked"] ? "state | $tmp[2]" : "state & ~$tmp[2]"), "ri=$tmp[1]");
}

function axCurrentSaleOrder()
{
  $order = new Order("orders", true);
  dbUtil()->getTable("orders")[DB_DEFAULT_WHERE] = "salesRef=" . $_REQUEST["ref"];
  obStart();
  $order->buildFullTable();
  msgBox(ob_get_clean(), "Commande N°" . $_REQUEST["ref"], MODAL_SIZE_LG);
}
