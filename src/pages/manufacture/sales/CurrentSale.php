<?php

class CurrentSale extends BuildTable
{

  private $amounts;

  function getDisplayValue($key, $row)
  {
    switch ($key) {
      case "id":
        return getButton([BUTTON_ATTR => "class='order py-0' data-ref=" . $row["ri"], BUTTON_SIZE_SM => true, BUTTON_OUTLINE => true, BUTTON_ROUNDED => true, BUTTON_TEXT => $row["ri"]]);

      case "customer" :
        if ($row["factory"]) {
          return $row["factory"] . ($row[$key] ? " (" . $row[$key] . ")" : '');
        }
        break;

      case SALE_STATE_CHARGED:
      case SALE_STATE_PAID:
      case SALE_STATE_PRODUCT_SENT:
        return "<input type=checkbox class=form-check-input id=sale_" . ($id = $row["ri"] . "_$key ") . ($row["state"] & $key ? "checked" : '') . "><label for=sale_$id></label>";

      case "amount":
        $this->amounts = dbUtil()->fetch_row(dbUtil()->selectRow("orders", "sum(price*quantity), sum(price*quantity*tva/100)", "salesRef=" . $row["ri"]));
        return $this->amounts[0];

      case "ttc":
        return array_sum($this->amounts);
    }
    
    return parent::getDisplayValue($key, $row);
  }

  function getUserAction($row)
  {
    return '<span style="display:none" class=icon-action data-action=archive><i title=Archiver class="fas fa-archive fa-lg"></i></span>';
  }

}
