<?php

include "salesMain.php";

function axOrdersEdit($ri = null)
{
  //only delete
  !$ri && ($ri = $_REQUEST["primary"][0][1]) || $noRefresh = true;
  !$noRefresh && dbUtil()->begin_transaction();
  $sale = dbUtil()->result(dbUtil()->selectRow("orders", "salesRef", "ri=$ri"), 0);
  dbUtil()->deleteRow("stockUsage", "usageRef=$ri && usageType='S'", true);
  dbUtil()->deleteRow();
  if (!dbUtil()->num_rows(dbUtil()->selectRow("orders", "ri", "salesRef=$sale limit 1"))) {
    dbUtil()->deleteRow("sales", "ri=$sale");
  }
  if (!$noRefresh) {
    dbUtil()->commit();
    unset($_REQUEST["page"]);
    utils()->axRefreshElement("order");
    utils()->axRefreshElement("toSale");
  }
}

function axOrdersSubmit()
{
  dbUtil()->begin_transaction();
  utils()->action = "insert";
  dbUtil()->updateRow("sales", "customer=" . $_REQUEST["customer"] . ", state=" . SALE_STATE_REGISTERED, "state=" . SALE_STATE_ORDER);
  utils()->axRefreshElement("order");
  utils()->axRefreshElement("currentSales");
  dbUtil()->commit();
}
