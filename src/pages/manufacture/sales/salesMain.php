<?php

include "Units.php";

function buildMain()
{
  echo ""
  . "<main id='main'>"
  . "  <ul class='nav nav-tabs nav-justified md-tabs white mt-2' role='tablist'>"
  . "    <li class='nav-item'>"
  . "      <a class='nav-link text-dark active font-weight-bold' id='home-tab-just' data-toggle='tab' href='#sales-1' role='tab'"
  . "        aria-selected='true'>Produits à vendre</a>"
  . "    </li>"
  . "    <li class='nav-item'>"
  . "      <a class='nav-link text-dark font-weight-bold' id='profile-tab-just' data-toggle='tab' href='#sales-2' role='tab'"
  . "        aria-selected='false'>Commande en cours</a>"
  . "    </li>"
  . "    <li class='nav-item'>"
  . "      <a class='nav-link text-dark font-weight-bold' id='profile-tab-just' data-toggle='tab' href='#sales-3' role='tab'"
  . "        aria-selected='false'>Ventes en cours</a>"
  . "    </li>"
  . "    <li class='nav-item'>"
  . "      <a class='nav-link text-dark font-weight-bold' id='contact-tab-just' data-toggle='tab' href='#sales-4' role='tab'"
  . "        aria-selected='false'>Ventes réalisées</a>"
  . "    </li>"
  . "  </ul>"
  . "  <div class='tab-content card pt-5'>"
  . "    <div class='tab-pane fade show active' id='sales-1' role='tabpanel'>";
  buildToSaleBox();
  echo ""
  . "    </div>"
  . "    <div class='tab-pane fade' id='sales-2' role='tabpanel'>";
  buildOrderBox();
  echo ""
  . "    </div>"
  . "    <div class='tab-pane fade' id='sales-3' role='tabpanel'>";
  buildCurrentSalesBox();
  echo ""
  . "    </div>"
  . "    <div class='tab-pane fade' id='sales-4' role='tabpanel'>";
  buildSalesMadeBox();
  echo ""
  . "    </div>"
  . "  </div>"
  . "</main>";
}

function buildToSaleBox()
{
  echo "<div id=toSaleBox>";
  $table = new ToSale;
  $table->theadAttr = "class='grey lighten-4'";
  $table->buildFullTable();
  utils()->insertReadyFunction("toSaleListener");
  echo "</div>";
}

function buildOrderBox()
{
  echo "<div id=orderBox>";
  $table = new Order("orders");
  if ($row = dbUtil()->fetch_row(dbUtil()->selectRow("sales", "ri, customer", "state=" . SALE_STATE_ORDER))) {
    echo BuildForm::getForm([
        "!customer" => [
            ED_VALUE       => $row[1],
            ED_DIV_ATTR    => "style=width:200px id=customer",
            ED_TYPE        => ED_TYPE_SELECT,
            ED_ATTR        => "searchable=Chercher...",
            ED_PLACEHOLDER => 'Choisir client',
            ED_LABEL       => "Client",
            ED_OPTIONS     => $table->getOptions(),
            ED_VALIDATE    => [ED_VALIDATE_REQUIRED => true, ED_VALIDATE_INVALIDE => "Client requis"]
        ],
        "!page"     => [ED_VALUE => "orders", ED_TYPE => ED_TYPE_HIDDEN],
        "!xAction"  => [ED_VALUE => "submit", ED_TYPE => ED_TYPE_HIDDEN],
            ], null, "orderForm");
    $table->theadAttr = "class='grey lighten-4'";
    $table->defaultWhere = "salesRef=$row[0]";
    $table->buildFullTable();
    utils()->insertReadyFunction("orderFormListener");
    utils()->insertReadyFunction("tableListener", "orders");
  }
  else {
    echo "<h4 class=h4>Pas de commande en cours</h4>";
  }
  echo "</div>";
}

function buildCurrentSalesBox()
{
  echo "<div id=currentSalesBox>";
  $table = new CurrentSale("currentSale");
  $table->defaultWhere = "state<>" . SALE_STATE_ORDER;
  $table->buildFullTable();
  echo "</div>";
  utils()->insertReadyFunction("currentSaleListener", "currentSale");
}

function buildSalesMadeBox()
{
  
}
