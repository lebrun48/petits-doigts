<?php

class Order extends BuildTable
{

  private $sumTVA;
  private $sumAmount;
  private $onlyTable;

  function __construct($tableId, $onlyTable = false)
  {
    $this->onlyTable = $onlyTable;
    parent::__construct($tableId);
  }

  function getOptions()
  {
    $options[] = [ED_VALUE => 0, ED_CONTENT => "(Nouveau client)"];
    return array_merge($options, mdbCompos()->getOptions(dbUtil()->selectRow("customers", "ri, contact", "1")));
  }

  function getDisplayValue($key, $row)
  {
    switch ($key) {
      case "stockRef":
        return Stock::getStockName($row["stockRef"]);

      case "serialNb":
        $rangeSN = dbUtil()->result(dbUtil()->selectRow("serialNb", "group_concat(ri)", "ordersRef=" . $row["ri"] . " order by ri asc"), 0);
        return Stock::getSerialNb($rangeSN);

      case "totalPrice":
        $this->sumAmount += ($amount = $row["price"] * $row["quantity"]);
        $this->sumTVA += $amount * $row["tva"] / 100;
        return $row["price"] * $row["quantity"];
    }
    return parent::getDisplayValue($key, $row);
  }

  function buildAfterTable()
  {
    echo "<tr style='border-top:2px solid black'><td colspan=4 class='text-right font-weight-bold'>TVA</td><td class='money-pos text-right'>" . utils()->getMoney($this->sumTVA) . "</td></tr>";
    echo "<tr><td colspan=4 class='text-right font-weight-bold'>Total</td><td class='money-pos text-right'>" . utils()->getMoney($this->sumAmount + $this->sumTVA) . "</td></tr>";
    parent::buildAfterTable();
    if (!$this->onlyTable) {
      echo getButton([BUTTON_SIZE_SM => true, BUTTON_ROUNDED => true, BUTTON_ATTR => "type=submit form=orderForm", BUTTON_TEXT => "Enregistrer"]);
    }
  }

}
