<?php

class ToSale extends BuildTable
{

  public $currentFolder;
  public $table = [];
  public $already = [];
  public $stock;

  function getCurrentStock(): Stock
  {
    return $this->stock;
  }

  function __construct()
  {
    $res = dbUtil()->selectRow("products", "ri, children, name", "leafType=" . LEAF_TYPE_ROOT_FOLDER . " order by name");
    while ($tup = dbUtil()->fetch_assoc($res)) {
      $this->currentFolder = $tup["name"];
      $this->buildMemoryTable($tup);
    }
    parent::__construct("toSale", $this->table);
  }

  function buildMemoryTable($tup)
  {
    foreach (explode('-', substr($tup["children"], 1, -1)) as $ri) {
      if ($ri && ("$ri")[0] != LEAF_TYPE_MATERIAL) {
        $product = dbUtil()->fetch_assoc(dbUtil()->selectRow("products", "ri, name, leafType, children, duration, hasSerialNb", "ri=" . substr($ri, 1)));
        $product["leafType"] == LEAF_TYPE_FINISHED_PRODUCT && $this->buildRow($product);
        $this->buildMemoryTable($product);
      }
    }
  }

  function buildRow($tup)
  {
    // check if in stock
    $res = dbUtil()->selectRow("stock", "stockRef", "stockRef like '-" . LEAF_TYPE_PRODUCT . $tup["ri"] . "-%' group by stockRef");
    while ($stockRef = dbUtil()->fetch_row($res)) {
      if (array_search($stockRef[0], $this->already[$this->currentFolder] ?? []) === false) {
        $stock = new Stock(substr($stockRef[0], 1, -1));
        $sumary = $stock->getSumary();
        foreach ($sumary as $size => $quantity) {
          $this->table[] = ["size" => $size, "ri" => $stockRef[0], "table" => SALE_TABLE_STOCK, "folder" => $this->currentFolder, "hasSerialNb" => $tup["hasSerialNb"]];
        }
        $this->already[$this->currentFolder][] = $stockRef[0];
      }
    }
    if (!dbUtil()->num_rows($res)) {
      //check if simple products with only materials
      $onlyMaterial = true;
      foreach (explode('-', substr($tup["children"], 1, -1)) as $ri) {
        if (("$ri")[0] != LEAF_TYPE_MATERIAL) {
          $onlyMaterial = false;
          break;
        }
        !isset($stockRef) && ($stockRef = "-$ri-") || $stockRef = 0;
      }
      $tup["duration"] && $stockRef = 0;
      if ($onlyMaterial && array_search($tup["ri"], $this->already[$this->currentFolder] ?? []) === false) {
        $this->table[] = [
            "ri"          => $tup["ri"],
            "table"       => SALE_TABLE_PRODUCTS,
            "folder"      => $this->currentFolder,
            "name"        => $tup["name"],
            "stock"       => $stockRef,
            "hasSerialNb" => $tup["hasSerialNb"],
            "size"        => 1
        ];
        $this->already[$this->currentFolder][] = $tup["ri"];
      }
    }
  }

  function getDisplayValue($key, $row)
  {
    switch ($key) {
      case "name":
        if ($row["table"] == SALE_TABLE_STOCK) {
          return Stock::getStockName($row["ri"]);
        }
        return $row[$key];

      case "stock":
        return $this->stock->getSumaryText(true, $row["size"]);

      case "serialNb":
        if ($row["table"] == SALE_TABLE_STOCK) {
          return $this->getCurrentStock()->getLotsSerialNb();
        }
    }
    return parent::getDisplayValue($key, $row);
  }

  function getTdAttributes($key, $row, $attr)
  {
    return $key == "name" && $row["hasSerialNb"] ? addAttribute($attr, "color:" . LEAF_FINISHED_PRODUCT, "style") : parent::getTdAttributes($key, $row, $attr);
  }

  function buildLine($row)
  {
    $row["stockRef"] = substr($row["table"] == SALE_TABLE_STOCK ? $row["ri"] : $row["stock"], 1, -1);
    $this->stock = new Stock($row["stockRef"]);
    $this->stock->getSumary() && parent::buildLine($row);
  }

  public function getUserAction($row)
  {
    return $this->stock->getSumary() ? "<span style = display:none class = icon-action data-action = buy><i class = 'fas fa-shopping-cart'></i></span>" : '';
  }

}
