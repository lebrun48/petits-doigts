<?php

function axCustomersEdit()
{
  if (utils()->action == "choice" && ($customer = $_REQUEST["customer"]) != 0) {
    dbUtil()->updateRow("sales", "customer=$customer", "state=" . SALE_STATE_ORDER);
    return;
  }
  $row = dbUtil()->getCurrentEditedRow();
  $arForm = [
      "!xAction" => [ED_TYPE => ED_TYPE_HIDDEN, ED_VALUE => "submit"],
      "!action"  => [ED_TYPE => ED_TYPE_HIDDEN, ED_VALUE => utils()->action],
      "!page"    => [ED_TYPE => ED_TYPE_HIDDEN, ED_VALUE => "customers"],
      "!primary" => [ED_TYPE => ED_TYPE_HIDDEN, ED_VALUE => $_REQUEST["primary"]],
      "contact"  => [
          ED_LABEL    => "Nom",
          ED_TYPE     => ED_TYPE_ALPHA,
          ED_VALIDATE => [ED_VALIDATE_MAX_LENGTH => OTHER_PARTY_CONTACT_LEN, ED_VALIDATE_REQUIRED => true]
      ],
      "factory"  => [
          ED_LABEL    => "Entreprise",
          ED_TYPE     => ED_TYPE_ALPHA,
          ED_VALIDATE => [ED_VALIDATE_MAX_LENGTH => OTHER_PARTY_FACTORY_LEN]
      ],
      "email"    => [
          ED_LABEL    => "E-Mail",
          ED_TYPE     => "email",
          ED_VALIDATE => [ED_VALIDATE_REQUIRED=>true, ED_VALIDATE_MAX_LENGTH => OTHER_PARTY_EMAIL_LEN, ED_VALIDATE_INVALIDE=>"le caractère '@' est nécessaire"]
      ],
      "phoneNbr" => [
          ED_LABEL    => "Tél.",
          ED_TYPE     => "tel",
          ED_ATTR     => "pattern=[\d/\.+\s]*",
          ED_VALIDATE => [ED_VALIDATE_MAX_LENGTH => OTHER_PARTY_PHONE_LEN, ED_VALIDATE_INVALIDE => "Chiffre, '.', '+' ou '/' autorisé"]
      ],
      "address"  => [
          ED_LABEL    => "Adresse",
          ED_TYPE     => ED_TYPE_ALPHA,
          ED_VALIDATE => [ED_VALIDATE_MAX_LENGTH => OTHER_PARTY_ADDRESS_LEN]
      ],
  ];
  $options = [
      MSGBOX_MODAL_ID      => "customerModal",
      MSGBOX_BUTTON_ACTION => "Enregistrer",
      MSGBOX_BUTTON_CLOSE  => "Annuler",
  ];
  msgBox(BuildForm::getForm($arForm, $row, "customerForm"), "Edition client", MODAL_SIZE_SMALL, $options);
}

function axCustomersSubmit()
{
  try {
    if (utils()->action == "update") {
      include "salesMain.php";
      dbUtil()->updateRow();
      utils()->axRefreshElement("currentSales");
    }
    else {
      dbUtil()->insertRow();
      $insertId = dbUtil()->getDbCnx()->insert_id;

      $options[] = [ED_VALUE => 0, ED_CONTENT => "(nouveau Client)"];
      $options = array_merge($options, mdbCompos()->getOptions(dbUtil()->selectRow("customers", "ri, contact", "1 order by contact")));
      $edCustomer = [
          ED_DIV_ATTR => "style=width:200px id=customer",
          ED_NAME     => "!customer",
          ED_TYPE     => ED_TYPE_SELECT,
          ED_VALUE    => $insertId,
          ED_OPTIONS  => $options,
          ED_LABEL    => "Client"
      ];
      mdbCompos()->buildSelect($edCustomer);
      utils()->axExecuteJS("setSelectData", "customer", "data");
    }
    utils()->axExecuteJS("msgBoxClose", "customerModal");
  } catch (Exception $exc) {
    if ($exc->getCode() == E_DB_DUPLICATE) {
      msgBox(strpos($exc->getMessage(), "'email'") ? "<p>Cette adresse mail existe déjà!</p>" : "<p>Ce nom d'entreprise existe déjà!</p>"
              , "Nom existe Déjà"
              , null
              , [MSGBOX_MODAL_ATTR => [MODAL_NO_BACKDROP => true, MODAL_CENTERED => true, MODAL_NO_FADE => true]]);
      exit();
    }
    throw $exc;
  }
}
