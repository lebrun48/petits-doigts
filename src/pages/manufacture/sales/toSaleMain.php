<?php

include "salesMain.php";

function axToSaleEdit()
{
  $stock = new Stock($_REQUEST["primary"][0][1]);
  $sumary = $stock->getSumary();
  $form = [
      "!page"     => [ED_TYPE => ED_TYPE_HIDDEN, ED_VALUE => "toSale"],
      "!quantity" => [
          ED_FIELD_WIDTH => 12,
          ED_TYPE        => ED_TYPE_INTEGER,
          ED_LABEL       => "Quantité à vendre",
          ED_TEXT_AFTER  => units()->getUnitQuantityText(null, $_REQUEST["primary"][1][1], $stock->stockUnit[1]),
          ED_VALIDATE    => [
              ED_VALIDATE_REQUIRED  => true,
              ED_VALIDATE_MIN_VALUE => 1,
              ED_VALIDATE_MAX_VALUE => $sumary[$_REQUEST["primary"][1][1]],
              ED_VALIDATE_INVALIDE  => "Nombre entier de minimum 1 et maximum " . $sumary[$_REQUEST["primary"][1][1]]
          ]
      ],
      "!serialNb" => [
          ED_FIELD_WIDTH => 12,
          ED_TYPE        => ED_TYPE_ALPHA,
          ED_ATTR        => "pattern=[\d\s,.]*",
          ED_LABEL       => "N° de série",
          ED_VALIDATE    => [ED_VALIDATE_REQUIRED => true, ED_VALIDATE_INVALIDE => "N° de série séparés par &laquo;virgule&raquo;, &laquo;point&raquo ou &laquo;espace&raquo"]
      ],
      "!price"    => [
          ED_FIELD_WIDTH => 6,
          ED_TYPE        => ED_TYPE_FLOAT,
          ED_ATTR        => "class=remove-stepper",
          ED_LABEL       => "Prix (HTVA)",
          ED_TEXT_AFTER  => "€",
          ED_VALIDATE    => [
              ED_VALIDATE_REQUIRED  => true,
              ED_VALIDATE_MIN_VALUE => 0.1,
              ED_VALIDATE_INVALIDE  => "Nombre décimal supérieur à 0.1"
          ]
      ],
      "!tva"      => [
          ED_FIELD_WIDTH => 6,
          ED_TYPE        => ED_TYPE_SELECT,
          ED_LABEL       => "TVA",
          ED_OPTIONS     => [[ED_VALUE => 21, ED_CONTENT => "21%"], [ED_VALUE => 12, ED_CONTENT => "12%"], [ED_VALUE => 6, ED_CONTENT => "6%"]]
      ]
  ];

  if ($stock->getLotsSerialNb(true)) {
    unset($form["!quantity"]);
  }
  else {
    unset($form["!serialNb"]);
  }

  msgBox("<p style=font-size:small>" . Stock::getStockName($stock->stockRef) . "</p>" . BuildForm::getForm($form, null, "toSaleForm")
          , "Vente Produit"
          , MODAL_SIZE_SMALL
          , [MSGBOX_BUTTON_ACTION => "En commande", MSGBOX_BUTTON_CLOSE => "annuler"]
  );
}

function axToSaleSubmit()
{
  dbUtil()->begin_transaction();
  //create sale
  !($ri = dbUtil()->result(dbUtil()->selectRow("sales", "ri", "state=0"), 0)) &&
          dbUtil()->insertRow("sales", ["date" => utils()->now->format("y-m-d"), "state" => SALE_STATE_ORDER, "customer" => "=null"]) &&
          $ri = dbUtil()->getDbCnx()->insert_id;

  $stock = new Stock($_REQUEST["primary"][0][1]);
  if (!($quantity = $_REQUEST["quantity"])) {
    $allSN = array_column(dbutil()->fetch_all(dbUtil()->selectRow("serialNb", "ri, stockRef", "ri in (" . $stock->getLotsSerialNb(true) . ")")), 1, 0);
    $sn = strtok($_REQUEST["serialNb"], " ,.");
    while ($sn) {
      if (!$allSN[$sn]) {
        $err .= ", $sn";
      }
      else {
        $quantity++;
        $serialNbs[] = $sn;
      }
      $sn = strtok(" ,.");
    }
    if ($err) {
      substr_count($err = substr($err, 1), ",") && $plural = ["s", "ent"];
      msgBox("<p>Le$plural[0] N° de série $err ne correpond$plural[1] pas ou plus au produit.</p>"
              , "N° de série invalide"
              , null
              , [MSGBOX_MODAL_ID => "errSN", MSGBOX_MODAL_ATTR => [MODAL_NO_BACKDROP => true, MODAL_NO_FADE => true]]
      );
      exit();
    }
  }
  dbUtil()->insertRow("orders", [
      "stockRef"    => $stock->stockRef,
      "salesRef"    => $ri,
      "price"       => $_REQUEST["price"],
      "tva"         => $_REQUEST["tva"],
      "quantity"    => $quantity,
      "reservedRef" => '1',
  ]);
  $ri = dbUtil()->getDbCnx()->insert_id;
  if ($_REQUEST["quantity"]) {
    Lot::$usageRef = "S$ri";
    Lot::$saveReserved = true;
    $stock->$sumary = $stock->getSumary();
    $stock->reserve([$_REQUEST["primary"][1][1], $stock->stockUnit[1]], $quantity, null);
  }
  else {
    $allSN = array_column(dbutil()->fetch_all(dbUtil()->selectRow("serialNb", "ri, stockRef", "ri in (" . $stock->getLotsSerialNb(true) . ")")), 1, 0);
    foreach ($serialNbs as $sn) {
      $lots[$allSN[$sn]] ++;
      //reserve serialNb
      dbUtil()->updateRow("serialNb", "ordersRef=$ri", "ri=$sn");
    }
    Lot::$saveReserved = array_keys($lots);
    foreach ($lots as $lot => $quantity) {
      dbUtil()->insertRow("stockUsage", [
          "lotRef"    => $lot,
          "usageRef"  => $ri,
          "usageType" => 'S',
          "quantity"  => $quantity,
          "size"      => 1,
      ]);
    }
  }
  dbUtil()->updateRow("orders", ["reservedRef" => json_encode(Lot::$saveReserved, JSON_NUMERIC_CHECK)], "ri=$ri");


  dbUtil()->commit();
  utils()->axRefreshElement("toSale");
  utils()->axRefreshElement("order");
}
