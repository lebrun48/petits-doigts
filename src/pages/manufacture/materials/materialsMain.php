<?php

include "Units.php";

function buildMain()
{
  echo ""
  . "<main id='main'>"
  . "  <div class='card mt-3'>"
  . "    <div class=card-body>"
  . "      <h5 class='h4 text-center card-title m-0'>Matériaux</h5>";
  buildMaterialsBox();
  echo ""
  . "    </div>"
  . "  </div>"
  . "</main>";
}

function buildMaterialsBox()
{

  class Materials extends BuildTable
  {

    function getDisplayValue($key, $row)
    {
      switch ($key) {
        case "stock":
          $stock = new Stock($row);
          return $stock->getSumaryText();
      }
      return parent::getDisplayValue($key, $row);
    }

    public function getUserAction($row)
    {
      return '<span style="display:none" class=icon-action  data-action=buy><i class="fas fa-shopping-cart"></i></span>';
    }

  }

  echo "<div id=materialsBox>";
  $table = new Materials("materials");
  $table->theadAttr = "class='grey lighten-4'";
  $table->defaultOrderLast = "name";
  $table->otherDbCol = LEAF_TYPE_MATERIAL . " as leafType, unit";
  $table->buildFullTable();
  utils()->insertReadyFunction("materialsListener");
  echo "</div>";
}

function axMaterialsSubmit()
{
  switch (utils()->action) {
    case "insert":
    case "update":
    case "delete":
      break;

    case "buy":
      $row = dbUtil()->getCurrentEditedRow();
      $obj["current"][] = $obj["unit"] = $row["unit"];
      $tup["description"] = json_encode($obj, JSON_NUMERIC_CHECK);
      dbUtil()->insertRow("stock", [
          "date"      => utils()->now->format("Y-m-d"),
          "stockRef"  => '-' . LEAF_TYPE_MATERIAL . $row["ri"] . '-',
          "stockType" => LEAF_TYPE_MATERIAL,
          "products"  => json_encode($row, true),
          "quantity"  => $_REQUEST["unlimited"] ? -1 : $_REQUEST["quantity"],
          "size"      => $_REQUEST["size"],
          "unit"      => $row["unit"]
      ]);
      break;
  }
  try {
    utils()->axRefreshElement("materials");
  } catch (Exception $exc) {
    if ($exc->getCode() == E_DB_DUPLICATE) {
      msgbox("<p>Ce nom de matériau existe déjà!</p>", "Nom existant", MODAL_SIZE_SMALL, [MSGBOX_MODAL_ATTR => [MODAL_NO_BACKDROP => true, MODAL_CENTERED => true, MODAL_NO_FADE => true]]);
      exit();
    }
  }
}
