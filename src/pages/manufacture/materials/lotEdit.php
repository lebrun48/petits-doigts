<?php

function axMaterialsEdit()
{

  function getProviders()
  {
    $options[] = [ED_VALUE => 0, ED_CONTENT => "(nouveau fournisseur)"];
    $options = array_merge($options, mdbCompos()->getOptions(dbUtil()->selectRow("providers", "ri, factory", "1 order by factory")));
    return $options;
  }

  $row = dbUtil()->getCurrentEditedRow();
  switch (utils()->action) {
    case "delete" :
      msgBox("Pas encore Disponible");
      //msgBox("Confirmation de la suppression", "Confirmation", MODAL_SIZE_SMALL, [MSGBOX_BUTTON_ACTION=>"Supprimer", MSGBOX_BUTTON_CLOSE=>"Annuler"]);
      return;

    case "update":
    case "insert":
      $arForm = [
          ED_FORM_ATTR => "class='px-3'",
          "name"       => [
              ED_FIELD_WIDTH => 12,
              ED_TYPE        => ED_TYPE_ALPHA,
              ED_LABEL       => "name",
              ED_VALIDATE    => [
                  ED_VALIDATE_INVALIDE   => "Le nom est requis",
                  ED_VALIDATE_REQUIRED   => true,
                  ED_VALIDATE_MAX_LENGTH => MATERIAL_NAME_LEN,
              ]
          ],
          "provider"   => [
              ED_FIELD_WIDTH => 6,
              ED_TYPE        => ED_TYPE_SELECT,
              ED_LABEL       => "Fournisseur",
              ED_PLACEHOLDER => "Choisir fournisseur",
              ED_OPTIONS     => getProviders(),
          ],
          "ref"        => [
              ED_FIELD_WIDTH => 6,
              ED_TYPE        => ED_TYPE_ALPHA,
              ED_LABEL       => "Réf fournisseur",
              ED_VALIDATE    => [
                  ED_VALIDATE_MAX_LENGTH => 30,
              ]
          ],
          "unit"       => [
              ED_FIELD_WIDTH => 3,
              ED_TYPE        => ED_TYPE_SELECT,
              ED_VALUE       => $row["unit"],
              ED_OPTIONS     => units()->getOptions(),
              ED_LABEL       => "Unité du matériau",
              ED_PLACEHOLDER => "Choisir",
              ED_VALIDATE    => [ED_VALIDATE_REQUIRED => true]
          ],
      ];

      msgBox([
          MSGBOX_TITLE         => ($row ? "Modification" : "Création") . " matériau",
          MSGBOX_CONTENT       => BuildForm::getForm($arForm, $row),
          MSGBOX_BUTTON_ACTION => "Enregistrer",
          MSGBOX_BUTTON_CLOSE  => "Annuler",
      ]);
      return;

    case "buy" :
      $arForm = [
          "!quantity" => [
              ED_FIELD_WIDTH => 6,
              ED_TYPE        => ED_TYPE_INTEGER,
              ED_LABEL       => "Quantité",
              ED_TEXT_AFTER  => "unité de ",
              ED_VALUE       => 1,
              ED_VALIDATE    => [ED_VALIDATE_REQUIRED => true, ED_VALIDATE_INVALIDE => "Nombre entier requis"]
          ],
          "!size"     => [
              ED_FIELD_WIDTH => 6,
              ED_TYPE        => ED_TYPE_FLOAT,
              ED_LABEL       => "Taille",
              ED_TEXT_AFTER  => units()->getName($row["unit"]),
              ED_VALIDATE    => [ED_VALIDATE_REQUIRED => true, ED_VALIDATE_INVALIDE => "Ce champ est requis"]
          ]
      ];
      msgBox([
          MSGBOX_TITLE         => "Achat «" . $row["name"] . '»',
          MSGBOX_CONTENT       => BuildForm::getForm($arForm, $row),
          MSGBOX_BUTTON_ACTION => "Enregistrer",
          MSGBOX_BUTTON_CLOSE  => "Annuler",
      ]);
      return;
  }
}
