<?php

include "Units.php";

function buildMain()
{
  echo ""
  . "<main id='main' class='card mt-3'>"
  . "  <div class=card-body>"
  . "    <h5 class='h4 text-center card-title m-0'>Production</h5>"
  . "      <h4 class='ml-3 mt2 h5'>A produire</h4>"
  . "      <div class='border pl-2 ml-3 mb-2' id=toProduce>&nbsp;</div>"
  . "      <div class=row>"
  . "          <div class=col-5>"
  . "            " . BuildForm::getForm([
      "!toProduce"   => [ED_TYPE => ED_TYPE_HIDDEN],
      "!tree"        => [ED_TYPE => ED_TYPE_HIDDEN],
      "!productTree" => [ED_TYPE => ED_TYPE_HIDDEN],
      "!xAction"     => [ED_TYPE => ED_TYPE_HIDDEN, ED_VALUE => "produce"],
      0              => [ED_INSERT_HTML => getButton([BUTTON_ATTR => "id=productionSubmit disabled type=submit", BUTTON_TEXT => "Produire", BUTTON_ROUNDED => true, BUTTON_SIZE_SM => true])]
          ], null, "productionForm")
  . "          <hr>";
  buildProductionTree();
  echo ""
  . "        </div>"
  . "        <div class=col-7>";
  buildProductionBox();
  echo ""
  . "      </div>"
  . "    </div>"
  . "  </div>"
  . "</main>";
}

function buildProductionTree()
{
  echo ""
  . "<div class='treeview w-100 dropdown'>";
  $tree = new ProductionTree();
  $tree->buildTree();
  utils()->insertReadyFunction("productionListener");
  echo ""
  . "</div>";
}

function buildProductionBox()
{
  echo "<div id=productionBox>";
  $table = new ProductionTable("production");
  $table->theadAttr = "class='grey lighten-4'";
  $table->defaultOrderLast = "stockType desc, stockRef";
  $table->buildFullTable();
  echo "</div>";
  utils()->insertReadyFunction("productionTableListener");
}

function axProductionProduce()
{
  //get current stock
  $line = "<p style=font-size:small>" . $_REQUEST["toProduce"] . "</p>";
  $production = new Production();
  $production->removeUselessProduct();
  if (!count($production->key)) {
    msgBox("Il n'y a rien à produire!", null, MODAL_SIZE_SMALL);
    exit();
  }
  $stock = new Stock(implode('-', $production->key));
  $line .= "<div class='d-flex'><span class='mr-1 font-weight-bold'>Stock actuel:</span>" . $stock->getSumaryText(false) . "</div>";
  $product = dbUtil()->fetch_assoc(dbUtil()->selectRow("products", "size, unit", "ri=" . substr($production->key[0], 1)));
  $form = [
      "!tree"        => [ED_TYPE => ED_TYPE_HIDDEN, ED_VALUE => $_REQUEST["tree"]],
      "!productTree" => [ED_TYPE => ED_TYPE_HIDDEN, ED_VALUE => $_REQUEST["productTree"]],
      "!quantity"    => [
          ED_FIELD_WIDTH => 12,
          ED_TYPE        => ED_TYPE_INTEGER,
          ED_LABEL       => "Nombre à produire",
          ED_TEXT_AFTER  => units()->getUnitQuantityText(null, $product["size"], $product["unit"]),
          ED_VALIDATE    => [
              ED_VALIDATE_REQUIRED  => true,
              ED_VALIDATE_MIN_VALUE => 1,
              ED_VALIDATE_INVALIDE  => "Nombre entier de minimum 1"
          ]
      ],
  ];

  $line .= BuildForm::getForm($form);
  msgBox($line, "Produire", MODAL_SIZE_SMALL, [MSGBOX_BUTTON_ACTION => "Produire", MSGBOX_BUTTON_CLOSE => "Annuler", MSGBOX_INPUT_XACTION => "buildProduct"]);
}

function axProductionBuildProduct()
{
  include "Production.php";
  include "ShowProduction.php";

  $production = new Production;

  $production->buildProduction();

  //debugLog($production->stocks, "stocks");

  $showProd = new ShowProduction($production);
  $result = $showProd->displayProduction();
}

function axRegisterProd()
{
  include "Production.php";
  include "ShowProduction.php";

  $production = new Production;
  dbUtil()->begin_transaction();

  $production->buildProduction();
  dbUtil()->commit();

  utils()->axRefreshElement("production");
}

function axProductionEdit()
{
  switch (utils()->action) {
    case "delete":
      msgBox("<p class=font-weight-bold>Cette opération est irréversible!</p><p>Confirmez la suppression de la production.</p>",
             "Confirmation suppression",
             null,
             [
                  MSGBOX_BUTTON_CLOSE  => "annuler",
                  MSGBOX_BUTTON_ACTION => "confirmation"
      ]);
      break;

    case "applyProd":
      msgBox("<p class=font-weight-bold>Cette opération est irréversible!</p><p>Les stocks seront adapter pour cette production et les N° de séries seront appliqués aux produits finis.</p>",
             "",
             null,
             [
                  MSGBOX_BUTTON_CLOSE  => "annuler",
                  MSGBOX_BUTTON_ACTION => "confirmation production"
      ]);
      break;
  }
}

function axProductionSubmit()
{
  switch (utils()->action) {
    case "delete" :

      dbUtil()->begin_transaction();
      deleteProduction($_REQUEST["primary"][0][1]);
      utils()->axRefreshElement("production");
      dbUtil()->commit();
      break;

    case "applyProd":
      dbUtil()->begin_transaction();
      $prodRi = $_REQUEST["primary"][0][1];
      //update isProduction
      dbUtil()->updateRow("stock", ["isProduction" => 0], "ri=$prodRi");
      //insert production in sale
      if ($ar = json_decode(dbUtil()->result(dbUtil()->selectRow("stock", "serialNb", "ri=$prodRi"), 0))) {
        foreach ($ar as $serialNb) {
          for ($i = 0; $i < $serialNb->toProduce; $i++) {
            dbUtil()->insertRow("serialNb", ["stockRef" => $prodRi]);
          }
          $serialNbAr = dbUtil()->fetch_row(dbUtil()->selectRow("serialNb", "min(ri), max(ri)", "stockRef=$prodRi"));
          $content .= "<p>N° <span class=font-weight-bold>$serialNbAr[0] " . ($serialNbAr[1] != $serialNbAr[0] ? "à $serialNbAr[1]" : '')
                  . "</span> pour produit &laquo;$serialNb->prodName&raquo;</p>";
        }
      }
      //move serialNb from stock product
      dbUtil()->updateRow("serialNb", "stockRef=$prodRi, usedBy=null", "usedBy='P$prodRi'");
      dbUtil()->commit();
      $content && msgBox("<p class=font-weight-bold>Application des N° de séries</p>$content");
      utils()->axRefreshElement("production");
      break;
  }
}

function deleteProduction($prodRi)
{
  //remove from stock
  dbUtil()->deleteRow("stockUsage", "usageRef=$prodRi and usageType='P'");
  //remove from serialNb
  dbUtil()->updateRow("serialNb", "usedBy=null", "usedBy='$prodRi'");
}
