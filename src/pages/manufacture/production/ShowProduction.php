<?php

class ShowProduction
{

  public $prod;
  public $tab = 0;
  public $result;
  public $title;

  function __construct(Production $prod)
  {
    $this->prod = $prod;
  }

  function displayProduction()
  {
    obStart();
    $this->tab = 0;
    echo ""
    . "<div class='card p-3'>"
    . "  <table class=table-sm>"
    . "    <tr><th>Nom</th><th>Quantité</th><th>Stock restant</th><th>N° de série</th></tr>" //<th>Temps production</th><th>Coût Production></th>
    . "    <tr><td colspan=5 class=group-col>Produits & matériaux en stock</td></tr>";
    foreach ($this->prod->stocks as $stockRef => $stock) {
      $stock = $this->prod->getStock($stockRef);
      if (!$this->title) {
        $this->title = "Production de " . units()->getUnitQuantityText($stock->toProduce, $stock->stockUnit[0], $stock->stockUnit[1])
                . " du produit<br> &laquo;" . $this->prod->getProductName($stockRef) . "&raquo";
        continue;
      }
      switch (("$stockRef")[0]) {
        case LEAF_TYPE_MATERIAL:
          if ($stock->fromStock) {
            echo "<tr><td>Matériau &laquo;" . $this->prod->getProductName($stockRef) . "&raquo;</td><td>"
            . "$stock->fromStock " . units()->getName($stock->stockUnit[1], $stock->fromStock) . "</td>";
            echo "<td>" . $stock->getSumaryText(false) . "</td></tr>";
          }
          if ($stock->toProduce) {
            $result ++;
            echo "<tr class='text-danger'><td class=font-weight-bold>Matériau &laquo;" . $this->prod->getProductName($stockRef) . "&raquo;</td><td class=font-weight-bold>"
            . "Réassortir $stock->toProduce " . units()->getName($stock->stockUnit[1], $stock->toProduce) . "</td></tr>";
          }
          break;

        case LEAF_TYPE_PRODUCT:
          if ($stock->fromStock) {
            $this->prod->products[strtok($stockRef, '-')]["hasSerialNb"] && $color = " style=color:" . LEAF_FINISHED_PRODUCT;
            echo "<tr$color><td>Produit &laquo;" . $this->prod->getProductName($stockRef) . "&raquo;</td><td>"
            . "$stock->fromStock " . units()->getName($stock->stockUnit[1], $stock->fromStock) . "</td>";
            echo "<td>" . $stock->getSumaryText(false) . "</td>";
            echo "<td class=font-weight-bold>" . $stock->getLotsSerialNb() . "</td></tr>";
            unset($color);
          }
          break;
      }
    }

    obStart();
    foreach ($this->prod->stocks as $stockRef => $stock) {
      $stock = $this->prod->getStock($stockRef);
      if (!$stock->isFirstStock && ("$stockRef")[0] == LEAF_TYPE_PRODUCT && $stock->toProduce) {
        $product = $this->prod->products[strtok($stockRef, '-')];
        if ($product["duration"]) {
          echo "<tr><td>Produit &laquo;" . $this->prod->getProductName($stockRef) . "&raquo;</td><td>"
          . units()->getUnitQuantityText($stock->toProduce, $stock->stockUnit[0], $stock->stockUnit[1]) . "</td></tr>";
        }
      }
    }
    if ($content = ob_get_clean()) {
      echo ""
      . "    <tr><td colspan=5 class=group-col>A produire</td></tr>"
      . "    $content";
    }
    echo ""
    . "  </table>"
    . "</div>";

    $content = ob_get_clean();
    if ($result) {
      $content = "<h4 class='h4 border p-2 danger-color-dark white-text mb-4'>Impossible de produire.<br> Les stocks de $result matériaux sont insuffisants!</h4>" . $content;
      msgBox($content, "Résultat production", MODAL_SIZE_LG, [MSGBOX_MODAL_ATTR => [MODAL_SCROLLABLE => true, MODAL_NO_BACKDROP => true, MODAL_NO_FADE => true]]);
      exit();
    }

    $content = "<h4 class='h6 border p-2 mb-4'>$this->title</h4>" . $content;
    $args = ["xAction" => "registerProd", "tree" => $this->prod->tree, "quantity" => $_REQUEST["quantity"], "productTree" => $this->prod->key];
    msgBox($content, "Résultat production", MODAL_SIZE_LG, [
        MSGBOX_MODAL_ATTR    => [MODAL_SCROLLABLE => true, MODAL_NO_BACKDROP => true, MODAL_NO_FADE => true],
        MSGBOX_BUTTON_ACTION => ["Enregister", "onclick=axExecute(" . json_encode($args) . ')'],
        MSGBOX_BUTTON_CLOSE  => "annuler"]);

    exit();
  }

}
