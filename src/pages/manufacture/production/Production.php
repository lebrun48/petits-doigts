<?php

class Production
{

  public $tree;
  public $key = [];
  //$products = [product Tuple from $key, ...]
  public $products;
  //$productBranches => way to follow to get from stock or produce each stockRef ["productKey"=>[productKey in productTrees, ...], ...]
  public $productBranches;
  //$productTrees = [possible produtsTree in stock. String format: '-xx-yyy-', ....]
  public $productTrees;
  public $stocks;
  public $quantity;
  public $stockType;
  //list of product tree alreaduy produced
  public $produced = [];
  //production on serialProduct
  public $serialNb;
  public $stockUsed;

  function getProductName($stockRef)
  {
    foreach (explode('-', $stockRef) as $ri) {
      $name .= "-" . $this->products[$ri]["name"];
    }
    return substr($name, 1);
  }

  public function __construct()
  {
    dbUtil()->tables["production"][DB_COLS_DEFINITION] = [
        "name"     => [COL_TITLE => "Nom", COL_TD_ATTR => "class=td-1"],
        "ri"       => [COL_PRIMARY => true, COL_HIDDEN => true],
        "stock"    => [COL_TITLE => "Stock", COL_NO_DB => true],
        "provider" => [
            COL_GROUP        => 0,
            COL_GROUP_ACTION => "updProvider",
            COL_TD_ATTR      => "colspan=4 class=group-col",
            COL_EXT_REF      => "providers",
            COL_DB           => "factory"
        ],
        "ref"      => [COL_TITLE => "Réf. matériau"],
    ];

    $this->tree = is_array($_REQUEST["tree"]) ? $_REQUEST["tree"] : json_decode($_REQUEST["tree"]);
    $this->key = is_array($_REQUEST["productTree"]) ? $_REQUEST["productTree"] : json_decode($_REQUEST["productTree"], true);
  }

  function removeUselessProduct()
  {
    foreach ($this->key as $idx => $product) {
      if (dbUtil()->result(dbUtil()->selectRow("products", "children", "ri=" . substr($product, 1)), 0) == '-') {
        unset($this->key[$idx]);
      }
    }
  }

  function readProducts()
  {
    $i = 0;
    //Compute usage
    //read all products
    foreach ($this->key as $idx => $product) {
      if (!($tup = $this->products[$product])) {
        $tup = dbUtil()->fetch_assoc(dbUtil()->selectRow("products", "*", "ri=" . substr($product, 1)));
        $tup["childrenUsage"] = json_decode($tup["childrenUsage"], true);
        $tup["children"] = explode('-', substr($tup["children"], 1, -1));
      }
      if (!count($tup["children"])) {
        unset($this->key[$idx]);
        continue;
      }
      //get parent usage
      foreach ($this->tree as $branch) {
        if (($parent = array_search($product, $branch)) !== false) {
          $parent--;
          $usages = $tup["products"][$branch[$parent]]["childrenUsage"] ??
                  json_decode(dbUtil()->result(dbUtil()->selectRow("products", "childrenUsage", "ri=" . substr($branch[$parent], 1)), 0), true);
          $tup["usage"] = $usages[$product];
          break;
        }
      }
      $this->products[$product] = $tup;
      $this->productBranches[$product] = [$product];
      !$this->stockType && $this->stockType = $tup["leafType"];
    }
    $this->key = array_values($this->key);
  }

  function registerStockableProducts()
  {
    //determine stockable product in productKeys
    foreach ($this->key as $idx => $product) {
      $this->productTrees[$product] = "-$product-";
      foreach ($this->tree as $idxBranch => $branch) {
        if (array_search($product, $branch) !== false) {
          foreach (array_slice($this->key, $idx + 1) as $idx1 => $productKey) {
            foreach (array_slice($this->tree, $idxBranch) as $branch2) {
              if (array_search($product, $branch2) !== false) {
                if (array_search($productKey, $branch2) !== false) {
                  //store product if not empty
                  $this->productTrees[$product] .= "$productKey-";
                  break;
                }
              }
              else {
                break;
              }
            }
          }
        }
        break;
      }
    }
    //debuglog($this->productTrees);
  }

  function addSerialNb(Stock $stock)
  {
    if ($this->products[strtok($stock->stockRef, '-')]["hasSerialNb"]) {
      $this->serialNb[] = ["prodTree" => $stock->stockRef, "prodName" => $this->getProductName($stock->stockRef), "toProduce" => ceil($stock->toProduce) * $stock->stockUnit[0]];
    }
  }

  function registerProductBranches()
  {
    //Complete $productBranches. Will contain the array of product branches
    try {
      $firstStock = new Stock();
      $firstStock->stockRef = implode('-', $this->key);
      $firstStock->isFirstStock = true;
      $firstStock->stockUnit = [$this->products[$this->key[0]]["size"], $this->products[$this->key[0]]["unit"]];
      $firstStock->fromStock = 0;
      $firstStock->toProduce = $this->quantity = $_REQUEST["quantity"];
      $this->stocks[implode('-', $this->key)] = $firstStock;
      $offset = 1;
      foreach ($this->productTrees as $productA => $branchA) {
        //getStock
        $stockRef = substr($branchA, 1, -1);
        !$this->stocks[$stockRef] && $this->stocks[$stockRef] = new Stock($this->products[$productA], $stockRef);
        foreach (array_slice($this->productTrees, $offset++, null, true) as $productB => $branchB) {
          if (strpos($branchA, $branchB) !== false) {
            $this->productBranches[$productA][] = $productB;
            $branchA = str_replace($branchB, '-', $branchA);
          }
        }
      }
    } catch (Exception $exc) {
      if ($exc->getCode() == E_USER_ERROR) {
        msgBox("L'utilisation du produit &laquo;" . $this->getProductName($productA) . "&raquo; n'est pas dans la même unité partout!");
        exit();
      }
      throw $exc;
    }
    foreach ($this->productBranches as $key => $ar) {
      if ($key != array_key_first($this->productBranches) && count($ar) == 1) {
        unset($this->productBranches[$key]);
      }
    }
    //debugLog($this->productBranches);
  }

  function registerProductionInStock()
  {
    //$products = [product Tuple from $key, ...]
    //$productTrees = [possible produtsTree in stock. String format: '-xx-yyy-', ....]
    //$productBranches => way to follow to get from stock or produce each stockRef ["productKey"=>[productKey in productTrees, ...], ...]
    //1) register production in stock
    //$tup["products"] = $products;
    dbUtil()->insertRow("stock", [
        "quantity"     => $this->quantity,
        "size"         => ($product = $this->products[$this->key[0]])["size"],
        "unit"         => $product["unit"],
        "isProduction" => '1',
        "date"         => utils()->now->format("Y-m-d"),
        "stockRef"     => '-' . implode('-', $this->key) . '-',
        "stockType"    => $this->stockType
    ]);
    Lot::$usageRef = 'P' . dbUtil()->getDbCnx()->insert_id;
  }

  function reserveInStock($leafRI, $toProduce)
  {
    foreach ($this->productBranches[$leafRI] as $idx => $productTreeIdx) {
      $stockRef = substr($this->productTrees[$productTreeIdx], 1, -1);
      $stock = $this->getStock($stockRef);
      // first element is already reserved
      if ($toProduce) {
        if ($idx != 0 && $this->productBranches[$productTreeIdx]) {
          $this->reserveInStock($productTreeIdx, $toProduce);
        }
        else {
          $prodToProduce = $toProduce;
          $tup = $this->products[$productTreeIdx];
          try {
            $serialNb = [];
            !$stock->isFirstStock && $prodToProduce = $stock->reserve($tup["usage"], $prodToProduce, [$tup["size"], $tup["unit"]], $serialNb);
          } catch (Exception $e) {
            if ($e->getCode() == E_USER_ERROR) {
              msgBox($e->getMessage() . " pour le produit &laquo;" . $this->getProductName($stockRef) . "&raquo;");
              exit();
            }
            throw $e;
          }
          $tup["hasSerialNb"] && $serialNb && $this->updateSerialNb($serialNb);
          unset($serialNb);
          if ($idx == 0 && !($toProduce = $prodToProduce)) {
            return;
          }
          $this->produceProduct($stock, $this->products[$productTreeIdx], $prodToProduce);
          $stock->toProduce && $this->addSerialNb($stock);
        }
      }
    }
  }

  function updateSerialNb($serialNb)
  {
    foreach ($serialNb as $lot => $quantity) {
      dbUtil()->updateRow("serialNb", "usedBy='" . Lot::$usageRef . "'", "stockRef=$lot and usedBy is null limit $quantity");
    }
  }

  function produceProduct(Stock $stock, $leaf, $toProduce)
  {
    foreach ($leaf["children"] as $ri) {
      unset($toProduceStock);
      switch ($ri[0]) {
        case LEAF_TYPE_MATERIAL:
          $tup = $this->products[$ri] ??
                  dbUtil()->fetch_assoc(dbUtil()->selectRow("materials", LEAF_TYPE_MATERIAL . " as leafType, ri, name, unit", "ri=" . substr($ri, 1)));
          $usage = $leaf["childrenUsage"][$ri];
          $this->products[$ri] = $tup;
          array_search($ri, $stock->toProduceStock) === false && $stock->toProduceStock[] = $ri;
          try {
            $toProduceStock = $this->stocks[$ri] ?? ($this->stocks[$ri] = new Stock($tup));
            $stock->isChoiceStock && $prevStock = $toProduceStock->fromStock;
            $toProduceStock->reserve($usage, $toProduce, [1, $tup["unit"]]);
          } catch (Exception $e) {
            if ($e->getCode() == E_USER_ERROR) {
              msgBox($toProduceStock ? "Impossible de réserver des " . units()->getName($leaf["childrenUsage"][$ri][1])
                              . " sur un stock en " . units()->getName($toProduceStock->stockUnit) . " pour le produit &laquo;" . $this->getProductName($ri) . "&raquo;" :
                              "L'unité d'utilisation pour le produit &laquo;" . $this->getProductName($ri) . "&raquo; n'est pas la même partout");
              exit();
            }
            throw $e;
          }
          if ($stock->isChoiceStock && !$stock->toProduce -= $toProduceStock->fromStock - $prevStock) {
            return;
          }
          break;

        case LEAF_TYPE_PRODUCT:
          if (array_search($ri, array_keys($this->productBranches)) !== false) {
            break;
          }

          if (!($tup = $this->products[$ri])) {
            $tup = dbUtil()->fetch_assoc(dbUtil()->selectRow("products", "leafType, ri, name, unit, children, childrenUsage, size, unit, duration, hasSerialNb", "ri=" . substr($ri, 1)));
            $tup["childrenUsage"] = json_decode($tup["childrenUsage"], true);
            $tup["children"] = explode('-', substr($tup["children"], 1, -1));
            $this->products[$ri] = $tup;
          }
          try {
            $toProduceStock = $this->stocks[$ri] ?? ($this->stocks[$ri] = new Stock($tup));
            array_search($ri, $stock->toProduceStock) === false && $stock->toProduceStock[] = $ri;
            $toProduce = $toProduceStock->reserve($leaf["childrenUsage"][$ri], $toProduce, [$tup["size"], $tup["unit"]], $serialNb);
          } catch (Exception $e) {
            if ($e->getCode() == E_USER_ERROR) {
              msgBox($toProduceStock ? "Impossible de réserver des " . units()->getName($leaf["childrenUsage"][$ri][1])
                              . " sur un stock en " . units()->getName($toProduceStock->stockUnit) . " pour le produit &laquo;" . $this->getProductName($ri) . "&raquo;" :
                              "L'unité d'utilisation pour le produit &laquo;" . $this->getProductName($ri) . "&raquo; n'est pas la même partout");
              exit();
            }
            throw $e;
          }
          $tup["hasSerialNb"] && $serialNb && $this->updateSerialNb($serialNb);
          if ($stock->isChoiceStock && !($stock->toProduce -= $toProduceStock->fromStock)) {
            return;
          }
          if ($toProduce) {
            $this->produceProduct($toProduceStock, $tup, $toProduce);
          }
          if (!$toProduceStock->fromStock && !$toProduceStock->toProduceStock) {
            //useless product stock
            unset($this->stocks[$ri]);
          }
          break;

        case LEAF_TYPE_PRODUCT_CHOICE:
          if (!($tup = $this->products[$ri])) {
            $tup = dbUtil()->fetch_assoc(dbUtil()->selectRow("products", "leafType, ri, name, children, childrenUsage", "ri=" . substr($ri, 1)));
            $tup["childrenUsage"] = json_decode($tup["childrenUsage"], true);
            $tup["children"] = explode('-', substr($tup["children"], 1, -1));
            $this->products[$ri] = $tup;
          }
          array_search($ri, $stock->toProduceStock) === false && $stock->toProduceStock[] = $ri;
          $toProduceStock = $this->stocks[$ri] ?? ($this->stocks[$ri] = new Stock($tup));
          $toProduceStock->isChoiceStock = true;
          $toProduceStock->toProduce = $toProduce;
          $this->produceProduct($toProduceStock, $tup, $toProduce);
          break;
      }
    }
  }

  function updateProduction()
  {
    foreach ($this->products as $ref => $product) {
      if ($product["leafType"] != LEAF_TYPE_MATERIAL) {
        $products[$ref] = $product;
      }
    }
    $this->serialNb && dbUtil()->updateRow("stock", ["serialNb" => json_encode($this->serialNb), "products" => json_encode($products, JSON_NUMERIC_CHECK)], "ri=" . substr(Lot::$usageRef, 1));
  }

  function getStock($idx): Stock
  {
    return $this->stocks[$idx];
  }

  function BuildProduction()
  {
    dbUtil()->begin_transaction();
    //Compute usage
    //read all products
    $this->readProducts();

    //determine stockable product in productKeys
    $this->registerStockableProducts();

    //Complete $productBranches. Will contain the array of product branches
    $this->registerProductBranches();

    // register production in stock
    $this->registerProductionInStock();

    // take from stock and/or set production values
    $this->reserveInStock($this->key[0], $this->quantity);

    // update production (stock) desc
    $this->updateProduction();
  }

}
