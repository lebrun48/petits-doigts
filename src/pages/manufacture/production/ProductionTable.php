<?php

class ProductionTable extends BuildTable
{
  public $prevRow;
  public $stocks;

  function getTdAttributes($key, $row, $attr)
  {
    if ($key == "stockRef") {
      return addAttribute($attr, htmlspecialchars($this->name, ENT_QUOTES), "title");
    }
    return parent::getTdAttributes($key, $row, $attr);
  }
  
  function buildLine($row)
  {
    if (!$row["isProduction"]) {
      if (!$this->prevRow) {
        $this->prevRow = $row;
        return;
      }
      if ($this->prevRow["stockRef"] == $row["stockRef"]) {
        return;
      }
      parent::buildLine($this->prevRow);
      $this->prevRow = $row;
      return;
    }
    parent::buildLine($row);
  }
  
  function buildAfterTable()
  {
    $this->prevRow && parent::buildLine($this->prevRow);
    parent::buildAfterTable();
  }

  function getDisplayValue($key, $row)
  {
    switch ($key) {
      case "stockRef":
        unset($this->name);
        $ar = explode('-', substr($row[$key], 1, -1));
        foreach ($ar as $ri) {
          $this->name .= '-' . dbUtil()->result(dbUtil()->selectRow("products", "name", "ri=" . substr($ri, 1)), 0);
        }
        $this->name = substr($this->name, 1);
        $row["isProduction"] && $quantity = $row["quantity"];
        return ($quantity ? "<span class=font-weight-bold>$quantity X </span>" : '')
                . "<span" . ($row["stockType"] == LEAF_TYPE_FINISHED_PRODUCT ? " style=color:" . LEAF_FINISHED_PRODUCT : '') . ">$this->name</span>";

      case "isProduction" :
        return $row[$key] ? "En production" : "En stock";

      case "quantity" :
        $stock = new Stock(substr($row["stockRef"], 1, -1));
        return $stock->getSumaryText();
    }
    return parent::getDisplayValue($key, $row);
  }

  function getUserAction($row)
  {
    if ($row["isProduction"]) {
      return parent::getActionIcon($action) . '<span style="display:none" class=icon-action  data-action=applyProd><i class="fas fa-check fa-lg"></i></span>';
    }
  }
  
  function getRightsOnLine($row, $currentRights)
  {
    if (!$row["isProduction"]) {
      return TABLE_RIGHT_ONLY_DISPLAY;
    }
    return parent::getRightsOnLine($row, $currentRights);
  }
  
}
