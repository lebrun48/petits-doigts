<?php

class Leaf
{

  public $tree;
  public $lastElt;
  public $ri;
  public $realRI;
  public $type;
  public $parent;
  public $parentType;
  public $parentRI;
  public $parentRealRI;
  public $content;
  public $table;
  public $leafId;

  public function __construct()
  {
    if ($this->leafId = $_REQUEST["leafId"]) {
      $this->type = $this->leafId[0];
      $this->tree = explode('-', substr($this->leafId, 2, -1));
      $this->lastElt = sizeof($this->tree) - 1;
      $this->ri = $this->tree[$this->lastElt];
      $this->realRI = substr($this->ri, 1);
      if ($this->tree[0] == 0) {
        utils()->action = LEAF_MENU_NEW_FOLDER;
        $this->type = LEAF_TYPE_ROOT_FOLDER;
        return;
      }
      $this->content = $this->getLeaf();
      $this->table = $this->type == LEAF_TYPE_MATERIAL ? "materials" : "products";
    }
    if ($tmp = $_REQUEST["parent"]) {
      $tree = explode('-', substr($tmp, 2, -1));
      sizeof($tree) && $parentRi = $tree[sizeof($tree) - 1];
    }
    else {
      $this->lastElt && $parentRi = $this->tree[$this->lastElt - 1];
    }
    if ($parentRi) {
      $this->parentRI = $parentRi;
      $this->parentRealRI = substr($parentRi, 1);
      $this->parent = dbUtil()->fetch_assoc(dbUtil()->selectRow("products", "*", "ri=$this->parentRealRI"));
      $this->parent && ($this->parentType = $this->parent["leafType"]);
    }
  }

  function getLeaf()
  {
    if ($this->ri[0] == LEAF_TYPE_MATERIAL) {
      return dbUtil()->fetch_assoc(dbUtil()->selectRow("materials", "*, 1 as isTerminal, " . LEAF_TYPE_MATERIAL . " as leafType", "ri=$this->realRI"));
    }
    return dbUtil()->fetch_assoc(dbUtil()->selectRow("products", "*", "ri=$this->realRI"));
  }

  static function getChildrenExceptTypes($type, $list, $separator = null)
  {
    is_array($list) && $list = '-' . implode('-', $list) . '-';
    preg_match_all("/-[^$type](\d+)/", $list, $ar);
    return !$separator ? $ar[1] : ($ar[1] ? implode($separator, $ar[1]) : '');
  }

  static function getChildrenByType($type, $list, $separator = null)
  {
    is_array($list) && $list = '-' . implode('-', $list) . '-';
    preg_match_all("/-[$type](\d+)/", $list, $ar);
    return !$separator ? $ar[1] : ($ar[1] ? implode($separator, $ar[1]) : '');
  }

}
