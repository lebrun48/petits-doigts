<?php

class TimesheetEdit extends Timesheet
{

  public function __construct()
  {
    parent::__construct();
    $wrkTime = ConfigProject::get()->getWorkTime();
    $_REQUEST["primary"] && ($row = dbUtil()->fetch_assoc(dbUtil()->selectRow(
                    ["timesheet", "left join tasks tsk on tsk.ri=tsh.task"],
                    "tsk.project as project, task, time, comment, started, day, worker, tsh.ri as ri",
                    "tsh.ri=" . $_REQUEST["primary"][0])));
    $res = dbUtil()->selectRow(["tasks", "left join projects prj on prj.ri=tsk.project"],
                               "project, prj.name",
                               "ended is null and prj.ri>0 and (role=0 or '" . utils()->userSession()["roles"] . "' like concat('%-',tsk.role,'-%')) group by project");
    //get the one before
    $n = mysqli_num_rows($res);
    $isToday = $_REQUEST["activeDay"] == utils()->now()->format("Ymd");
    $started = $row["started"];

    //get first and last
    $where = "day=" . $_REQUEST["activeDay"] . " and worker=" . $_REQUEST["worker"] . " and started<>0";
    $firstLast = dbUtil()->fetch_all(dbUtil()->query("(SELECT ri,running,started,time FROM timesheet WHERE $where ORDER BY started LIMIT 1) union "
                    . "(SELECT ri, running,started, time FROM timesheet WHERE $where ORDER BY started DESC LIMIT 1)"));

    $fields = [
        ED_FORM_ATTR => "class=nofocus",
        "!project"   => [
            ED_TYPE        => ED_TYPE_SELECT,
            ED_SET_FOCUS   => true,
            ED_LABEL       => "Projet",
            ED_PLACEHOLDER => '',
            ED_OPTIONS     => mdbCompos()->getOptions($n ? $res : [null, "Pas de projet prévu pour toi"], $n ? null : "class='font-italic text-dark' disabled"),
            ED_VALIDATE    => [ED_VALIDATE_REQUIRED => true],
            ED_FIELD_WIDTH => 12
        ],
        "task"       => [
            ED_TYPE        => ED_TYPE_SELECT,
            ED_LABEL       => "Tâche",
            ED_PLACEHOLDER => '',
            ED_OPTIONS     => ($project = $row["project"]) ?
            mdbCompos()->getOptions(dbUtil()->selectRow("tasks", "ri,name", "project=$project and (role=0 or '" . utils()->userSession()["roles"] . "' like concat('%-',tsk.role,'-%'))")) :
            mdbCompos()->getOptions([null, "Sélectionner d'abord un projet"], "class='font-italic text-dark' disabled"),
            ED_VALIDATE    => [ED_VALIDATE_REQUIRED => true, ED_VALIDATE_INVALIDE => "Requis"],
            ED_FIELD_WIDTH => 12
        ],
        "!timeFrom"  => [
            ED_LABEL                => "Heure début",
            ED_TYPE                 => ED_TYPE_ALPHA,
            ED_DISABLED             => $firstLast && $row["ri"] != $firstLast[0][0],
            ED_TEXT_SELECT_ON_FOCUS => true,
            ED_ATTR                 => 'autocomplete=off pattern=\d{1,2}((:|h|\.)\d{1,2})?$',
            ED_VALUE                => utils()->translateToEdTime(!$row && sizeof($firstLast) ? ($last = $firstLast[sizeof($firstLast) - 1])[2] + $last[3] : $started),
            ED_VALIDATE             => [ED_VALIDATE_INVALIDE => "Invalide format h:m ou h.d (ex 5:15, 5h15, 5.25, 2)"],
            ED_FIELD_WIDTH          => 6
        ],
        "!timeTo"    => [
            ED_LABEL                => "Heure arrêt",
            ED_TYPE                 => ED_TYPE_ALPHA,
            ED_DISABLED             => $row && $row["ri"] != $firstLast[sizeof($firstLast) - 1][0],
            ED_TEXT_SELECT_ON_FOCUS => true,
            ED_ATTR                 => 'autocomplete=off pattern=\d{1,2}((:|h|\.)\d{1,2})?$',
            ED_VALUE                => utils()->translateToEdTime($started + $row["time"]),
            ED_VALIDATE             => [ED_VALIDATE_INVALIDE => "Doit correspondre au format h:m ou h.d (ex 5:15, 5h15, 5.25, 2)"],
            ED_FIELD_WIDTH          => 6
        ],
        "started"    => [ED_TYPE => ED_TYPE_HIDDEN],
        "time"       => [ED_TYPE => ED_TYPE_HIDDEN],
        "comment"    => [
            ED_LABEL       => "Commentaire",
            ED_TYPE        => ED_TYPE_ALPHA,
            ED_VALIDATE,
            ED_FIELD_WIDTH => 12
        ]
    ];

    if ($isToday || $row && !($row["ri"] == $firstLast[0][0] || $row["ri"] == $firstLast[sizeof($firstLast) - 1][0])) {
      unset($fields["!timeFrom"]);
      unset($fields["!timeTo"]);
      unset($fields["started"]);
      unset($fields["time"]);
    }

    msgBox(BuildForm::getForm($fields, $row, "timesheetForm"), "Edition tâche", null, [
        MSGBOX_BUTTON_ACTION => "Enregistrer",
        MSGBOX_BUTTON_CLOSE  => "annuler",
        MSGBOX_MODAL_ATTR    => [MODAL_NO_ESC_KEY => true, MODAL_SCROLLABLE => true],
    ]);
  }

  static function getTaskSelect()
  {
    $res = dbUtil()->selectRow("tasks", "ri,name",
                               "project=" . $_REQUEST["project"] . " and name<>'' and (role=0 or '" . utils()->userSession()["roles"] . "' like concat('%-',tsk.role,'-%'))");
    $n = mysqli_num_rows($res);
    mdbCompos()->buildSelect([
        ED_TYPE        => ED_TYPE_SELECT,
        ED_NAME        => "task",
        ED_LABEL       => "Tâche",
        ED_PLACEHOLDER => '',
        ED_OPTIONS     => mdbCompos()->getOptions($n ? $res : [null, "Pas de tâche trouvée"], $n ? null : "class='font-italic text-dark' disabled"),
        ED_VALIDATE    => [ED_VALIDATE_REQUIRED => true],
        ED_FIELD_WIDTH => 12
    ]);
  }

}
