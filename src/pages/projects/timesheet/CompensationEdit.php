<?php

class CompensationEdit
{

  public function __construct()
  {
    $row = dbUtil()->getCurrentEditedRow();
    $fields = [
        "!type"    => [
            ED_TYPE        => ED_TYPE_RADIO,
            ED_OPTIONS     => [
                [ED_LABEL => "Retrait compteur", ED_VALUE => 1, ED_CHECKED => !$row || $row["time"] < 0],
                [ED_LABEL => "Ajout compteur", ED_VALUE => 1, ED_CHECKED => $row["time"] > 0]
            ],
            ED_FIELD_WIDTH => 6,
        ],
        "comment" => [
            ED_TYPE        => ED_TYPE_ALPHA,
            ED_LABEL       => "Description",
            ED_VALIDATE    => [ED_VALIDATE_REQUIRED => true, ED_VALIDATE_MAX_LENGTH => 50],
            ED_FIELD_WIDTH => 12,
        ],
        "time"    => [
            ED_LABEL                => "Heures compensées",
            ED_TYPE                 => ED_TYPE_ALPHA,
            ED_TEXT_SELECT_ON_FOCUS => true,
            ED_ATTR                 => 'autocomplete=off pattern=\d{1,2}((:|h|\.)\d{1,2})?$',
            ED_VALUE                => utils()->translateToEdTime(abs($row["time"])),
            ED_VALIDATE             => [ED_VALIDATE_INVALIDE => "Invalide format h:m ou h.d (ex 5:15, 5h15, 5.25, 2)"],
            ED_FIELD_WIDTH          => 12
        ]
    ];

    msgBox(BuildForm::getForm($fields, $row, 'compensationForm'), "Compensation", null, [MSGBOX_BUTTON_ACTION => 'enregistrer', MSGBOX_BUTTON_CLOSE => 'annuler']);
  }

}
