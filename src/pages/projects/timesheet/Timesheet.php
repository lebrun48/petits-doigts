<?php

class Timesheet extends BuildTable
{

  private $worker;
  private $isToday;
  private $detailed;
  private $activeDay;
  private $working;

  public function __construct()
  {
    $cols = [
        "ri"         => [COL_HIDDEN => true, COL_PRIMARY => true],
        "started"    => [COL_HIDDEN => true],
        "running"    => [COL_HIDDEN => true],
        "delete"     => [
            COL_NO_DB      => true,
            COL_TD_ATTR    => "class=align-middle style=width:2em;max-width:2em",
            COL_TD_ACTIONS => true,
//            COL_VALUE   => "<a class='td-no-action icon-action p-2 icon-action' data-action=delete><i class='fa-lg far fa-trash-alt'></i></a>"
        ],
        "taskName"   => [COL_EXT_REF => ["tasks", "tsk.ri", "task"], COL_DB => "tsk.name"],
        "taskRef"    => [COL_HIDDEN => true, COL_DB => "tsk.ri"],
        "prjName"    => [COL_HIDDEN => true, COL_EXT_REF => ["projects", "prj.ri", "tsk.project"], COL_DB => "prj.name"],
        "day"        => [COL_HIDDEN => true],
        "time"       => [COL_TD_ATTR => "class=align-middle"],
        "start_stop" => [COL_NO_DB => true, COL_TD_ATTR => "class=align-middle"]
    ];
    !utils()->hasUserRole(ROLE_ADMIN) && !utils()->getPermanentValue("worker") && $_REQUEST["worker"] = utils()->userSession()["ri"];
    $this->worker = $_REQUEST["worker"] ?? utils()->userSession()["ri"];
    !utils()->getPermanentValue("activeDay", "timesheet") && ($_REQUEST["activeDay"] = utils()->now()->format("Ymd")) || $_REQUEST["activeDay"] = dbUtil()->getDbCnx()->real_escape_string($_REQUEST["activeDay"]);
    $this->activeDay = $_REQUEST["activeDay"];
    $this->isToday = $this->activeDay == utils()->now()->format("Ymd");
    dbUtil()->tables["timesheet"][DB_DEFAULT_WHERE] = "tsh.worker=$this->worker and tsh.day=$this->activeDay";
    dbUtil()->tables["timesheet"][DB_DEFAULT_INSERT] = "worker=$this->worker";
    dbUtil()->tables["timesheet"][DB_RIGHTS_ACCES] = TABLE_RIGHT_DELETE | TABLE_RIGHT_EDIT | TABLE_RIGHT_INSERT;
    dbUtil()->tables["timesheet"][DB_COLS_DEFINITION] = $cols;
    parent::__construct("timesheet");
    $this->detailed = $_REQUEST["detailed"];
    $this->setDefaultSort($this->detailed ? "started asc" : "task asc");
    $this->defaultWhere = $this->detailed ? "tsh.started<>0" : 'tsh.task>0';
    $this->settings[TABLE_ATTR_NO_HEADER] = true;
  }

  protected function buildBeforeTable()
  {
    //counter with calendar
//    //counter total hours
//    $dayTest = "day<" . ($this->isToday ? '' : '=') . $this->activeDay;
//    $counter = dbUtil()->result(dbUtil()->selectRow("timesheet", "sum(time)"
//                    , "day>='" . ($year = substr($this->activeDay, 0, 4)) . "0101' and $dayTest and task<>0 and worker=$this->worker", false), 0);
//    $sql = "select sum(partial) from calendar where year=$year and $dayTest and worker=0"
//            . " union select sum(partial) from calendar where year=$year and $dayTest"
//            . " and day not in (select day from calendar where year=$year and $dayTest and worker=0) and worker=$this->worker";
//    $leaveTime = array_sum(array_column(dbUtil()->fetch_all(dbUtil()->query($sql)), 0));
//    $fullTime = (DateTime::createFromFormat("Ymd", $this->activeDay)->diff(DateTime::createFromFormat("Ymd", "{$year}0101"))->days + !$this->isToday - $leaveTime) *
//            ConfigProject::get()->getWorkTime(CONFIG_WORKTIME_DAYTIME);
//counter with monthly hours

    $month = substr($this->activeDay, 0, 6);
    $dayTest = "day<" . ($this->isToday ? '' : '=') . $this->activeDay;
    $counter = dbUtil()->result(dbUtil()->selectRow("timesheet", "sum(time)"
                    , "day>='" . ($year = substr($this->activeDay, 0, 4)) . "0101' and $dayTest and task<>0 and worker=$this->worker", false), 0);
    $fullTime = dbUtil()->result(dbUtil()->selectRow("monthlyHours", "sum(hours)*60", "worker=$this->worker and month<$month and month>=" . substr($month, 0, 4) . "01"), 0);
    $fullMonth = dbUtil()->result(dbUtil()->selectRow("monthlyHours", "sum(hours)*60", "worker=$this->worker and month=$month"), 0);

    echo ""
    . "<div class='row align-items-center'>"
    . "  <div class=col>"
    . "    <h5 class=h5>Compteur horaire: <span" . (($t = $counter - $fullTime) < 0 ? " class=text-danger" : '') . ">" . utils()->translateToEdTime($t) . "</span></h5>"
    . "  </div>"
    . "  <div class='col" . (utils()->hasUserRole(ROLE_ADMIN) ? " d-none d-lg-flex'>" : "'>" )
    . "    <h5 class=h5>" . (($fullMonth - $t) < 0 ? "Heure sup: " : "Reste: ") . "<span>" . utils()->translateToEdTime(abs($fullMonth - $t)) . "</span></h5>"
    . "  </div>"
    . "  " . (utils()->hasUserRole(ROLE_ADMIN) ? "<div class=col>"
            . "  " . BuildForm::getForm(["!worker" => [
                    ED_LABEL   => "Collaborateur",
                    ED_TYPE    => ED_TYPE_SELECT,
                    ED_VALUE   => $this->worker,
                    ED_OPTIONS => mdbCompos()->getOptions(dbUtil()->selectRow("users", "ri, concat(firstName, ' ', upper(name))")),
                    ED_ATTR    => "onchange=pageAction('timesheet',{page:'projects'})"
                ]], null, ED_NO_FORM)
            . "  </div>" : '')
    . "</div>";

    //Add task and select day
    date_diff(DateTime::createFromFormat("Ymd", $this->activeDay), utils()->now)->days >= 30 &&
            $hide = " style=visibility:hidden";
    echo ""
    . "<div class = 'd-flex justify-content-between align-items-center z-depth-1 text-white " . SITE_DEFAULT_COLOR . " stickyBefore'>"
    . "  <span style=width:120px>"
    . "    <a onclick=pageAction('timesheet',{xAction:'edit',action:'insert'}) class='btn-floating btn-sm btn-white'>"
    . "      <i class='fas fa-plus text-dark' title='Ajouter tâche'></i>"
    . "    </a>"
    . "    <a onclick=pageAction('timesheet',{xAction:'edit',action:'compensation'}) class='btn-floating btn-sm btn-white" . (utils()->hasUserRole(ROLE_ADMIN) && $this->isToday ? "'>" : " d-none'>")
    . "      <i class='fas fa-euro-sign text-dark' title='Ajouter compensation'></i>"
    . "    </a>"
    . "  </span>"
    . "  <a title='Jour précédent'$hide class='icon-action' onclick=pageAction('timesheet',{action:'prevDay'})><i class='fas fa-arrow-left fa-lg'></i></a>";
    $this->buildActiveDaySelect();
    echo ""
    . "  <a title='Jour suivant'" . ($this->activeDay == utils()->now()->format("Ymd") ? " style=visibility:hidden" : '') . " class='icon-action' onclick=pageAction('timesheet',{action:'nextDay'})>"
    . "    <i class='fas fa-arrow-right fa-lg'></i>"
    . "  </a>"
    . "</div>";
  }

  private function buildActiveDaySelect()
  {
    $ar[utils()->now()->format("Ymd")] = "Aujourd'hui";
    $today = clone utils()->now();
    for ($i = 1; $i < 31; $i++) {
      $day = $today->sub(new DateInterval("P1D"));
      $ar[$day->format("Ymd")] = utils()->formatDate($day, "D j M");
    }
    mdbCompos()->buildSelect([ED_NAME => "!activeDay", ED_VALUE => $this->activeDay, ED_OPTIONS => mdbCompos()->getOptions($ar)]);
  }

  protected function DBupdate()
  {
    utils()->action && $row = dbUtil()->getCurrentEditedRow();
    switch (utils()->action) {
      case "stop":
        if ($this->isToday) {
          dbUtil()->begin_transaction();
          //start pause
          $time = ($started = utils()->translateFromEdTime(utils()->now()->format("H:i"))) - $row["started"];
          dbUtil()->insertRow("timesheet", ["time" => 0, "running" => 1, "started" => $started, "task" => 0, "day" => $this->activeDay]);
          $time &&
                  //stop current
                  dbUtil()->updateRow("timesheet", ["running" => '', "time" => $time], "ri=" . $_REQUEST["primary"][0], false) ||
                  //delete if time=0
                  dbUtil()->deleteRow();
          dbUtil()->commit();
        }
        return;

      case "start":
        if ($this->isToday) {
          $started = utils()->translateFromEdTime(utils()->now()->format("H:i"));
          dbUtil()->begin_transaction();
          //stop running
          $tup = dbUtil()->fetch_row(dbUtil()->selectRow("timesheet", "started, ri", "running=1"));
          $tup && (($time = $started - $tup[0]) &&
                  dbUtil()->updateRow("timesheet", ["running" => '', "time" => $time], "ri=$tup[1]", null) ||
                  dbUtil()->deleteRow("timesheet", "ri=$tup[1]"));
          //start current
          dbUtil()->query("insert into timesheet (time, started, task, running, day, worker) "
                  . "select 0, $started, task, 1, $this->activeDay, $this->worker from timesheet where ri=" . $_REQUEST["primary"][0]);
          dbUtil()->commit();
        }
        return;

      case "delete":
        if (!$this->detailed) {
          dbUtil()->deleteRow("timesheet", "task=" . $row["task"]);
          return;
        }
        break;

      case "compensation":
        dbUtil()->insertRow('timesheet', [
            'time' => ($_REQUEST['type'] ? '-' : '') . utils()->translateFromEdTime($_REQUEST['val_time']),
            'started' => 1, 'task' => -1,
            'comment' => 'val_comment']);
        return;
    }

    $_REQUEST["val_day"] = $this->activeDay;
    !$_REQUEST["val_time"] && ($_REQUEST["val_time"] = 0 || true) || $_REQUEST["val_running"] = "";
    try {
      parent::DBupdate();
    } catch (Exception $exc) {
      if ($exc->getCode() == E_DB_DUPLICATE) {
        msgBox("Cette tâche est déjà dans la journée", "Tâche existe", MODAL_SIZE_SMALL, [MSGBOX_MODAL_ATTR => [MODAL_CENTERED => true, MODAL_NO_BACKDROP => true]]);
        exit();
      }
      throw $exc;
    }
  }

  protected function buildLine($row)
  {
    if ($this->detailed) {
      //all rows
      parent::buildLine($row);
      return;
    }

    //group by task
    if (!isset($this->taskRow)) {
      $this->taskRow = $row;
      return;
    }
    if ($row["taskRef"] == $this->taskRow["taskRef"]) {
      $time = $this->taskRow["time"] + $row["time"];
      $row["running"] && $this->taskRow = $row;
      $this->taskRow["time"] = $time;
    }
    else {
      parent::buildLine($this->taskRow);
      $this->taskRow = $row;
    }
  }

  protected function buildLastLine()
  {
    $this->taskRow && parent::buildLine($this->taskRow);

    if (!$this->countRows) {
      return;
    }
    //Total day
    $workTime = ConfigProject::get()->getWorkTime();
    //tup[0]=workTime, tup[1]=current work
    $tup = dbUtil()->fetch_row(dbUtil()->selectRow("timesheet", "sum(time), sum(if(running=1, started, 0))", "day=$this->activeDay and task>0"));
    $time = $tup[0] + ($tup[1] && $this->isToday ? utils()->translateFromEdTime(utils()->now()->format("H:i")) - $tup[1] : 0);

    echo ""
    . "<tr style=z-index:2 class='text-white " . SITE_DEFAULT_COLOR . ($this->countRows > 3 ? " stickyAfter'>" : "'>")
    . " <td colspan=2 style=font-size:1.3em" . ($this->working ? " id=tshTotalTime data-work=$tup[0]>" : '>')
    . "   <span class=font-weight-bold>Total journée: </span><span>" . utils()->translateToEdTime($time) . "</span>"
    . " </td>"
    . " <td colspan=2>"
    . " </td>"
    . "</tr>";
  }

  protected function getTrAttributes($row = null)
  {
    return $row && !$row["taskRef"] ? "data-pause = 1" : parent::getTrAttributes($row);
  }

  protected function getRightsOnLine($row, $currentRights)
  {
    return $row["taskRef"] == 0 ? $currentRights &= ~TABLE_RIGHT_EDIT : $currentRights;
  }

  protected function getDisplayValue($key, $row)
  {
    switch ($key) {
      case "taskName":
        return ""
                . "<div class=timesheet-dv>" . $row["prjName"] . "</div>"
                . "<div class=ml-2>" . $row["taskName"] . "</div>";

      case "time":
        if ($this->detailed && $row["started"]) {
          $fromTo = "<span class=ml-3>De " . utils()->translateToEdTime($row["started"]) . " à ";
          $fromTo .= $row["running"] ? "..." : utils()->translateToEdTime($row["started"] + $row["time"]) . "</span>";
        }
        $time = $row["time"];
        $this->isToday && $row["running"] && $time += utils()->translateFromEdTime(utils()->now()->format("H:i")) - $row["started"];
        return ""
                . "<span" . (($s = $row["running"]) ? " data-started=[" . $row["started"] . "," . $row["time"] . "]" : '') . " style=font-size:1.3em;font-weight:bold>"
                . utils()->translateToEdTime($time)
                . "</span>$fromTo";

      case "start_stop":
        if ($this->isToday && !$this->detailed) {
          $this->working |= $row["running"];
          return $row["running"] ?
                  getButton([
                      BUTTON_ATTR      => "class='td-no-action btn-sm' onclick=pageAction('timesheet',{xAction:'submit',action:'stop',primary:[" . $row["ri"] . "]})",
                      BUTTON_ICON_ONLY => "<i class='fas fa-stop'>",
                      BUTTON_COLOR     => "btn-danger",
                  ]) :
                  getButton([
                      BUTTON_ATTR      => "class='td-no-action btn-sm' onclick=pageAction('timesheet',{xAction:'submit',action:'start',primary:[" . $row["ri"] . "]})",
                      BUTTON_COLOR     => "btn-light-green",
                      BUTTON_ICON_ONLY => "<i class='fas fa-play'>",
          ]);
        }
    }
  }

}
