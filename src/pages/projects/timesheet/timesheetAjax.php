<?php

function axTimesheetSubmit()
{
  switch (utils()->action) {
    case "prevDay";
      $_REQUEST["activeDay"] = DateTime::createFromFormat("Ymd", $_REQUEST["activeDay"])->sub(new DateInterval("P1D"))->format("Ymd");
      break;

    case "nextDay";
      $_REQUEST["activeDay"] = DateTime::createFromFormat("Ymd", $_REQUEST["activeDay"])->add(new DateInterval("P1D"))->format("Ymd");
      break;
  }
  utils()->axRefreshElement();
}

function axTimesheetEdit()
{
  switch ($_REQUEST["action"]) {
    case "update":
    case "insert":
      new TimesheetEdit();
      return;

    case "delete":
      if (!($row=dbUtil()->getCurrentEditedRow())["running"] && !$row["time"]) {
        utils()->xAction = "submit";
        axTimesheetSubmit();
        break;
      }
      msgBox("<p>Le temps pour cette tâche sera supprimé également!</p><p>Confirmation suppression.</p>"
              , "Confirmation", MODAL_SIZE_SMALL
              , [MSGBOX_BUTTON_ACTION => "Supprimer", MSGBOX_BUTTON_CLOSE => "annuler", MSGBOX_MODAL_ATTR => [MODAL_CENTERED => true]]);
      return;
      
    case "compensation":
      new CompensationEdit();
      return;

    case "getTaskSelect":
      TimesheetEdit::getTaskSelect();
      return;
  }
}
