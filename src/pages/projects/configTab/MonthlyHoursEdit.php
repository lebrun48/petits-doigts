<?php

class MonthlyHoursEdit
{

  public function __construct()
  {
    ($row = dbUtil()->getCurrentEditedRow()) && ($row["year"] = substr($row["month"], 0, 4)) && $row["month"] = substr($row["month"], -2);
    $fields = [
        "!year"  => [
            ED_LABEL       => "Année",
            ED_DISABLED    => !!$row,
            ED_TYPE        => ED_TYPE_SELECT,
            ED_OPTIONS     => mdbCompos()->getOptions([$year = utils()->now()->format("Y") => $year, $year + 1 => $year + 1]),
            ED_FIELD_WIDTH => 6
        ],
        "month"  => [
            ED_LABEL       => "Mois",
            ED_DISABLED    => !!$row,
            ED_TYPE        => ED_TYPE_SELECT,
            ED_OPTIONS     => mdbCompos()->getOptions(utils()->ucfMonths),
            ED_FIELD_WIDTH => 6
        ],
        "worker" => [
            ED_TYPE        => ED_TYPE_SELECT,
            ED_DISABLED    => !!$row,
            ED_PLACEHOLDER => '',
            ED_OPTIONS     => mdbCompos()->getOptions(dbUtil()->selectRow("users", "ri, concat(firstName, ' ', upper(name))")),
            ED_VALIDATE    => [ED_VALIDATE_REQUIRED => true],
            ED_LABEL       => "Collaborateur",
            ED_FIELD_WIDTH => 6
        ],
        "hours"  => [
            ED_LABEL                => "Heures pour le mois",
            ED_TEXT_SELECT_ON_FOCUS => true,
            ED_TYPE                 => ED_TYPE_INTEGER,
            ED_VALIDATE             => [ED_VALIDATE_REQUIRED => true, ED_VALIDATE_MIN_VALUE => 0, ED_VALIDATE_MAX_VALUE => 240, ED_VALIDATE_INVALIDE => "Entre 0 et 240"],
            ED_NO_STEPPER           => true,
            ED_NO_AUTOCOMPLETE      => true,
            ED_FIELD_WIDTH          => 6
        ]
    ];

    if (utils()->action == "duplicate") {
      unset($fields["worker"]);
      unset($fields["hours"]);
      $fields["!year"][ED_DISABLED] = $fields["month"][ED_DISABLED] = null;
    }

    msgBox(BuildForm::getForm($fields, $row)
            , utils()->action == "duplicate" ? "Dupliquer mois" : "Edition heures"
            , null
            , [MSGBOX_BUTTON_ACTION => "enregistrer", MSGBOX_BUTTON_CLOSE => "annuler", MSGBOX_MODAL_ATTR => [MODAL_NO_BACKDROP => true, MODAL_CENTERED => true]]);
  }

}
