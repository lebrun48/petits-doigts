<?php

function axMonthlyHoursSubmit()
{
  utils()->axRefreshElement("monthlyHours", false);
}

function axMonthlyHoursEdit()
{
  switch (utils()->action) {
    case "update":
    case "duplicate":
    case "insert":
      new MonthlyHoursEdit();
      return;

    case "delete":
      msgBox("Confirmation suppression", null, MODAL_SIZE_SMALL
              , [MSGBOX_BUTTON_ACTION => "Confirmer", MSGBOX_BUTTON_CLOSE => "annuler", MSGBOX_MODAL_ATTR => [MODAL_ID => "msgbox", MODAL_NO_BACKDROP => true, MODAL_CENTERED => true]]);
      return;
  }
}

function buildMonthlyHoursBox()
{
  echo ""
  . "<div id=monthlyHoursBox>";
  new MonthlyHours();
  echo ""
  . "</div>";
  utils()->insertReadyFunction("tableListener", 'monthlyHours');
}
