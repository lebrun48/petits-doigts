<?php

class MonthlyHours extends BuildTable
{

  public function __construct()
  {
    $fields = [
        "ri"         => [COL_HIDDEN => true, COL_PRIMARY => true],
        "dummy"      => [COL_TD_ACTIONS => true, COL_NO_DB => true],
        "month"      => [
            COL_GROUP                 => 0,
            COL_TD_ATTR               => "colspan=3 class=group-col",
            COL_TD_ACTIONS            => true,
            COL_GROUP_ORDER_DIRECTION => "desc"
        ],
        "workerName" => [
            COL_TITLE   => "Collaborateur",
            COL_EXT_REF => ["users", "ri", "worker"],
            COL_DB      => "concat(usr.firstName,' ', upper(usr.name))"],
        "hours"      => [COL_TITLE => "Heures du mois"]
    ];
    dbUtil()->tables["monthlyHours"][DB_COLS_DEFINITION] = $fields;
    parent::__construct("monthlyHours");
    $this->buildFullTable();
    utils()->axExecuteJS("msgBoxClose", "msgbox");
    utils()->axExecuteJS("tableListener", "monthlyHours");
  }

  protected function DBupdate()
  {
    $_REQUEST["val_month"] && $_REQUEST["val_month"] = sprintf("%d%02d", $_REQUEST["year"], $_REQUEST["val_month"]);
    if (utils()->action == "duplicate") {
      try {
        dbUtil()->query("insert into monthly_hours (worker,month,hours) "
                . "select worker," . $_REQUEST["val_month"] . ",hours from monthly_hours where month=" . dbUtil()->getCurrentEditedRow()["month"]);
      } catch (Exception $exc) {
        if ($exc->getCode() == E_DB_DUPLICATE) {
          msgBox("Ce mois est déjà présent", null, MODAL_SIZE_SMALL, [MSGBOX_MODAL_ATTR => [MODAL_NO_BACKDROP => true]]);
          exit();
        }
        throw new $exc;
      }
    }
    parent::DBupdate();
  }

  protected function buildBeforeTable()
  {
    $this->settings[TABLE_ATTR_BEFORE_TABLE_ATTR] = "class=pb-3";
    parent::buildBeforeTable();
    echo "<input type=hidden name=active value=configTab>";
  }

  protected function getRightsOnLine($row, $currentRights)
  {
    return $this->isGroupLine ? TABLE_RIGHT_USER_ACTION : $currentRights;
  }

  protected function getUserAction($row)
  {
    return "<span class=icon-action title='Dupliquer mois' data-action=duplicate><i class='fas fa-clone fa-sm'></i></span>";
  }

  protected function getDisplayValue($key, $row)
  {
    switch ($key) {
      case "month":
        return utils()->ucfMonths[intval(substr($row[$key], -2))] . " " . substr($row[$key], 0, 4);
    }
    return parent::getDisplayValue($key, $row);
  }

}
