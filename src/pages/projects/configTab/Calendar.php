<?php

class Calendar
{

  private $worker;
  private $partial;

  public function __construct()
  {
    !isset($_REQUEST["worker"]) && $_REQUEST["worker"] = utils()->userSession()["ri"];
    ($this->worker = $_REQUEST["worker"]) &&
            ($this->partial = dbUtil()->fetch_row(dbUtil()->selectRow("users", "partialDate, partialTime", "ri=$this->worker"))) &&
            $this->partial[1] = json_decode($this->partial[1], JSON_OBJECT_AS_ARRAY);
    if (utils()->action == "leave") {
      $partial = 1;
      ($_REQUEST["leave"] == ConfigProject::WORK && (($dw = DateTime::createFromFormat("Ymd", $_REQUEST["day"])->format('N')) >= 6 && ($_REQUEST["leave"] = ConfigProject::WE) ||
              $this->partial[1][$dw] && $_REQUEST["day"] >= $this->partial[0] && ($partial = $this->partial[1][$dw]) && ($_REQUEST["leave"] = ConfigProject::PARTIAL)) ||
              $_REQUEST["leave"] != ConfigProject::WORK) && dbUtil()->insertRow("calendar", [
                  "day"     => $_REQUEST["day"],
                  "weekDay" => DateTime::createFromFormat("Ymd", $_REQUEST["day"])->format('N'),
                  "year"    => substr($_REQUEST["day"], 0, 4),
                  "partial" => $partial,
                  "reason"  => $_REQUEST["leave"],
                  "worker"  => $this->worker], "reason=values(reason)") ||
              dbUtil()->deleteRow("calendar", "day=" . $_REQUEST["day"] . " and worker=$this->worker");
    }
    echo "<form id=calendarFormTable>";
    $this->buildBeforeTable();
    echo "<table id=calendar class=mx-auto>";
    $this->buildBody();
    echo "</table></form>";

    echo ""
    . "<div class=dropdown-menu id=calendarLeave>";
    foreach (ConfigProject::get()->leave as $i => $leave) {
      if (!$this->worker && ($i == ConfigProject::SICK || $i == ConfigProject::PARTIAL || $i == ConfigProject::WE || $i == ConfigProject::EDUC) ||
              $this->worker && ($i == ConfigProject::LEAVE || $i == ConfigProject::WE || $i == ConfigProject::PARTIAL))
        continue;
      echo "<a data-leave=$i class=dropdown-item>$leave</a>";
    }
    echo "</div>";
  }

  function buildHeader()
  {
    echo "<tr>";
    $days = ["Lu.", "Ma.", "Me.", "Je.", "Ve.", "Sa.", "Di."];
    for ($i = 0; $i < 7; $i++) {
      echo "<th>" . $days[$i] . "</th>";
    }
    echo "</tr>";
  }

  protected function buildBeforeTable()
  {
    if (utils()->hasUserRole(ROLE_ADMIN)) {
      $calendar = [
          "!worker" => [
              ED_VALUE       => $this->worker,
              ED_TYPE        => ED_TYPE_SELECT,
              ED_OPTIONS     => mdbCompos()->getOptions([0 => "(Commun)"]) + mdbCompos()->getOptions(dbUtil()->selectRow("users", "ri, concat(firstName, ' ', upper(name))")),
              ED_LABEL       => "Calendrier",
              ED_ATTR        => "onchange=pageAction('calendar',{page:'configTab',action:'refresh'})",
              ED_FIELD_WIDTH => 6
          ],
      ];
      echo BuildForm::getForm($calendar, null, ED_NO_FORM);
    }
  }

  protected function buildBody()
  {
    if ($toBuild = !mysqli_num_rows(dbUtil()->selectRow("calendar", "year", "year=" . ($year = utils()->now()->format("Y")) . " and worker=0 limit 1"))) {
      $firstSaterday = 7 - DateTime::createFromFormat("Ymd", sprintf("%d%02d%02d", $year, 1, 1))->format("N");
      ;
    }

    $leavesCommon = array_column(dbUtil()->fetch_all(dbUtil()->selectRow("calendar", "day,reason", "year=$year and worker=0")), 1, 0);
    $leavesUser = array_column(dbUtil()->fetch_all(dbUtil()->selectRow("calendar", "day,reason", "year=$year and worker=$this->worker")), 1, 0);
    $dayYear = 1;
    for ($month = 1; $month <= 12; $month++) {
      $daysNb = cal_days_in_month(CAL_GREGORIAN, $month, $year);
      $firstDayWeek = DateTime::createFromFormat("Ymd", sprintf("%d%02d%02d", $year, $month, 1))->format("N");
      echo "<tr><td colspan=2 class=month>" . ucfirst(utils()->months[$month]) . "</td></tr>";
      $this->buildHeader();
      $day = 1;
      while ($day <= $daysNb) {
        echo "<tr>";
        for ($weekDay = 1; $weekDay <= 7; $weekDay++) {
          if ($weekDay < $firstDayWeek) {
            echo "<td></td>";
            continue;
          }
          $firstDayWeek = 0;
          if ($day <= $daysNb) {
            $d = sprintf("%d%02d%02d", $year, $month, $day);
            $toBuild && ($we = ($weekDay = ($dayYear - $firstSaterday) % 7) <= 1) &&
                    ($insert .= ", ($d," . ($weekDay + 6) . ",$year,1," . ConfigProject::WE . ",0)");
            ($we && ($leave = ConfigProject::get()->leave[ConfigProject::WE]) ||
                    ($leaveCommon = isset($leavesCommon[$d])) && ($leave = ConfigProject::get()->leave[$leavesCommon[$d]])) && ($attr = " class=leave-common title=$leave") ||
                    ($leaveUser = isset($leavesUser[$d])) && ($leave = ConfigProject::get()->leave[$leavesUser[$d]]) && $attr = " class=leave-user title=$leave";

            utils()->hasUserRole(ROLE_ADMIN) && !($this->worker && $leaveCommon || !$this->worker && $leaveUser) && $attr = addAttribute($attr, "icon-action");
            $d = "<div data-day=$d class=btn-group><span $attr data-toggle=dropdown>$day</span></div>";
            $dayYear++;
          }
          else
            $d = '';
          echo "<td><span class=no-action>$d</span></td>";
          $day++;
          $attr = $leaveCommon = $leaveUser = null;
        }
        echo "</tr>";
      }
    }
    $insert && dbUtil()->query("insert into calendar (day,weekDay,year,partial,reason,worker) values " . substr($insert, 2));
  }

}
