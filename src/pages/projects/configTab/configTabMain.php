<?php

class configTabMain
{

  public function __construct()
  {
    switch (utils()->getPermanentValue("active", "configTab")) {
      case "calendar":
        if (!utils()->action) {
          obStart();
          buildCalendarBox();
          MsgBox::buildModal([MODAL_SIZE => MODAL_SIZE_LG, MODAL_BODY => ob_get_clean(), MODAL_TITLE => "Calendrier", MODAL_ID => "modalConfigTab"]);
          utils()->axExecuteJS("msgBoxShow", "data");
          return;
        }
        utils()->axExecuteJS("msgBoxClose", "msgbox");
        utils()->axRefreshElement("calendar", false);
        return;

      case "monthlyHours":
        if (!utils()->action) {
          include "monthlyHoursAjax.php";
          obStart();
          buildMonthlyHoursBox();
          MsgBox::buildModal([MODAL_SIZE => MODAL_SIZE_LG, MODAL_BODY => ob_get_clean(), MODAL_TITLE => "Heures mensuelles", MODAL_ID => "modalConfigTab", MODAL_SCROLLABLE=>true]);
          utils()->axExecuteJS("msgBoxShow", "data");
          return;
        }
        utils()->axExecuteJS("msgBoxClose", "msgbox");
        utils()->axRefreshElement("monthlyHours", false);
        return;
        
    }
  }

}

function axConfigTabSubmit()
{
  new configTabMain();
}

function buildCalendarBox()
{
  echo ""
  . "<div id=calendarBox>";
  new Calendar();
  echo ""
  . "</div>";
  utils()->insertReadyFunction("configTabCalendarListener");
}
