<?php

class projectsMain
{

  function __construct()
  {
    $_REQUEST["page"] != "projects" && ($_REQUEST["page"] = "projects") && $_REQUEST["active"] = "timesheet";
    !utils()->getPermanentValue("active") && $_REQUEST["active"] = "timesheet";
    $curPage = $_REQUEST["active"];
    echo ""
    . "<main id=main>"
    . "  <form id=mainFormAction class='d-flex justify-content-between align-items-center'>"
    . "    <div class='d-flex align-items-center'" . (!utils()->hasUserRole(ROLE_ADMIN) ? " style=visibility:hidden>" : '>')
    . "      <div class=pb-2>"
    . "        <ul class='container pt-3 text-center nav nav-tabs'>"
    . "          <li class='nav-item'>"
    . "            <a class='nav-link" . ($curPage == "tasks" ? " active'" : "'") . " onclick=pageAction('projects',{active:'tasks'}) data-toggle='tab'>"
    . "              Tâches"
    . "            </a>"
    . "          </li>"
    . "          <li class=' nav-item'>"
    . "            <a class='nav-link" . ($curPage == "timesheet" ? " active'" : "'") . " onclick=pageAction('projects',{active:'timesheet'}) data-toggle='tab'>"
    . "              Timesheet"
    . "            </a>"
    . "          </li>"
    . "          <li class=' nav-item'>"
    . "            <a class='nav-link" . ($curPage == "stats" ? " active'" : "'") . " onclick=pageAction('projects',{active:'stats'}) data-toggle='tab'>"
    . "              Statistiques"
    . "            </a>"
    . "          </li>"
    . "        </ul>"
    . "      </div>"
    . "      <div class='row" . ($_REQUEST["page"] == "configTabFinances" ? " d-none" : '') . "'>"
    . "        <div class='col'>"
    . "          " . $this->getOptions($curPage)
    . "        </div>"
    . "      </div>"
    . "    </div>";

    $this->buildLocalMenu();
    echo ""
    . "  </form>"
    . "  <div class='tab-content pt-3 px-0'>"
    . "    <div class='tab-pane fade show active' id=mainTab>";
    switch ($curPage) {
      case "tasks":
        buildTasksBox();
        break;

      case "timesheet":
        buildTimesheetBox();
        break;

      case "stats":
        buildStatsBox();
        break;
    }
    echo ""
    . "    </div>"
    . "  </div>"
    . "</main>";
    utils()->insertReadyFunction("optionsDropdownListener");
  }

  function buildLocalMenu()
  {
    if ($_REQUEST["active"] != "timesheet")
      return;
    echo ""
    . "<div class='btn-group dropleft' data-page=timesheet>"
    . "  <i class='fa-lg fas fa-cog' data-toggle='dropdown'></i>"
    . "  <div class='dropdown-menu dropdown-dark'>"
    . "    <a class='dropdown-item options'>"
    . "      <input type = hidden name=detailed value='" . $_REQUEST["detailed"] . "'>En séquence<i class='fas fa-check fa-lg ml-3' style=visibility:hidden></i>"
    . "    </a>";
//    . "    <a onclick=axExecute({page:'configTab',active:'calendar',xAction:'submit'}) class='dropdown-item'>"
//    . "      Calendrier"
//    . "    </a>"
    if (utils()->hasUserRole(ROLE_ADMIN)) {
      echo ""
      . "    <a onclick=axExecute({page:'configTab',active:'monthlyHours',xAction:'submit'}) class='dropdown-item'>"
      . "      Heures mensuelles"
      . "    </a>";
    }
    echo""
    . "  </div>"
    . "</div>";
  }

  private function getOptions($curPage)
  {
    
  }

}

function axProjectsSubmit()
{
  utils()->axRefreshMain();
}

function buildTasksBox()
{
  $obj = new Tasks();
  $obj->executeAction();
  echo ""
  . "<div id=tasksBox>";
  $obj->buildFullTable();
  echo ""
  . "</div>";
  utils()->insertReadyFunction("tableListener", "tasks");
}

function buildTimesheetBox()
{
  $obj = new Timesheet();
  $obj->executeAction();
  echo ""
  . "<div id=timesheetBox>";
  $obj->buildFullTable();
  echo ""
  . "</div>";
  utils()->insertReadyFunction("timesheetListener");
}

function buildStatsBox()
{
  echo "<h2 calss=h2>En construction...</h2>";
}
