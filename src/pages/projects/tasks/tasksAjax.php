<?php

function axTasksEdit()
{
  if (utils()->action == "deleteGroup" || utils()->action == "delete" || utils()->action == "deleteRole") {
    utils()->xAction = "submit";
    utils()->axRefreshElement();
    return;
  }
  new TaskEdit();
}

function axTasksSubmit()
{
  utils()->axRefreshElement();
}
