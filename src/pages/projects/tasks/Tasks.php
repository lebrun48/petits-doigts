<?php

class Tasks extends BuildTable
{

  private $currentRole = -1;
  private $currentTask;

  public function __construct()
  {
    dbUtil()->tables["tasks"][DB_COLS_DEFINITION] = [
        "ri"       => [COL_HIDDEN => true, COL_PRIMARY => true],
        "prjName"  => [
            COL_GROUP        => 0,
            COL_GROUP_ACTION => "updPrj",
            COL_TD_ATTR      => "colspan=3 class=group-col",
            COL_ORDER_BY     => "prjName,role,name",
            COL_EXT_REF      => ["projects", "prj.ri", "tsk.project"],
            COL_DB           => "prj.name",
            COL_TD_ACTIONS   => true,
        ],
        "project"  => [COL_HIDDEN => true],
        "role"     => [COL_HIDDEN => true],
        "name"     => [
            COL_TH_ATTR    => "style=width:50em;max-width:50em",
            COL_TD_ACTIONS => true
        ],
        "prjEnded" => [COL_TITLE => "Terminé", COL_DB => "prj.ended", COL_TD_ATTR => "class=text-bold"],
    ];
    $this->settings[TABLE_ATTR_BEFORE_TABLE_ATTR] = "class=pb-2";
    $this->defaultWhere = "prj.ri>0";
    !$_REQUEST["ended"] && $this->defaultWhere .= " and prj.ended is null";;
    parent::__construct("tasks");
  }

  protected function getCurrentEditedRow()
  {
    if ($_REQUEST["primary"]) {
      return dbUtil()->fetch_assoc(dbUtil()->query($this->getdBRequestAttr() . " " . $this->getDbFrom() . " where " . dbUtil()->getFromKeys(true)));
    }
  }

  protected function getUserAction($row)
  {
    if ($this->isGroupLine) {
      return ""
              . "<span class=icon-action title='Ajouter rôle' data-action=addNewRole><i class='fas fa-user-plus fa-sm'></i></span>"
              . "<span class=icon-action title='Dupliquer projet' data-action=duplicatePrj><i class='fas fa-clone fa-sm'></i></span>";
    }
    if (!$row["name"]) {
      return ""
              . "<span class=icon-action title='Ajouter nouvelle tâche' data-action=addNewTask><i class='fas fa-tasks fa-lg'></i></span>"
              . "<span class=icon-action title='Ajouter tâche existantes' data-action=addExistingTask><i class='fas fa-plus-circle fa-lg'></i></span>"
              . "<span class=icon-action title='Dupliquer rôle' data-action=duplicateRole><i class='fas fa-clone fa-lg'></i></span>";
    }
  }

  protected function DBupdate()
  {
    switch (utils()->action) {
      case "deleteGroup":
        $row = $this->getCurrentEditedRow();
        if (!dbUtil()->deleteRow("projects", "ri=" . $row["project"], true, DB_OPTION_NO_THROW)) {
          msgBox("<p>Au moins une tâche est utilisée pour le projet.</p><p>Suppression anulée.</p>", null, MODAL_SIZE_SMALL, [MSGBOX_MODAL_ATTR => [MODAL_NO_BACKDROP => true, MODAL_CENTERED => true]]);
          exit();
        }
        return;

      case "deleteRole":
        $row = $this->getCurrentEditedRow();
        if (!dbUtil()->deleteRow("tasks", "project=" . $row["project"] . " and role=" . $row["role"], true, DB_OPTION_NO_THROW)) {
          msgBox("<p>Au moins une tâche est utilisée pour ce rôle.</p><p>Suppression anulée.</p>", null, MODAL_SIZE_SMALL, [MSGBOX_MODAL_ATTR => [MODAL_NO_BACKDROP => true, MODAL_CENTERED => true]]);
          exit();
        }
        return;

      case "delete":
        if (!dbUtil()->deleteRow(null, null, true, DB_OPTION_NO_THROW)) {
          msgBox("<p>La tâche est utilisée dans une timesheet.</p><p>Suppression anulée.</p>", null, MODAL_SIZE_SMALL, [MSGBOX_MODAL_ATTR => [MODAL_NO_BACKDROP => true, MODAL_CENTERED => true]]);
          exit();
        }
        return;

      case "updPrj":
        $row = $this->getCurrentEditedRow();
        if (!dbUtil()->updateRow(
                        "projects",
                        ["name" => $_REQUEST["val_prjName"], "ended" => $_REQUEST["val_prjEnded"]],
                        "ri=" . $row["project"], true,
                        DB_OPTION_NO_THROW)) {
          msgBox("Le projet <b>«" . $_REQUEST["val_prjName"] . "»</b> existe déjà", null, MODAL_SIZE_SMALL, [MSGBOX_MODAL_ATTR => [MODAL_NO_BACKDROP => true, MODAL_CENTERED => true]]);
          exit();
        }
        break;

      case "duplicatePrj":
        $action = utils()->action;
      case "insert":
        !$action && $action = utils()->action;
        dbUtil()->begin_transaction();
        $row = $this->getCurrentEditedRow();
        if (!dbUtil()->insertRow(
                        "projects",
                        ["name" => $_REQUEST["val_prjName"], "ended" => $_REQUEST["val_prjEnded"]],
                        null,
                        DB_OPTION_NO_THROW)) {
          msgBox("Le projet «" . $_REQUEST["val_prjName"] . "» existe déjà", null, MODAL_SIZE_SMALL, [MSGBOX_MODAL_ATTR => [MODAL_NO_BACKDROP => true, MODAL_CENTERED => true]]);
          exit();
        }
        $ri = dbUtil()->getDbCnx()->insert_id;
        $task = dbUtil()->getTableNameNoAs("tasks");
        $action == "duplicatePrj" && dbUtil()->query("insert into $task (project, name, role) select $ri, name, role from $task where project=" . $row["project"]);
        $action == "insert" && dbUtil()->insertRow("tasks", ["project" => $ri]);
        break;

      case "updRole":
        $row = $this->getCurrentEditedRow();
        if (!dbUtil()->updateRow(
                        "tasks",
                        ["role" => $_REQUEST["val_role"]],
                        "project=" . $row["project"] . " and role=" . $row["role"], true,
                        DB_OPTION_NO_THROW)) {
          msgBox("Le rôle est déjà dans le projet", null, MODAL_SIZE_SMALL, [MSGBOX_MODAL_ATTR => [MODAL_NO_BACKDROP => true, MODAL_CENTERED => true]]);
          exit();
        }
        break;

      case "duplicateRole":
        $action = utils()->action;
      case "addNewRole":
        !$action && $action = utils()->action;
        dbUtil()->begin_transaction();
        $row = $this->getCurrentEditedRow();
        if (!dbUtil()->insertRow(
                        "tasks",
                        ["project" => $row["project"], "role" => $_REQUEST["val_role"]],
                        null,
                        DB_OPTION_NO_THROW)) {
          msgBox("Le rôle est déjà dans le projet", null, MODAL_SIZE_SMALL, [MSGBOX_MODAL_ATTR => [MODAL_NO_BACKDROP => true, MODAL_CENTERED => true]]);
          exit();
        }
        $ri = dbUtil()->getDbCnx()->insert_id;
        $task = dbUtil()->getTableNameNoAs("tasks");
        $action == "duplicateRole" && dbUtil()->query(""
                        . "insert into $task (project, name, role) "
                        . "select " . $row["project"] . ", name, " . $_REQUEST["val_role"] . " from $task "
                        . "where project=" . $row["project"] . " and role=" . $row["role"] . " and name<>''");
        break;

      case "updTask":
        $row = $this->getCurrentEditedRow();
        if (!dbUtil()->updateRow(
                        "tasks",
                        ["name" => $_REQUEST["val_name"]],
                        "ri=" . $_REQUEST["primary"][0], true,
                        DB_OPTION_NO_THROW)) {
          msgBox("La tâche <b>«" . $_REQUEST["val_name"] . "»</b> est déjà dans le projet", null, MODAL_SIZE_SMALL, [MSGBOX_MODAL_ATTR => [MODAL_NO_BACKDROP => true, MODAL_CENTERED => true]]);
          exit();
        }
        break;

      case "addNewTask":
        $row = $this->getCurrentEditedRow();
        if (!dbUtil()->insertRow("tasks", ["project" => $row["project"], "role" => $row["role"], "name" => $_REQUEST["val_name"]], null, DB_OPTION_NO_THROW)) {
          msgBox("La tâche <b>«" . $_REQUEST["val_name"] . "»</b> est déjà dans le projet", null, MODAL_SIZE_SMALL, [MSGBOX_MODAL_ATTR => [MODAL_NO_BACKDROP => true, MODAL_CENTERED => true]]);
          exit();
        }
        break;

      case "addExistingTask":
        dbUtil()->begin_transaction();
        $row = $this->getCurrentEditedRow();
        $action = utils()->action;
        $task = dbUtil()->getTableNameNoAs("tasks");
        foreach ($_REQUEST["val_name"] as $name) {
          dbUtil()->insertRow("tasks", ["project" => $row["project"], "name" => $name, "role" => $row["role"]], null, DB_OPTION_NO_THROW);
        }
        break;
    }
    $action && dbUtil()->commit();
  }

  protected function buildLine($row)
  {
    if ($row["role"] == -1) {
      parent::buildGroup($row);
      return;
    }
    parent::buildLine($row);
  }

  protected function getActionName($action)
  {
    if ($action == "update") {
      if ($this->isGroupLine)
        $action = "updProject";
      else if ($this->getRow()["name"] == "")
        $action = "updRole";
      else
        $action = "updTask";
    }
    $action == "delete" && $this->getRow()["name"] == "" && $action = "deleteRole";
    return $action;
  }

  protected function getDisplayValue($key, $row)
  {
    switch ($key) {
      case "prjName":
        $this->currentRole = -1;
        $this->currentTask = null;
        break;

      case "name":
        if ($this->currentRole != $row["role"]) {
          $this->currentRole = $row["role"];
          return !$row["role"] ? "(Tous)" : ConfigProject::get(CONFIG_ROLES)->getNameById($row["role"]);
        }
        if ($this->currentTask != $row["ri"]) {
          $this->currentTask = $row["ri"];
          return "<div class='ml-5 d-inline font-italic' style=text-decoration:underline>" . $row["name"] . "</div>";
        }

      case "prjEnded":
        return $row["prjEnded"] ? "X" : '';
    }

    return parent::getDisplayValue($key, $row);
  }

}
