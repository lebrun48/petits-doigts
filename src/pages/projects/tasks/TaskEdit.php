<?php

class TaskEdit extends Tasks
{

  public function __construct()
  {
    parent::__construct();
    $row = $this->getCurrentEditedRow();
    if ($row) {
      $row["name"] == '' && $row["name"] = null;
      $row["roleName"] = $row["role"] ? ConfigProject::get(CONFIG_ROLES)->getNameById($row["role"]) : "(Tous)";
    }
    switch (utils()->action) {
      case "duplicatePrj":
        $title = "Dupliquer projet";
        unset($row["prjName"]);
        unset($row["prjEnded"]);
      case "insert":
        !$title && $title = "Nouveau projet";
        $fields = [
            "prjName" => [
                ED_LABEL       => "Projet",
                ED_TYPE        => ED_TYPE_ALPHA,
                ED_VALIDATE    => [ED_VALIDATE_REQUIRED => true, ED_VALIDATE_MAX_LENGTH => 50],
                ED_FIELD_WIDTH => 12,
            ],
        ];
        break;

      case "updPrj":
        $title = "Editer projet";
        $fields = [
            "prjName"  => [
                ED_LABEL       => "Projet",
                ED_TYPE        => ED_TYPE_ALPHA,
                ED_VALIDATE    => [ED_VALIDATE_REQUIRED => true, ED_VALIDATE_MAX_LENGTH => 50],
                ED_FIELD_WIDTH => 6,
            ],
            "prjEnded" => [
                ED_LABEL       => "Terminé",
                ED_TYPE        => ED_TYPE_CHECK,
                ED_VALUE       => 1,
                ED_FIELD_WIDTH => 4,
                ED_NEXT_LINE   => true
            ],
        ];
        break;

      case "duplicateRole":
        $title = "Dupliquer rôle";
      case "addNewRole":
        !$title && $title = "Nouveau rôle";
        $row["role"] = null;
      case "updRole":
        !$title && $title = "Editer rôle";
        $fields = [
            ED_FORM_ATTR => "style=min-height:200px",
            "prjName"    => [
                ED_TYPE     => ED_TYPE_ALPHA,
                ED_DISABLED => true,
                ED_DIV_ATTR => "class='grey lighten-4 rounded-lg'"
            ],
            "role"       => [
                ED_LABEL         => "Rôle",
                ED_TYPE          => ED_TYPE_SELECT,
                ED_PLACEHOLDER   => '',
                ED_OPTIONS       => mdbCompos()->getOptions(array_merge(["0" => "(Tous)"], ConfigProject::get(CONFIG_ROLES)->getPropListById("name"))),
                ED_VALIDATE      => [ED_VALIDATE_REQUIRED => true],
                ED_DEFAULT_VALUE => "=''"
            ],
        ];
        break;

      case "addNewTask":
        $title = "Nouvelle tâche";
        $fields = [
            "prjName" => [
                ED_TYPE     => ED_TYPE_ALPHA,
                ED_DISABLED => true,
                ED_DIV_ATTR => "class='grey lighten-4 rounded-lg'"
            ],
            "role"    => [
                ED_TYPE     => ED_TYPE_ALPHA,
                ED_DISABLED => true,
                ED_VALUE    => $row["roleName"],
                ED_DIV_ATTR => "class='grey lighten-4 rounded-lg'"
            ],
            "name"    => [
                ED_TYPE     => ED_TYPE_ALPHA,
                ED_LABEL    => "Tâche",
                ED_VALIDATE => [ED_VALIDATE_REQUIRED => true, ED_VALIDATE_MAX_LENGTH => 50],
            ],
        ];
        break;

      case "addExistingTask":
        !$title && $title = "Ajouter tâches";
        $fields = [
            ED_FORM_ATTR => "style=min-height:300px",
            "prjName"    => [
                ED_TYPE     => ED_TYPE_ALPHA,
                ED_DISABLED => true,
                ED_DIV_ATTR => "class='grey lighten-4 rounded-lg'"
            ],
            "role"       => [
                ED_TYPE     => ED_TYPE_ALPHA,
                ED_DISABLED => true,
                ED_VALUE    => $row["roleName"],
                ED_DIV_ATTR => "class='grey lighten-4 rounded-lg'"
            ],
            "name[]"     => [
                ED_TYPE              => ED_TYPE_SELECT,
                ED_SELECT_SEARCHABLE => "Chercher ici",
                ED_SELECT_MULTIPLE   => true,
                ED_PLACEHOLDER       => "",
                ED_OPTIONS           => mdbCompos()->getOptions(dbUtil()->selectRow(
                                "tasks",
                                "null, name",
                                "name<>'' and name not in (select name from tasks where project=" . $row["project"] . " and role=" . $row["role"] . ") group by name"), "class=py-0"),
                ED_LABEL             => "Tâche",
                ED_VALIDATE          => [ED_VALIDATE_REQUIRED => true, ED_VALIDATE_MAX_LENGTH => 50],
            ],
        ];
        break;

      case "updTask":
        $title = "Editer tâche";
        $fields = [
            "prjName" => [
                ED_TYPE     => ED_TYPE_ALPHA,
                ED_DISABLED => true,
                ED_DIV_ATTR => "class='grey lighten-4 rounded-lg'"
            ],
            "role"    => [
                ED_TYPE     => ED_TYPE_ALPHA,
                ED_VALUE    => $row["roleName"],
                ED_DISABLED => true,
                ED_DIV_ATTR => "class='grey lighten-4 rounded-lg'"
            ],
            "name"    => [
                ED_TYPE     => ED_TYPE_ALPHA,
                ED_LABEL    => "Tâche",
                ED_VALIDATE => [ED_VALIDATE_REQUIRED => true, ED_VALIDATE_MAX_LENGTH => 50],
            ],
        ];
        break;
    }

    msgBox(BuildForm::getForm($fields, $row), $title, null, [MSGBOX_BUTTON_ACTION => "Eregistrer", MSGBOX_BUTTON_CLOSE => "Annuler", MSGBOX_MODAL_ATTR => [MODAL_SCROLLABLE => true]]);
  }

}
