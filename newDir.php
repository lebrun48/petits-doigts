<?php

class ServiceConfig
{

  public $pathinfo;
  public $config;

  public function __construct()
  {
    global $curDir;
    $configFile = explode('.', $_SERVER["HTTP_HOST"]);
    $configFile = $configFile[0] == "192" && $configFile[1] == "168" ? "localhost" : (($idx = sizeof($configFile) - 2) > 0 ? $configFile[$idx] : $configFile[0]);
    $url = explode('/', substr($_SERVER["SCRIPT_URL"], 1, -1));
    $configFile .= ($url[0] == "demo" || $url[0] == "test" ? ".$url[0]" : '') . ".ini";
    file_exists($iniFile = "config/$configFile") || $iniFile = "config/service.ini";
    $this->config = parse_ini_file($iniFile, true);
    $this->pathinfo = pathinfo($_SERVER["SCRIPT_NAME"]);
    $relative = str_replace("\\", "/", str_replace(dirname(__FILE__), '', $curDir));
    $rootPath = str_replace("$relative", '', dirname($_SERVER["SCRIPT_NAME"]));
    $this->config["path"]["request_root_path"] = $rootPath = rtrim(str_replace("\\", "/", $rootPath), "/");
    chdir($this->config["path"]["sources_path"]);
    $this->config["path"]["project_full_path"] = $projectPath = str_replace("\\", "/", dirname(stream_resolve_include_path("newDir.php")));
    $this->config["path"]["include_path"] = str_replace($rootPath, '', $projectPath);
  }

}

function serviceConfig() : ServiceConfig
{
  static $serviceConfig;
  !$serviceConfig && $serviceConfig = new ServiceConfig;
  return $serviceConfig;
}

//$maintenance = true;
include serviceConfig()->pathinfo["basename"];

